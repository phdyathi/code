//
// Created by yathi on 12/03/16.
//

#include "TranslationMSCLayer.h"

TranslationMSCLayer::TranslationMSCLayer(array* forwardInput,array* backwardInput, float maxX, float minX,
                    float maxY, float minY,
                    const int &numTransformsX, int numTransformsY)
        : IMSCLayer(forwardInput,backwardInput,numTransformsX * numTransformsY)
{
    transformValues = std::vector<transform2D>(numTransformsX * numTransformsY);

    auto xInterval = (maxX + std::abs(minX))/numTransformsX;
    auto yInterval = (maxY + std::abs(minY))/numTransformsY;

    auto i = 0;
    for(auto xi = 0; xi < numTransformsX; xi++ )
    {
        auto x = xInterval * xi;
        auto xt = minX + x;
        for(auto yi = 0;yi < numTransformsY; yi++)
        {
            auto y = yInterval * yi;
            auto yt = minY + y;

            transformValues[i].X = xt;
            transformValues[i].Y = yt;
            i++;
        }
    }
}

TranslationMSCLayer::TranslationMSCLayer(const af::dim4 &numDims, float maxX, float minX,
                    float maxY, float minY,
                    const int &numTransformsX, int numTransformsY) :
    IMSCLayer(numTransformsX * numTransformsY,numDims)
{
    transformValues = std::vector<transform2D>(numTransformsX * numTransformsY);

    auto xInterval = (maxX + std::abs(minX)+1)/numTransformsX;
    auto yInterval = (maxY + std::abs(minY)+1)/numTransformsY;

    auto i = 0;
    for(auto xi = 0; xi < numTransformsX; xi++ )
    {
        auto x = xInterval * xi;
        auto xt = minX + x;
        for(auto yi = 0;yi < numTransformsY; yi++)
        {
            auto y = yInterval * yi;
            auto yt = minY + y;

            transformValues[i].X = xt;
            transformValues[i].Y = yt;
            i++;
        }
    }
}

array TranslationMSCLayer::transform(const array &input, const int &transform, bool inverse)
{
    //std::cout<<"Translation layer"<<std::endl;
    auto t = transformValues[transform];
    if(inverse)
    {
        t.X = - t.X;
        t.Y = - t.Y;
    }
    return af::translate(input,t.X,t.Y);
}

std::string TranslationMSCLayer::getTransformAsString(int transformIndx)
{
    std::stringstream stringstream;
    stringstream << "X{"<<transformValues[transformIndx].X<<"} Y{"<<transformValues[transformIndx].Y<<"}";
    return stringstream.str();
}

void TranslationMSCLayer::printType()
{
    std::cout<<"Translation"<<std::endl;
}