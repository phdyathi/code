//
// Created by yathi on 5/03/16.
//

#ifndef MAPSEEKINGCIRCUIT_ROTATIONMSCLAYER_H
#define MAPSEEKINGCIRCUIT_ROTATIONMSCLAYER_H

#include "IMSCLayer.h"
class RotationMSCLayer  : public IMSCLayer {

public:
    RotationMSCLayer(array *forwardInput,array *backwardInput, float maxDegrees, float minDegrees,
                         const int &numTransforms);
    RotationMSCLayer(const af::dim4& numDims, float maxDegrees, float minDegrees,
                     const int &numTransforms);
    array transform(const array &input, const int &transform, bool inverse);

    void printType();
private:
    std::vector<float> transformValues;
    std::string getTransformAsString(int transformIndx);
};


#endif //MAPSEEKINGCIRCUIT_ROTATIONMSCLAYER_H
