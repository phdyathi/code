//
// Created by yathi on 5/03/16.
//

#include "RotationMSCLayer.h"
#include <assert.h>
#include "Utils.h"
    RotationMSCLayer::RotationMSCLayer(array *forwardInput,array *backwardInput, float maxDegrees, float minDegrees,
                                   const int &numTransforms)
: IMSCLayer(forwardInput, backwardInput, numTransforms)
{
    transformValues = std::vector<float>(numTransforms);
    auto interval = (maxDegrees + std::abs(minDegrees))/numTransforms;
    for(auto i = 0; i < numTransforms; i++)
    {
        auto inter = (interval * i);
        auto t = (minDegrees + inter);
        transformValues[i] =  t;
    }
    assert(transformValues[numTransforms-1] != (maxDegrees) );
}

RotationMSCLayer::RotationMSCLayer(const af::dim4& numDims, float maxDegrees, float minDegrees,
                 const int &numTransforms) : IMSCLayer(numTransforms,numDims)
{
    transformValues = std::vector<float>(numTransforms);
    auto interval = (maxDegrees + std::abs(minDegrees))/numTransforms;
    for(auto i = 0; i < numTransforms; i++)
    {
        auto inter = (interval * i);
        auto t = (minDegrees + inter);
        transformValues[i] =  t;
    }
    assert(transformValues[numTransforms-1] != (maxDegrees) );
}

array RotationMSCLayer::transform(const array &input, const int &transform, bool inverse)
{
    //std::cout<<"Rotation layer"<<std::endl;
    auto transValue = inverse ? -transformValues[transform] : transformValues[transform];
    return af::rotate(input,TO_RADIANS(transValue));
}

std::string RotationMSCLayer::getTransformAsString(int transformIndx)
{
    std::stringstream stringstream;
    stringstream << transformValues[transformIndx] << " degrees";
    return stringstream.str();
}

void RotationMSCLayer::printType()
{
    std::cout<<"Rotation layer"<<std::endl;
}