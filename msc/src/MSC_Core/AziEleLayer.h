//
// Created by yathi on 20/06/16.
//

#ifndef MAPSEEKINGCIRCUIT_MEMLAYER_H
#define MAPSEEKINGCIRCUIT_MEMLAYER_H


#include "IMSCLayer.h"

class AziEleLayer : public IMSCLayer{
public:
    AziEleLayer(const int &numTransforms,const af::dim4 &numDims,const std::string &loadFromDir);

    array transform(const array &input, const int &transform, bool inverse);

    void printType();

protected:
    std::string getTransformAsString(int transformIndx);

    std::vector<array>* memories;
    std::vector<std::string> fileNames;
};


#endif //MAPSEEKINGCIRCUIT_MEMLAYER_H
