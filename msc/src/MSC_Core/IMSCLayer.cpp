//
// Created by yathi on 20/02/16.
//

#include "IMSCLayer.h"
#include "Utils.h"
#define constant_K 0.7f

IMSCLayer::IMSCLayer(array* forwardInput,array* backwardInput, const int &numTransforms) :
        inputForwardSuperPostion(forwardInput),
        inputBackwardSuperPostion(backwardInput),
        numTransforms(numTransforms)
{
    this->outputBackwardSuperpostion = af::constant<float>(0.0f, inputForwardSuperPostion->dims(), af_dtype::f32);
    this->outputForwardSuperposition = af::constant<float>(0.0f, inputForwardSuperPostion->dims(), af_dtype::f32);
    weights = af::constant<float>(1.0f,numTransforms,af_dtype::f32);
}

IMSCLayer::IMSCLayer(const int &numTransforms,const af::dim4 &numDims) :
numTransforms(numTransforms)
{
    this->inputForwardSuperPostion = 0;
    this->inputBackwardSuperPostion = 0;
    this->outputBackwardSuperpostion = af::constant<float>(0.0f, numDims, af_dtype::f32);
    this->outputForwardSuperposition = af::constant<float>(0.0f, numDims, af_dtype::f32);
    weights = af::constant<float>(1.0f,numTransforms,af_dtype::f32);
}

void IMSCLayer::computeBackwardPath()
{
    outputBackwardSuperpostion = af::constant<float>(0.0f, outputBackwardSuperpostion.dims(), af_dtype::f32);
    unsigned int* wIdx = 0;
    int wIdxSize = 0;
    {
        auto wIdxArray = af::where(weights);
        //std::cout << wIdxArray.dims() << std::endl << wIdxArray.type()<<std::endl;
        wIdx = wIdxArray.host<unsigned int>();
        wIdxSize = wIdxArray.elements();
    }
    array dots = constant<float>(0.0f,numTransforms,af::dtype::f32);
    for(int i = 0; i < wIdxSize; i++) {
        auto wi = wIdx[i];
        if (!this->isBackwardTransformStatic){
            array transformedArray = transform(*inputBackwardSuperPostion, wi, false);
            outputBackwardSuperpostion = (transformedArray * weights(wi).scalar<float>()) + outputBackwardSuperpostion;
        }else
        {
            outputBackwardSuperpostion = ((*this->precomputedTransforms)[i] * weights(wi).scalar<float>()) + outputBackwardSuperpostion;
        }
    }
    outputBackwardSuperpostion = Utils::normalize(outputBackwardSuperpostion);
}

void IMSCLayer::computeForwardPath()
{
    std::vector<array>* transformed = isForwardTransStatic ? precomputedTransforms : new std::vector<array>(numTransforms);

    unsigned int* wIdx = 0;
    int wIdxSize = 0;
    {
        auto wIdxArray = af::where(weights);
        //std::cout << wIdxArray.dims() << std::endl << wIdxArray.type()<<std::endl;
        wIdx = wIdxArray.host<unsigned int>();
        wIdxSize = wIdxArray.elements();
    }
    array dots = constant<float>(0.0f,numTransforms,af::dtype::f32);

    for(int i = 0; i < wIdxSize; i++)
    {
        auto wi = wIdx[i];
        if(!isForwardTransStatic)
            (*transformed)[wi] = transform(*inputForwardSuperPostion, wi, true);

        if(inputBackwardSuperPostion != 0)
            dots(wi) = dot(af::flat(*inputBackwardSuperPostion),af::flat((*transformed)[wi]));
        else
            dots(wi) = dot(af::flat((*transformed)[wi]),af::flat(*inputForwardSuperPostion));
    }

    //af_print(dots);
    //af_print(weights);
    if(this->numNonZeroWeights() != 1)
        weights = calculateWeights(dots,weights);
    //af_print(weights);
    outputForwardSuperposition = af::constant<float>(0.0f, outputForwardSuperposition.dims(), af_dtype::f32);
    for(int i = 0; i < wIdxSize; i++)
    {
        auto wi = wIdx[i];
        outputForwardSuperposition =( (*transformed)[wi]*weights(wi).scalar<float>()) + outputForwardSuperposition;
    }
    outputForwardSuperposition = Utils::normalize(outputForwardSuperposition);
    if(!isForwardTransStatic)
        delete transformed;

    delete wIdx;
}

array IMSCLayer::calculateWeights(const array &dotProducts, const array &currWeights)
{
    float const maxQ = max<float>(dotProducts);

    auto newWieghts = currWeights - (this->k_constant * (1 - (dotProducts/maxQ)));

    auto b =(newWieghts > this->minWeight);
    //af_print(b);
    //af_print(b*newWieghts);
    return newWieghts * b;
}

int IMSCLayer::numNonZeroWeights()
{
     return weights.nonzeros();
}

void IMSCLayer::printWeights()
{
    af_print(weights);
}

void IMSCLayer::printResults()
{
    auto nonzero = af::where(weights);
    auto size = nonzero.elements();
    if(size > 0)
    {
        for(auto i = 0; i < size; i++)
        {
            auto index = nonzero(i).scalar<unsigned int>();
            std::cout<<i<<": "<<getTransformAsString(index) << " w("<<weights(index).scalar<float>()<<")"<<std::endl;
        }
    }
}

void IMSCLayer::setForwardTransStatic()
{
    this->isForwardTransStatic = true;
    if(this->precomputedTransforms == 0)
        this->precomputedTransforms = new std::vector<array>(this->numTransforms);
    else {
        this->precomputedTransforms->clear();
        this->precomputedTransforms->resize(this->numTransforms);
    }

    for(int i = 0; i < numTransforms; i++)
    {
        (*this->precomputedTransforms)[i] = transform(*inputForwardSuperPostion, i, false);
    }
}
void IMSCLayer::setBackwardTransStatic()
{
    this->isBackwardTransformStatic = true;
    if(this->precomputedTransforms == 0)
        this->precomputedTransforms = new std::vector<array>(this->numTransforms);
    else{
        this->precomputedTransforms->clear();
        this->precomputedTransforms->resize(this->numTransforms);
    }

    for(int i = 0; i < numTransforms; i++)
    {
        (*this->precomputedTransforms)[i] = transform(*inputBackwardSuperPostion, i, true);
        //Utils::showImgInWindow((*this->precomputedTransforms)[i]);
    }
}