//
// Created by yathi on 5/03/16.
//

#include "MSCLayerManager.h"
#include "TranslationMSCLayer.h"
#include "RotationMSCLayer.h"
#include "ScaleMSCLayer.h"
#include "AziEleLayer.h"
#include "Utils.h"

MSCLayerManager::MSCLayerManager(const TranslationOptions& tOpt,const RotationOptions& rOpts,const ScaleOptions& sOpts,const AziEleOptions& aziEleOpts,const af::dim4 numDim)
{
    TranslationMSCLayer* translationMSCLayer =  new TranslationMSCLayer(numDim,tOpt.x,-tOpt.x,tOpt.y,-tOpt.y,tOpt.numTransforms,tOpt.numTransforms);
    translationMSCLayer->k_constant = tOpt.layerOptions.k_constant;
    translationMSCLayer->minWeight = tOpt.layerOptions.minWeight;
    RotationMSCLayer* rotationMSCLayer = new RotationMSCLayer(numDim,rOpts.maxRotation,rOpts.minRotation,rOpts.numTransforms);
    rotationMSCLayer->k_constant = rOpts.layerOptions.k_constant;
    rotationMSCLayer->minWeight = rOpts.layerOptions.minWeight;
    ScaleMSCLayer* scaleMSCLayer = new ScaleMSCLayer(numDim,sOpts.maxScale,sOpts.minScale,sOpts.numTransforms);
    scaleMSCLayer->k_constant = sOpts.layerOptions.k_constant;
    scaleMSCLayer->minWeight = sOpts.layerOptions.minWeight;
    AziEleLayer* aziEleMSCLayer = new AziEleLayer(aziEleOpts.numImgs,numDim,aziEleOpts.imgDir);
    aziEleMSCLayer->k_constant = aziEleOpts.layerOptions.k_constant;
    aziEleMSCLayer->minWeight = aziEleOpts.layerOptions.minWeight;

    translationMSCLayer->inputBackwardSuperPostion = &rotationMSCLayer->outputBackwardSuperpostion;
    rotationMSCLayer->inputForwardSuperPostion = &translationMSCLayer->outputForwardSuperposition;
    rotationMSCLayer->inputBackwardSuperPostion = &scaleMSCLayer->outputBackwardSuperpostion;
    scaleMSCLayer->inputForwardSuperPostion = &rotationMSCLayer->outputForwardSuperposition;
    scaleMSCLayer->inputBackwardSuperPostion = &aziEleMSCLayer->outputBackwardSuperpostion;
    aziEleMSCLayer->inputForwardSuperPostion = &scaleMSCLayer->outputForwardSuperposition;

    this->layers.push_back(translationMSCLayer);
    this->layers.push_back(rotationMSCLayer);
    this->layers.push_back(scaleMSCLayer);
    this->layers.push_back(aziEleMSCLayer);
}

void MSCLayerManager::run(array* input,array* memory)
{
    //this->layers[this->layers.size()-1]->inputBackwardSuperPostion = memory;
    //this->layers[this->layers.size()-1]->setBackwardTransStatic();
    this->layers[0]->inputForwardSuperPostion = input;
    //this->layers[0]->setForwardTransStatic();

    timer::start();
    auto mscIter = 0;
    while(!areAllLayersDone() && mscIter < 20)
    {
        mscIter++;
        for(int i = (this->layers.size()-1); i >= 0; i--)
        {
            this->layers[i]->computeBackwardPath();
        }

        for(auto i = 0; i < this->layers.size(); i++)
        {
            this->layers[i]->computeForwardPath();
        }

        //this->layers[0]->printWeights();
        //Utils::showImgInWindow(this->layers[0]->outputBackwardSuperpostion);
    }
    auto time = timer::stop();
    std::cout<< "Time: "<<time<<std::endl;
    for(auto iter = this->layers.begin(); iter != this->layers.end(); ++iter)
    {
        (*iter)->printResults();
    }
}

bool MSCLayerManager::areAllLayersDone()
{
    auto total = 0;
    for( auto iter = this->layers.begin(); iter != this->layers.end(); ++iter)
    {
        total += (*iter)->numNonZeroWeights();
        //uncomment to enable
        //(*iter)->printType();
        //std::cout<<(*iter)->numNonZeroWeights()<<std::endl;
    }
    return total == this->layers.size();
}