//
// Created by yathi on 12/03/16.
//

#ifndef MAPSEEKINGCIRCUIT_TRANSLATIONMSCLAYER_H
#define MAPSEEKINGCIRCUIT_TRANSLATIONMSCLAYER_H
#include "IMSCLayer.h"

class TranslationMSCLayer : public IMSCLayer {

public:
    TranslationMSCLayer(array* forwardInput,array* backwardInput, float maxX, float minX,
                        float maxY, float minY,
                     const int &numTransformsX, int numTransformsY);
    TranslationMSCLayer(const af::dim4 &numDims, float maxX, float minX,
                        float maxY, float minY,
                        const int &numTransformsX, int numTransformsY);
    array transform(const array &input, const int &transform, bool inverse);

    std::string getTransformAsString(int transformIndx);

    void printType();
private:
    struct transform2D
    {
        float X;
        float Y;
    };
    std::vector<transform2D> transformValues;

};


#endif //MAPSEEKINGCIRCUIT_TRANSLATIONMSCLAYER_H
