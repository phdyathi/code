//
// Created by yathi on 20/06/16.
//

#include "AziEleLayer.h"

#include "Utils.h"

AziEleLayer::AziEleLayer(const int &numTransforms,const af::dim4 &numDims,const std::string &loadFromDir)
: IMSCLayer(numTransforms,numDims)
{

    memories = Utils::LoadImgsFromDir(loadFromDir,this->fileNames);
    if(memories->size() != numTransforms)
        throw exception("Number of memories does not match numTransforms!");
    if(memories->at(0).dims() != numDims)
        throw exception("File dims differant!");
}

array AziEleLayer::transform(const array &input, const int &transform, bool inverse)
{
    return memories->at(transform);
}

void AziEleLayer::printType()
{
    std::cout<< "AziEle"<<std::endl;
}

std::string AziEleLayer::getTransformAsString(int transformIndx)
{
    std::stringstream stringstream;
    stringstream << fileNames[transformIndx];
    return stringstream.str();
}