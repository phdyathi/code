//
// Created by yathi on 27/03/16.
//

#ifndef MAPSEEKINGCIRCUIT_STATICMEMORYLAYER_H
#define MAPSEEKINGCIRCUIT_STATICMEMORYLAYER_H


#include "IMSCLayer.h"

class StaticMemoryLayer : public IMSCLayer{

public:
    StaticMemoryLayer(const dim4 &numDims, const af::array& memories,int numMemories);

    array transform(const array &input, const int &transform, bool inverse);

protected:
    std::string getTransformAsString(int transformIndx);

    const af::array& memories;
};


#endif //MAPSEEKINGCIRCUIT_STATICMEMORYLAYER_H
