//
// Created by yathi on 28/02/16.
//

#ifndef CMAKETESTPROJECT_PRECODEDMSCLAYER_H
#define CMAKETESTPROJECT_PRECODEDMSCLAYER_H

#include "IMSCLayer.h"

class PrecodedMSCLayer : public IMSCLayer{
public:
    PrecodedMSCLayer(array* forwardInput,array* backwardInput, const int& numtransforms);
    array transform(const array &input, const int &transform, bool inverse);
    void printType();
private:
    std::vector<std::array<float,5>> precodedTransforms;
    std::string getTransformAsString(int transformIndx);
};


#endif //CMAKETESTPROJECT_PRECODEDMSCLAYER_H
