//
// Created by yathi on 13/03/16.
//

#include "ScaleMSCLayer.h"

ScaleMSCLayer::ScaleMSCLayer(array* forwardInput,array* backwardInput, float maxScale, float minScale,
              const int &numTransforms)
    : IMSCLayer(forwardInput,backwardInput,numTransforms)
{
    transformValues = std::vector<float>(numTransforms);
    //auto interval = (minScale)/((numTransforms-1)/2);
    auto curr = 0.0f;
    for(auto i = 0; curr < maxScale; i++)
    {
        auto inter = (0.1 * i);
        auto t = (minScale + inter);
        curr = t;
        if(i >= transformValues.size())
        {
            transformValues.push_back(t);
        }
        else
            transformValues[i] =  t;
    }
}

ScaleMSCLayer::ScaleMSCLayer(const af::dim4& numDims, float maxScale, float minScale,
              const int &numTransforms) : IMSCLayer(numTransforms,numDims)
{
    transformValues = std::vector<float>(numTransforms);
    //auto interval = (minScale)/((numTransforms-1)/2);
    auto curr = 0.0f;
    for(auto i = 0; curr < maxScale; i++)
    {
        auto inter = (0.1 * i);
        auto t = (minScale + inter);
        curr = t;
        if(i >= transformValues.size())
        {
            transformValues.push_back(t);
        }
        else
            transformValues[i] =  t;
    }
}

array ScaleMSCLayer::transform(const array &input, const int &transform, bool inverse)
{
    //std::cout<<"Scale layer"<<std::endl;
    auto scale = transformValues[transform];
    float tvals[] = {scale,0.0f,((input.dims(0)-(input.dims(0) * scale)) * 0.5f),
                     0.0f,scale,((input.dims(0)-(input.dims(0) * scale)) * 0.5f)};
    auto t = array(3,2,tvals);

    return af::transform(input,t,0,0,AF_INTERP_NEAREST,inverse);
   // return af::scale(input,transformValues[transform],transformValues[transform]
   //         ,input.dims(0),input.dims(1));
}

std::string ScaleMSCLayer::getTransformAsString(int transformIndx)
{
    std::stringstream stringstream;
    stringstream << transformValues[transformIndx];
    return stringstream.str();
}

void ScaleMSCLayer::printType()
{
    std::cout<<"Scale layer"<<std::endl;
}