//
// Created by yathi on 27/03/16.
//

#include "StaticMemoryLayer.h"

StaticMemoryLayer::StaticMemoryLayer(const dim4 &numDims, const af::array& memories,int numMemories) : IMSCLayer(
        numMemories,numDims),memories(memories)
{

}

array StaticMemoryLayer::transform(const array &input, const int &transform, bool inverse)
{
    return memories(transform);
}

std::string StaticMemoryLayer::getTransformAsString(int transformIndx)
{

}
