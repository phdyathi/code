//
// Created by yathi on 28/02/16.
//
#include <array>
#include "PrecodedMSCLayer.h"
PrecodedMSCLayer::PrecodedMSCLayer( array* forwardInput,array* backwardInput, const int& numtransforms) :
        IMSCLayer(forwardInput, backwardInput, numtransforms)
{
}

array PrecodedMSCLayer::transform(const array &input, const int &transform, bool inverse)
{
    float arr[][5] = {{1,0,1,0,1},
                      {0,1,0,1,1},
                      {1,0,1,1,1}};
    return array(input.dims(0),arr[transform]);
}

std::string PrecodedMSCLayer::getTransformAsString(int transformIndx)
{
    return "";
}

void PrecodedMSCLayer::printType()
{
    std::cout<<"Precoded"<<std::endl;
}