//
// Created by yathi on 20/02/16.
//

#ifndef CMAKETESTPROJECT_MSCLAYER_H
#define CMAKETESTPROJECT_MSCLAYER_H

#include "arrayfire.h"
using namespace af;

class IMSCLayer {
public:
    IMSCLayer(array* forwardInput,array* backwardInput, const int &numTransforms);
    IMSCLayer(const int &numTransforms,const af::dim4 &numDims);

    void computeBackwardPath();
    void computeForwardPath();

    //not sure if virtual will slow things down. Might need to move virtual
    //to transform init then just call normal func to transform
    virtual array transform(const array &input, const int &transform, bool inverse) =0;

    array calculateWeights(const array& dotProducts, const array& currWeights);

    int numNonZeroWeights();

    void printWeights();

    void printResults();
    virtual void printType() = 0;

    //calling these after construction, pre-transforms and stores input
    void setForwardTransStatic();
    void setBackwardTransStatic();

    array* inputForwardSuperPostion;
    array* inputBackwardSuperPostion;
    float minWeight =0.2f;
    float k_constant = 0.7f;
    array outputBackwardSuperpostion;
    array outputForwardSuperposition;

protected:

    array weights;

    int numTransforms;

    virtual std::string getTransformAsString(int transformIndx) = 0;

    bool isForwardTransStatic = false;
    bool isBackwardTransformStatic = false;
    std::vector<af::array>* precomputedTransforms= 0;

    af::array batchDots(std::vector<af::array>* transformed);
};


#endif //CMAKETESTPROJECT_MSCLAYER_H
