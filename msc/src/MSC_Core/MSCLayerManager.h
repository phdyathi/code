//
// Created by yathi on 5/03/16.
//
#include "IMSCLayer.h"


#ifndef MAPSEEKINGCIRCUIT_MSCLAYERMANAGER_H
#define MAPSEEKINGCIRCUIT_MSCLAYERMANAGER_H

struct LayerOptions
{
    float minWeight;
    float k_constant;
    float weight_threshold;
};

struct RotationOptions
{
    float maxRotation;
    float minRotation;
    int numTransforms;
    LayerOptions layerOptions;
};

struct TranslationOptions
{
    float x;
    float y;
    int numTransforms;
    LayerOptions layerOptions;
};

struct ScaleOptions
{
    float minScale;
    float maxScale;
    int numTransforms;
    LayerOptions layerOptions;
};

struct AziEleOptions
{
    float minAzimuth;
    float maxAzimuth;
    float minElevation;
    float maxElevation;
    std::string imgDir;
    std::string imgExt;
    int numImgs;
    LayerOptions layerOptions;
};

class MSCLayerManager {

public:
    MSCLayerManager(const TranslationOptions& tOpt,const RotationOptions& rOpts,const ScaleOptions& sOpts,const AziEleOptions& aziEleOpts,const af::dim4 numDim);
    MSCLayerManager(const TranslationOptions& tOpt);
    MSCLayerManager(const RotationOptions& rOpts);
    MSCLayerManager(const ScaleOptions& sOpts);

    void run(array* input,array* memory);


    //const af::array& forwardInputVector;
private:
    std::vector<IMSCLayer*> layers;

    bool areAllLayersDone();

};
#endif //MAPSEEKINGCIRCUIT_MSCLAYERMANAGER_H
