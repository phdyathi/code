//
// Created by yathi on 6/03/16.
//

#ifndef MAPSEEKINGCIRCUIT_UTILS_H
#define MAPSEEKINGCIRCUIT_UTILS_H

#include <math.h>
#define BOOST_FILESYSTEM_VERSION 3
#define BOOST_FILESYSTEM_NO_DEPRECATED
#include <boost/filesystem.hpp>
namespace fs = ::boost::filesystem;
#define TO_RADIANS(x) x * (M_PI/180.0)
#define TO_DEGREES(x) x * (180.0/M_PI)

class Utils
{
public:

    static array normalize(const array &in)
    {
        float mx = max<float>(in);
        float mn = min<float>(in);
        return (in-mn)/(mx-mn);
    }

    static unsigned numTransformsFor(float maxVal, float minVal,float increment)
    {
        return ((float)(fabs(maxVal) - minVal)/increment)+1;
    }

    static array loadSobelImg(const char* imgFile) {

        af::array img = af::loadImage(imgFile,false);
        //img = af::resize(img, img.dims(0) / 2, img.dims(1) / 2);


        int channels = img.dims(2);
        if (channels > 1)
            img = af::rgb2gray(img);
        //float max = af::max<float>(img);
        //img = img / max;
        int w = 5;
        if (img.dims(0) < 512) w = 3;
        if (img.dims(0) > 2048) w = 7;
        int h = 5;
        if (img.dims(0) < 512) h = 3;
        if (img.dims(0) > 2048) h = 7;
        af::array ker = af::gaussianKernel(w, h);//(2 * w * 2), (2 * h * 2));//, (2 * w * 2), (2 * h * 2));
        af::array smooth = convolve(img, ker);

        af::array Gx, Gy;
        af::sobel(Gx, Gy, smooth,3);
        // Find magnitude and direction
        af::array mag = hypot(Gx, Gy);
        af::array dir = atan2(Gy, Gx);

        mag = normalize(mag);
        auto thresholdValue = 0.3f;
        auto toReturn = 1.0f*(mag > thresholdValue);//(mag < thresholdValue) * 0.0f + 255.0f * (mag > thresholdValue);
        return toReturn;
    }

    static void showImgInGrid(const af::array img[],int num )
    {
        for(int i = 0; i < num; i++)
        {
            std::stringstream stringstream;
            stringstream <<"./chickOut_" <<i<<".jpg";
            af::saveImage(stringstream.str().c_str(),img[i]);
        }


        af::Window myWindow("Edge Dectectors");
        while(!myWindow.close())
        {
            myWindow.grid(num,1);
            for(int i = 0; i < num; i++)
            {
                myWindow(i,0).image(img[i]);
            }
            myWindow.show();
        }
    }

    static void showImgInWindow(const af::array& img)
    {
        af::saveImage("./chickOut.jpg",img);
        af::Window myWindow("Edge Dectectors");
        try {
            while (!myWindow.close()) {
                myWindow.grid(1,1);
                myWindow(0,0).image(img,"chick");

                //myWindow.setSize(img.dims(0),img.dims(1));
                myWindow.show();
            }
        }catch (af::exception& af)
        {
            std::cout << af.what()<<std::endl;
            throw;
        }
    }

    // return the filenames of all files that have the specified extension
    // in the specified directory and all subdirectories
    static void get_all(const fs::path& root, const std::string& ext, std::vector<fs::path>& ret)
    {
        if(!fs::exists(root) || !fs::is_directory(root)) return;

        fs::recursive_directory_iterator it(root);
        fs::recursive_directory_iterator endit;

        while(it != endit)
        {
            if(fs::is_regular_file(*it) && it->path().extension() == ext) ret.push_back(it->path());
            ++it;

        }

    }

    static std::vector<af::array>* LoadImgsFromDir(const std::string& filePath,std::vector<std::string>& outFilesNames)
    {
        auto files = std::vector<fs::path>();
        get_all(fs::path(filePath),".png",files);
        if(files.size() > 0)
        {
            auto toReturn = new std::vector<array>();
            for(int i = 0; i < files.size(); i++)
            {
                toReturn->push_back(loadSobelImg(files[i].string().c_str()));
                outFilesNames.push_back(files[i].string());
            }
            return toReturn;
        }
        else
            throw exception("Files not found!");

    }
};


#endif //MAPSEEKINGCIRCUIT_UTILS_H
