//
// Created by yathi on 13/03/16.
//

#ifndef MAPSEEKINGCIRCUIT_SCALEMSCLAYER_H
#define MAPSEEKINGCIRCUIT_SCALEMSCLAYER_H


#include "IMSCLayer.h"

class ScaleMSCLayer : public IMSCLayer {

public:
    ScaleMSCLayer(array* forwardInput,array* backwardInput, float maxScale, float minScale,
                  const int &numTransforms);
    ScaleMSCLayer(const af::dim4& numDims, float maxScale, float minScale,
                  const int &numTransforms);
    array transform(const array &input, const int &transform, bool inverse);

    void printType();
private:
    std::vector<float> transformValues;
    std::string getTransformAsString(int transformIndx);
};


#endif //MAPSEEKINGCIRCUIT_SCALEMSCLAYER_H
