#include <array>
#include <unistd.h>
#include "arrayfire.h"
#include "MSC_Core/IMSCLayer.h"
#include "MSC_Core/PrecodedMSCLayer.h"
#include "MSC_Core/RotationMSCLayer.h"
#include "MSC_Core/TranslationMSCLayer.h"
#include "MSC_Core/ScaleMSCLayer.h"
#include "MSC_Core/MSCLayerManager.h"
#include "MSC_Core/Utils.h"



void runMSC()
{
    float inRaw[] = {1,0,1,1,1};
    af::array in(5,inRaw);
    PrecodedMSCLayer layer(&in,&in,3);

    layer.computeForwardPath();
    layer.computeForwardPath();
    layer.computeForwardPath();

}

void runRotMSC()
{
    auto img = Utils::loadSobelImg("./test.jpg");
    auto input = rotate(img,TO_RADIANS(-60));
    RotationMSCLayer rotationMSCLayer(&input, &img, 90, -90, 36);

    while(rotationMSCLayer.numNonZeroWeights() != 1)
    {
        rotationMSCLayer.computeBackwardPath();
        rotationMSCLayer.computeForwardPath();
        Utils::showImgInWindow(rotationMSCLayer.outputForwardSuperposition);
    }
    rotationMSCLayer.printResults();
}

void runScaleMSC()
{
    auto img = Utils::loadSobelImg("./test.jpg");
    auto scale = 0.8f;
    auto actualScale = 2.0f - scale;
    float tvals[] = {scale,0.0f,((img.dims(0)-(img.dims(0) * scale)) * 0.5f),
                     0.0f,scale,((img.dims(1)-(img.dims(1) * scale)) * 0.5f)};
    auto t = array(3,2,tvals);

    auto input = transform(img,t,0,0,AF_INTERP_NEAREST,false); //scale(img,0.5,0.5,img.dims(0),img.dims(1));
    std::cout << input.dims() << std::endl;
    std::cout << img.dims() << std::endl;
    //Utils::showImgInWindow(img);
    //Utils::showImgInWindow(input);
    af::array arr[2] = {img,input};
    //Utils::showImgInGrid(arr,2);
    auto scaleMSCLayer = ScaleMSCLayer(&input,&img,2.0,0.5,15);

    while(scaleMSCLayer.numNonZeroWeights() != 1)
    {
        scaleMSCLayer.computeBackwardPath();
        scaleMSCLayer.computeForwardPath();
        Utils::showImgInWindow(scaleMSCLayer.outputForwardSuperposition);
    }
    scaleMSCLayer.printResults();
}

void runTransMSC()
{
    auto img = Utils::loadSobelImg("./test.jpg");
    auto input = translate(img,10,10);
    auto d = input.dims();
    auto d4 = af::dim4(550,366,1,1);
    TranslationMSCLayer translationMSCLayer(d,20,-20,20,-20,9,9);
    translationMSCLayer.inputBackwardSuperPostion = &img;
    translationMSCLayer.inputForwardSuperPostion = &input;

    while(translationMSCLayer.numNonZeroWeights() != 1)
    {
        translationMSCLayer.computeBackwardPath();
        translationMSCLayer.computeForwardPath();
        //Utils::showImgInWindow(translationMSCLayer.outputBackwardSuperpostion);
        std::cout<<translationMSCLayer.numNonZeroWeights()<<std::endl;
        translationMSCLayer.printWeights();
    }
    Utils::showImgInWindow(translationMSCLayer.outputForwardSuperposition);
    translationMSCLayer.printWeights();
    translationMSCLayer.printResults();
}

int main(int argc, char** argv)
{
    char t_device_name[64] = {0};
    char t_device_platform[64] = {0};
    char t_device_toolkit[64] = {0};
    char t_device_compute[64] = {0};
    af::deviceInfo(t_device_name, t_device_platform, t_device_toolkit, t_device_compute);

    printf("Device name: %s\n", t_device_name);
    printf("Platform name: %s\n", t_device_platform);
    printf("Toolkit: %s\n", t_device_toolkit);
    printf("Compute version: %s\n", t_device_compute);
/*
    float a_buff[] = {0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1};
    float b_buff[] = {0, 1, 0, 1};
    float c_buff[] = {0,0,0,0};

    array A(4, 3, a_buff);
    array B(4, b_buff);
    array C(4,c_buff);
    af_print(A);
    af_print(B);
    af_print(matmul(A, B,AF_MAT_TRANS,AF_MAT_NONE));
    af_print(where(B));
    af_print(where(C));
*/
    /*auto jak = Utils::LoadImgsFromDir("./jakka/");
    for(auto im : *jak)
    {
        Utils::showImgInWindow(im);
    }*/
    /*TranslationOptions tOpts = {20,20,21, {0.5f,1.0f,0.0f}};
    RotationOptions rOpts = {90,-90,36,{0.6f,0.8f,0.0f}};
    ScaleOptions sOpts = {0.5,2.0,16,{0.4f,0.5f,0.0f}};
    AziEleOptions aziEleOpts = {0.0f,350.0f,0.0f,45.0f,"./pauwelsMem/Clown/",".png",162,{0.6f,0.2f,0.0f}};*/
    TranslationOptions tOpts = {120,120,Utils::numTransformsFor(120.0f,-120.0f,5.0f), {0.7f,0.8f,0.0f}};
    RotationOptions rOpts = {20,-20,Utils::numTransformsFor(20,-20,2.0f),{0.3f,0.6f,0.0f}};
    ScaleOptions sOpts = {0.8,1.2,Utils::numTransformsFor(1.5f,0.5f,0.1f),{0.1f,0.2f,0.0f}};
    AziEleOptions aziEleOpts = {0.0f,350.0f,0.0f,45.0f,"./pauwelsMem/Clown/",".png",162,{0.1f,0.3f,0.0f}};
    auto img = Utils::loadSobelImg("./pauwels/translated10cm.png");//("./pauwels/clown/left_00055.png");
    //auto img = Utils::loadSobelImg("./pauwelsMem/Clown/image0_20.png");
    //auto img1 = Utils::loadSobelImg("./pauwelsMem/Clown/image0_0.png");
    Utils::showImgInWindow(img);
    auto input = translate(img,0,0);
    MSCLayerManager mscLayerManager(tOpts,rOpts,sOpts,aziEleOpts,img.dims());
    mscLayerManager.run(&input,&img);

    //runScaleMSC();
    //runRotMSC();
    //runTransMSC();
    return 0;
}