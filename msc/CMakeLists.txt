cmake_minimum_required(VERSION 2.8)
project(MapSeekingCircuit)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

# indicate we wish for the binaries to be written to the bin directory
SET(EXECUTABLE_OUTPUT_PATH ${CMAKE_SOURCE_DIR}/bin)

message(STATUS "SourcePath is ${CMAKE_SOURCE_DIR}")
# we have custom CMake configuration files, tell CMake to use them:
LIST(APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/CMakeModules)

# build items in subdirectories
add_subdirectory(src)

add_custom_target(profileRun)
add_custom_command (TARGET profileRun
                    POST_BUILD
                    WORKING_DIRECTORY ${EXECUTABLE_OUTPUT_PATH}
                    COMMAND valgrind --tool=callgrind ./Debug/MSC-run
        )
