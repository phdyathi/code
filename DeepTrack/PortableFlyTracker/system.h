// System dependent definitions and functions
// File: system.h
#include <time.h>     // for seconds()

/*************** FUNCTIONS  *******************/

extern void seedRandom(unsigned int seed);

extern double randomDbl(void);

extern double seconds();
