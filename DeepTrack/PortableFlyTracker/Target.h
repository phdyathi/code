#pragma once
#include "cv.h"
#include "cxcore.h"
#include <queue>
using namespace cv;
typedef struct Event
{
	int t;
	int event_type;
	int target_id;//����
	std::vector<int> other_targets;//����
	//double score;
};
typedef struct myPoint
{
	int t;
	double x;
	double y;
	double z;
	double score;

};
typedef struct Tracker
{
	Mat S;//particles
	Mat W;
	Mat C;
	std::vector<Mat> refs;//ÿ���ӽǵĲο�����
	std::vector<myPoint> pts;//�켣����ά����
	std::vector<Mat> pts2;//��άͶӰ
	bool is_alive;//�Ƿ񻹻�Ծ
	bool is_fixed;//�Ƿ��̶�
	int bad_frame;
	int no_update;
};

class Target
{
public:
	std::vector<Tracker> trackers;
	bool is_ambiguous;
	int true_ind;
public:
	Target(void);
	Target(const std::vector<myPoint>& pt3);
	~Target(void);
};

typedef struct GraphNode
{
	std::vector<int> tids;
	int t;
	int pre;
	int next;
};
class MyGraph
{
public:
	std::vector<GraphNode> nodes;
	Mat connectivity;
	std::vector<Point> edges;
	std::vector< std::vector<int> > con_list;//�����ڽ�����
	std::vector< std::vector<int> > con_list_reverse;//�����ڽ�����

	std::vector<int> visit;
	std::vector<int> max_id;
	std::vector<int> max_len;
	std::vector<int> max_id_reverse;
	std::vector<int> max_len_reverse;
public:
	void add_node(GraphNode n){nodes.push_back(n);};
	void add_edge(int i, int j){Point p; p.x = i; p.y = j;edges.push_back(p);};
	GraphNode get_node(int i){ return nodes[i];};
	void shortest_path(int s, int e, std::vector<int>& path);
	void construct_adj();
	void split(std::vector< std::vector<int> >& paths);
	int longest_reverse(int i);
	int longest(int i);
};
