import os
import glob
import time
import sys

startTime = time.time()

testDataDir ="./data/"
evalDir ="./eval/"

trackerPathTemplate="/flyTracker/renders/"
trackerPath = "./build/FlyTracker"

testDirs = [d for d in os.listdir(testDataDir) if os.path.isdir(os.path.join(testDataDir, d))]

for d in testDirs:
    if(os.system("sbatch ./evalFlyJob.script "+testDataDir+d+'/flyTracker/ '+evalDir+d+'.traj') != 0):
        print('Error starting job\n')
        sys.exit(-1)

endTime = time.time()

print(endTime - startTime)
