from collections import namedtuple
import  os
import argparse

parser = argparse.ArgumentParser(description='Convert')
parser.add_argument('-s', default='./txt/',help='The source directory')
parser.add_argument('-d', default='./grids/',help='The destination directory')
args = parser.parse_args()

TrajPoint = namedtuple("TrajPoint", "time x y z")
Vec3 = namedtuple("Vec3","x y z")

#output dir
outputDir = args.d
if not os.path.exists(outputDir):
    os.makedirs(outputDir)

#number of grid cells per dimension
numIntX=31
numIntY=31
numIntZ=31

#the size of the divisions (size of each voxel)
xDiv=0.645161
yDiv=0.645161
zDiv=0.645161

#global cube's minimum corner
minX=-20.0
minY=-10.0
minZ=0.0
#global cube's max corner
maxX=minX+(numIntX*xDiv)
maxY=minY+(numIntY*yDiv)
maxZ=minZ+(numIntZ*zDiv)

#the total number of frames
numFrames=2004

#filename to read
flyResultFilename = args.s

def writeDummyGrid(fileName):
    print("Writing "+fileName)
    outfile = open(fileName,'w')

    #output settings
    outfile.write("{0} {1} {2} {3} {4} {5} {6} {7} {8}\n".format(minX,minY,minY,numIntX,numIntY,numIntZ,xDiv,yDiv,zDiv))

    outfile.write("0\n")
    outfile.write("0\n")

def writeGridTxt(fileName,grid):
    print("Writing "+fileName)
    outfile = open(fileName,'w')

    #output settings
    outfile.write("{0} {1} {2} {3} {4} {5} {6} {7} {8}\n".format(minX,minY,minY,numIntX,numIntY,numIntZ,xDiv,yDiv,zDiv))
    #output number of cloud voxels
    outfile.write("{0}\n".format(len(grid)))
    #output the grid coords
    for coord in grid:
        outfile.write("{0} {1} {2}\n".format(coord.x,coord.y,coord.z))

    #output number of occlution voxels as zero
    outfile.write("0\n")

def convertToM(allTrajectories):
    for traj in allTrajectories:
        for t in range(len(traj)):
            #if there is a trajectory at that time step
            if(traj[t] != None):
                traj[t]=TrajPoint(traj[t].time,traj[t].x/100.0,traj[t].y/100.0,traj[t].z/100.0)

    return allTrajectories

def covertToGrid(allTrajectories):
    #this is a list with lists of occupied grid coords for each time step
    # time steps with no data have None
    # each grid coord entry is a Vec3 named tuple
    gridCoordsToReturn = [None]*numFrames

    for traj in allTrajectories:
        for t in traj:
            #if there is a trajectory at that time step
            if(t != None):
                #if the grid coords for that time step do not exist make it
                if(gridCoordsToReturn[t.time-1] == None):
                    gridCoordsToReturn[t.time-1] = []
                #descetize the floating point numbers
                gridX = int(round((t.x-minX)/xDiv))+1
                gridY = int(round((t.y-minY)/yDiv))+1
                gridZ = int(round((t.z-minZ)/zDiv))+1
                gridCoordsToReturn[t.time-1].append(Vec3(gridX,gridY,gridZ))

    return gridCoordsToReturn

def readTrajectory(flyResFile):
    #this is a list with lists of trajectory enties for each time step
    # time steps with no data have None
    # each entry is represented by a TrajPoint named tuple
    toReturn = [None]*numFrames
    lengthOfTraj = int(flyResFile.next())
    print("Reading in "+str(lengthOfTraj)+" trajectory entries...\n")
    for i in range(lengthOfTraj):
        row = flyResFile.next().split(" ")
        time = int(row[0])
        x = float(row[1])
        y = float(row[2])
        z = float(row[3])
        toReturn[time-1]=TrajPoint(time,x,y,z)
    return toReturn;

def readTrajFile():
    flyResFile = open(flyResultFilename,'r')
    numberOfTraj = int(flyResFile.next())
    print("Reading in "+str(numberOfTraj)+" trajectories...\n")
    #this is a list with lists of occupied grid coords for each time step
    # time steps with no data have None
    trajs = []
    #read all trajs
    for t in range(numberOfTraj):
        trajs.append(readTrajectory(flyResFile))
    #print(trajs[0])

    return trajs;

allTrajectories = readTrajFile()
allGrids = covertToGrid(convertToM(allTrajectories))
print(allGrids)
for gridInd in range(len(allGrids)):
    if(allGrids[gridInd] != None):
        writeGridTxt("{0}{1}.gridtxt".format(outputDir,gridInd),allGrids[gridInd])
    else:
        writeDummyGrid("{0}{1}.gridtxt".format(outputDir,gridInd))
