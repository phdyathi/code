#include "cv.h"
#include "highgui.h"
#include "MultiTracker.h"

//#define VISUALISE_RUNNING


using namespace cv;
//int main()
//{
//	MultiTracker t;
//	t.getParameters("param.txt");
//	t.getCurImages(3);
//	t.init_pre(3);
//	Tracker tracker = t.targets[1].trackers[0];
//	Mat X = Mat::ones(4,1,CV_64F);
//	Mat x = Mat::ones(3,1,CV_64F);
//	for(int i = 4; i < 300; i++)
//	{
//		t.getCurImages(i);
//		t.tracker_update(tracker, i);
//		X.at<double>(0,0) = tracker.pts[tracker.pts.size()-1].x;
//		X.at<double>(1,0) = tracker.pts[tracker.pts.size()-1].y;
//		X.at<double>(2,0) = tracker.pts[tracker.pts.size()-1].z;
//		x = t.pms[0]*X;
//		x = x/x.at<double>(2,0);
//		Point pt;
//		pt.x = x.at<double>(0,0);
//		pt.y = x.at<double>(1,0);
//		Mat img;
//		cvtColor(t.cur_imgs[0],img,CV_GRAY2BGR);
//		t.drawParticles(img,tracker,0);
//		Mat img1;
//		cvtColor(t.cur_imgs[1],img1,CV_GRAY2BGR);
//		//t.drawParticles(img1,tracker,1);
//		//circle(img,pt,5,Scalar(0,0,255));
//		imshow("img",img);
//		//imshow("img1",img1);
//		waitKey();
//	}
//}
int main( int argc, char *argv[] )
{
	if(argc != 3)
	{
		std::cout<<"Usage: "<<argv[0]<<" ./exp2 all.traj\n";
        return -1;
	}
    printf("OpenCV version: %i.%i\n",CV_MAJOR_VERSION,CV_MINOR_VERSION);
	MultiTracker t;
	//t.getColorReference("reference.bmp");
	t.getParameters("param.txt");
	t.filedir = argv[1];
	
	//t.readTrajs("all.traj");
	//t.eventRetrieve(3);
	//t.reTrack(224);
	//t.link_traj();
	//t.link_traj();
	int s = 0;
	t.getCurImages(s);
	while(t.cur_imgs[0].rows==0)
	{	
		s++;
		t.getCurImages(s);
	}
	if(t.cur_bgs[0].rows==0)
	{
		std::cout<<"computing background..."<<std::endl;
		for(int i=0; i < t.view_num; i++)
			t.calBackground(7,i);
	}
	t.getCurImages(s+1);
	t.init_pre(s+1);

	//for(int it = 0; it <1; it++)
	//{
		//t.cur_bgs.clear();
		//t.cur_imgs.clear();
		bool flag = true;
		int e = s+2;
		for(int i = s+2; /*i<400&&*/flag; i++)
		{

			t.getCurImages(i);
			if((t.cur_imgs[0].rows==0)) flag = false;
			else
			{
				t.track_targets(i);
				std::cout<<t.active_count<<std::endl;
				t.init_pre(i);
				int cn = 0;
				for(int k = 0; k < t.targets.size();k++)
					if(!t.targets[k].trackers[0].is_fixed) cn ++;
				std::cout<<cn<<std::endl;
				e++;
			}
		}
		t.filter_traj(20);
		for(int i = e-1; i>=s+2; i--)
		{
			t.getCurImages(i);
			t.track_targets_backwards(i);
		}
		t.link_traj();
	//}
	
	t.saveTrajs(argv[2]);
	//t.readTrajs("all.traj");
	//t.link_traj();
	//t.link_traj();
#ifdef VISUALISE_RUNNING
	for(int i = s; i<e; i++)
	{
		
		t.show_traj(i);
	}
#endif
    return 0;
}