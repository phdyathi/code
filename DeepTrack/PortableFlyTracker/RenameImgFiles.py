import os
import glob
import sys

def renameImgs(pathStr):

    def getKey(filename):
        file_text_name = os.path.splitext(os.path.basename(filename))  #you get the file's text name without extension
        file_last_num = os.path.basename(file_text_name[0]).split('_')  #you get three elements, the last one is the number. You want to sort it by this number
        nums=[]
        nums.append(file_last_num[1])
        nums.append(file_last_num[2])
        return map(int,nums)

    files = sorted(glob.glob(pathStr+'*.bmp'),key=getKey)#sorted(glob.glob('*.bmp'))#sorted(glob.glob('*.bmp'),key=getKey)
    fileNum = 0
    for file in files:
        print(file)
        print(pathStr+'img{0:0>3}.bmp'.format(fileNum))
        os.rename(file,pathStr+'img{0:0>3}.bmp'.format(fileNum))
        fileNum = fileNum+1

print 'RenameImgs'
print '=========='
print 'Number of arguments:', len(sys.argv), 'arguments.'
print 'Argument List:', str(sys.argv)
if(len(sys.argv) !=2):
    print("Usage: RenameImgFiles.py ./directoryWithImgs/\n")
    sys.exit(-1)

renameImgs(sys.argv[1])

print "Done renaming"
