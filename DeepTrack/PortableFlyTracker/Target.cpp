#include "Target.h"


Target::Target(void)
{
}

Target::Target(const std::vector<myPoint>& pt3)
{

}

Target::~Target(void)
{
}

void MyGraph::construct_adj()
{
	for(int i = 0; i < nodes.size(); i++)
	{
		std::vector<int> tmp;
		con_list_reverse.push_back(tmp);
		con_list.push_back(tmp);
	}
	connectivity = Mat::zeros(nodes.size(),nodes.size(),CV_32S);
	for(int i = 0; i < edges.size(); i++)
	{
		int a = edges[i].x;
		int b = edges[i].y;
		connectivity.at<int>(a,b) = 1;
		bool flag = true;
		for(int j = 0; j<con_list_reverse[b].size()&&flag; j++)
		{
			if(con_list_reverse[b][j]==a) flag = false;
		}
		if(flag) con_list_reverse[b].push_back(a);
		flag = true;
		for(int j = 0; j<con_list[a].size()&&flag; j++)
		{
			if(con_list[a][j]==b) flag = false;
		}
		if(flag) con_list[a].push_back(b);
	}
}

void MyGraph::split(std::vector< std::vector<int> >& paths)
{
	construct_adj();
	for(int i = 0; i < nodes.size(); i++)
	{
		max_len.push_back(-1);
		max_len_reverse.push_back(-1);
		max_id.push_back(-1);
		max_id_reverse.push_back(-1);
		visit.push_back(0);
	}
	for(int i = 0; i < nodes.size(); i++)
	{
		if(visit[i]==0)
		{
			/*if(i==10)
			{
				std::cout<<i<<std::endl;
			}*/
			std::vector<int> path;
			longest(i);
			int a = i;
			while(a!=-1)
			{
				path.push_back(a);
				visit[a] = 1;
				a = max_id[a];
			}
			paths.push_back(path);
			fill(max_len.begin(),max_len.end(),-1);
			fill(max_len_reverse.begin(),max_len_reverse.end(),-1);
		}
	}
	//std::queue<int> q1;
	//std::queue<int> q2;//reverse
	//q1.push(0);
	//while(!(q1.empty()&&q2.empty()))
	//{
	//	if(!q1.empty())
	//	{
	//		int a = q1.front();
	//		q1.pop();
	//		longest(a);
	//		std::vector<int> path;
	//		while(a!=-1)
	//		{
	//			path.push_back(a);
	//			visit[a] = 1;
	//			a = max_id[a];
	//		}
	//		paths.push_back(path);
	//		fill(max_len.begin(),max_len.end(),-1);
	//		fill(max_len_reverse.begin(),max_len_reverse.end(),-1);
	//		fill(max_id.begin(),max_id.end(),-1);
	//		fill(max_id_reverse.begin(),max_id_reverse.end(),-1);
	//		for(int i = 0; i < path.size(); i++)
	//		{
	//			for(int j = 0; j<con_list[path[i]].size(); j++)
	//			{
	//				int i1 = con_list[path[i]][j];
	//				if(visit[i1] == 0)
	//					q1.push(i1);
	//			}
	//			for(int j = 0; j<con_list_reverse[path[i]].size(); j++)
	//			{
	//				int i1 = con_list_reverse[path[i]][j];
	//				if(visit[i1] == 0)
	//					q2.push(i1);
	//			}
	//		}
	//	}
	//	if(!q2.empty())
	//	{
	//		int b = q2.front();
	//		q2.pop();
	//		longest_reverse(b);
	//		std::vector<int> path;
	//		while(b!=-1)
	//		{
	//			path.push_back(b);
	//			visit[b] = 1;
	//			b = max_id_reverse[b];
	//		}
	//		std::vector<int> path1;
	//		for(int i = path.size()-1; i>=0; i--)
	//		{
	//			path1.push_back(path[i]);
	//			for(int j = 0; j<con_list[path[i]].size(); j++)
	//			{
	//				int i1 = con_list[path[i]][j];
	//				if(visit[i1] == 0)
	//					q1.push(i1);
	//			}
	//			for(int j = 0; j<con_list_reverse[path[i]].size(); j++)
	//			{
	//				int i1 = con_list_reverse[path[i]][j];
	//				if(visit[i1] == 0)
	//					q2.push(i1);
	//			}
	//		}
	//		paths.push_back(path1);
	//		fill(max_len.begin(),max_len.end(),-1);
	//		fill(max_len_reverse.begin(),max_len_reverse.end(),-1);
	//		fill(max_id.begin(),max_id.end(),-1);
	//		fill(max_id_reverse.begin(),max_id_reverse.end(),-1);
	//	}
	//}
}
int MyGraph::longest(int i)
{
	if(con_list[i].empty())
	{
		max_len[i] = 1;
		max_id[i] = -1;
		return max_len[i];
	}
	else
	{
		int mv = 0;
		int mi = -1;
		for(int j = 0; j < con_list[i].size(); j++)
		{
			int id = con_list[i][j];
			int len;
			if(visit[id]==0)
			{
				if(max_len[id]==-1)
					len = longest(id);
				else len = max_len[id];
				if(len>mv)
				{
					mv = len;
					mi = id;
				}
			}
		}
		if(mi==-1)
		{
			max_len[i] = 1;
			max_id[i] = -1;
			return max_len[i];
		}
		else
		{
			max_len[i] = 1+max_len[mi];
			max_id[i] = mi;
			return max_len[i];
		}
	}
}
int MyGraph::longest_reverse(int i)
{
	if(con_list_reverse[i].empty())
	{
		max_len_reverse[i] = 1;
		max_id_reverse[i] = -1;
		return max_len_reverse[i];
	}
	else
	{
		int mv = 0;
		int mi = -1;
		for(int j = 0; j < con_list_reverse[i].size(); j++)
		{
			int id = con_list_reverse[i][j];
			int len;
			if(max_len_reverse[id]==-1)
				len = longest_reverse(id);
			else len = max_len_reverse[id];
			if(visit[id]==0&&len>mv)
			{
				mv = len;
				mi = id;
			}
		}
		max_len_reverse[i] = 1+mv;
		max_id_reverse[i] = mi;
		return max_len_reverse[i];
	}
}
