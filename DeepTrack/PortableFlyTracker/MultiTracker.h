#pragma once
#define NO_OVERLAP 1 //û��ϵ
#define LINKABLE1 2 //��������1��ǰ
#define LINKABLE2 3 //��������2��ǰ
#define SPLIT1 4 //��һ������������
#define SPLIT2 5 //�ڶ�������������
#define MERGE1 6 //���Ժϲ�
#define MERGE2 7 //���Ժϲ�
#define PROBLEMATIC 8 //������
#define OCC_VIEW1 1
#define OCC_VIEW2 2
#define OCC_BOTH 3
#include <fstream>
#include <iostream>
#include "cv.h"
#include <opencv2/opencv.hpp>
#include "cxcore.h"
#include "highgui.h"
#include <vector>
#include <queue>
#include "math.h"
#include <list>
#include "Target.h"
#include <string>
#include <sstream>
#include <cmath>


using namespace cv;

class MultiTracker
{
public:
	//parameters
	char* filedir;
	int particle_num;
	int patch_radius;//patch �뾶
	double sigma;//noise of the transition model
	int view_num;//�ӽǸ���

	//
	std::vector<Mat> cur_imgs;
	std::vector<Mat> pre_imgs;
	std::vector<Mat> cur_bgs;
	std::vector<Mat> pre_bgs;

	std::vector<Mat> pms;//��������

	std::vector<Target> targets;

	int active_count;
	Mat color_ref;
public:
	MultiTracker(void);
	~MultiTracker(void);
	void getParameters(char* filename);
	void calBackground(const int win, const int view);
	void calMedianImage(Mat* imglist,int win, Mat& img);
	void subImage(const Mat& m1,const Mat& m2, Mat& result);
	void readImage(const int t, const int view, Mat& img);
	void saveBgImage(const int t, const int view, const Mat& bg);
	void readBgImage(const int t, const int view, Mat& bg);//��tʱ�̵ļ�����������ͼ��
	void detectTargets(const Mat& bg, Mat& label, int thre, int size_thre, std::vector<Point2f>& pts);
	void getCurImages(int t);
	void findConnectComponent(const Mat& binary, std::vector<std::vector<Point> >& contours);//������ͨ����
	void find_unvisit(const Mat& label,int view, const std::vector<Point2f>& pts1, std::vector<Point2f>& pts2);
	void find_unvisit(const Mat& label,int view, const std::vector<Point2f>& pts1, std::vector<Point2f>& pts2, int tim);
	void init_pre(int tim);//��ʼ��tracker
	void init_tracker(const Mat& pt1, const Mat& pt2, Tracker& t, int tim);
	void update_ref(Tracker& t);
	void tracker_update(Tracker& tracker, int tim);
	void track_targets(int tim);
	void track_targets_backwards(int tim);
	void filter_traj(int len_thre);
	void link_traj();
	void show_traj(int t);
	bool find_overlap(const Target& t1, const Target& t2, int len, int pos1, int pos2);//���������켣֮����overlap��û�ҵ�����false�� �ҵ���len����overlap���ȣ�pos1��2������ʼλ��
	//void get_reference();
	int find_relation(const Target& t1, const Target& t2,Target& t3);//t3�������Ӻ����߲��ֺ��Ĺ켣
	void link_two_traj(const Target& t1, const Target& t2, Target& t3);//����������overlap�Ĺ켣
	void link_n_traj(std::vector<int> traj_ids, std::vector<Target>& ts);//�ϲ�����
	void templateMatch(const Mat& img1, const Mat& img2, const std::vector<Point2f>& pts1, std::vector<Point2f>& pts2);
	void searchMatch(const Mat& img1, const Mat& img2, const std::vector<Point2f>& pts1, const std::vector<Point2f>& pts2, std::vector<int>& match);
	int get_subpixel( const Mat& pts, const Mat& img ,Mat &value );
	double calc_ncc( const Mat& v1, const Mat& v2 );
	double calc_ncc( const Mat& v1, const Mat& v2 ,const Scalar& mean1,const Scalar& std1);
	Mat calc_fund_matrix(const Mat& p1, const Mat& p2);//����fundamenta matrix
	Mat cal_skew_matrix(Mat e);
	void triangulatePoints( const Mat& pt_left, const Mat& pt_right, const Mat& p1, const Mat& p2, Mat &pt3);
	void triangulatePoints( const Mat& pt_left, const Mat& pt_right, const Mat& p1, const Mat& p2, Mat &pt3, double a, double b, double c);
	void getData(char* filename, cv::Mat& m);
	void printMatrix(const Mat& m);
	void drawEpipolar(Mat& img, double a, double b, double c);
	void drawParticles(Mat& img, Tracker tracker, int view);
	void saveTrajs(char* filename);
	void readTrajs(char* filename);
	void draw2dtraj(Mat& img, const Mat& pts, Scalar color);
	void showGraph(Mat& img, const MyGraph graph, Scalar color);
	int fabs(int a)
	{
		if(a<0) return -a;
		else return a;
	};
	void eventRetrieve(int event_type);
	void showParticles(const Mat& S, const Mat& W, int tim, int view, int scale, char* win_name, const Mat& X1);
	void reTrack(int i);//���¸���һ��
	void calcColor(double w, double wmin, double wmax, Vec<uchar,3>& color);//����Ȩ��Ϊw��������ʾ����ɫ
	void getColorReference(char* filename)
	{
		color_ref = imread(filename,1);
	};
	double calcDistance(const Target& t1,const Target& t2, int tim, int view);//��������Ŀ����ĳһʱ��ĳһ�ӽ�ͼ���ϵľ���
};
