#include "MultiTracker.h"
#include <exception>


MultiTracker::MultiTracker(void)
{
	filedir = new char[500];
}

void MultiTracker::getParameters(char* filename)
{
	std::ifstream infile;
	infile.open(filename);
	if(!infile)
	{
		std::cerr << "error: unable to open input file: "<<filename<<std::endl;
	}
	else
	{
		while(!infile.eof())
		{
			char str[500];
			//infile>>str;
			infile.getline(str,500);
			int a = strcmp(str,"filedir");
			if(a==0)
			{
				infile.getline(filedir,500);
				//std::cout<<a<<' '<<filedir<<std::endl;
			}
			a = strcmp(str,"particlenum");
			if(a==0)
			{
				infile>>particle_num;
			}
			a = strcmp(str,"patchradius");
			if(a==0)
			{
				infile>>patch_radius;
			}
			a = strcmp(str,"sigma");
			if(a==0)
			{
				infile>>sigma;
			}
			a = strcmp(str,"viewnum");
			if(a==0)
			{
				infile>>view_num;
			}
			//std::cout<<a<<' '<<str<<std::endl;
		}
	}
	infile.close();
	for(int i = 0; i < view_num; i++)
	{
		char filename[500];
		int a = sprintf(filename,"p%d.txt",i+1);
		Mat pm(3,4,CV_64F);
		getData(filename,pm);
		pms.push_back(pm);
		//printMatrix(pm);
		//getchar();
	}
	//std::cout<<filedir<<std::endl;
	//std::cout<<particle_num<<std::endl;
	//std::cout<<patch_radius<<std::endl;
	//std::cout<<sigma<<std::endl;
}

void MultiTracker::calBackground(const int win, const int view)
{
	bool flag = true;
	int ind = 0;
	Mat img;
	std::vector<Mat> imgs;
	int half = win/2;
	while(flag)
	{
		readImage(ind,view,img);
		if(img.rows>1) flag = false;
		ind ++;
	}
	std::list<Mat> q;
	Mat* imglist = new Mat[win];
	ind --;
	for(int i = 0; i<win; i++)
	{
		//Mat img1;
		readImage(ind+i,view,img);
		imglist[i] = img;
		q.push_back(img);
	}
	/*for(int i = 0; i < win; i++)
	{
	imshow("img",imglist[i]);
	waitKey();
	}*/
	Mat bg(img.size(),CV_8U);
	calMedianImage(imglist,win,bg);
	Mat result(img.size(),CV_8U);
	for(int i = 0; i <=half; i++)
	{
		subImage(imglist[i],bg,result);
		//imshow("bg",result);
		//waitKey();
		int ti = ind+i;
		saveBgImage(ti, view, result);
	}
	flag=true;
	int end = 0;
	int cn=0;
	for(int t = ind+half+1; flag; t++)
	{

		readImage(t+half,view,img);
		if(img.rows>1)
		{
			imglist[end] = img;

		}
		else
			cn++;
		end ++;
		end = end%win;
		int cur = (end+half)%win;
		if(cn>half)
			flag = false;
		else
		{
			calMedianImage(imglist,win,bg);
			subImage(imglist[cur],bg,result);
			saveBgImage(t, view, result);
			std::cout<<t<<std::endl;
		}

		//imshow("bg",result);
		//waitKey(50);
	}
	delete [] imglist;
}

void MultiTracker::subImage(const Mat& m1,const Mat& m2, Mat& result)
{
	for(int i = 0; i < m1.rows; i++)
		for(int j = 0; j < m1.cols; j++)
		{
			int a = m1.at<uchar>(i,j);
			int b = m2.at<uchar>(i,j);
			int c = a-b;
			if(c<0) c=-c;
			result.at<uchar>(i,j) = c;
		}
}

void MultiTracker::calMedianImage(Mat* imglist,int win, Mat& img)
{
	//img must be of the same size as the images in the imglist
	std::vector<int> tmp(win,255);

	for(int i = 0; i< img.rows; i++)
		for(int j = 0; j < img.cols; j++)
		{
			//std::vector<int> tmp;
			for(int it = 0; it<win; it++)
			{
				int a = imglist[it].at<uchar>(i,j);
				//tmp.push_back(a);
				tmp[it] = a;
				//imshow("fda",*it);
				//waitKey();
			}
			std::sort(tmp.begin(),tmp.end());

			//int mid = tmp.size();
			int mid = win/2;
			//if(tmp[mid]!=0)
			//	for(int k = 0; k < tmp.size(); k++) std::cout<<tmp[k]<<std::endl;
			img.at<uchar>(i,j) = tmp[mid];
		}
}

void MultiTracker::readImage(const int t, const int view, Mat& img)
{
	char dst[500];
	for(int i =0; i < 500; i++)
		dst[i] = filedir[i];
	char src[500];
	int a;
	if(t<10)
		a = sprintf(src,"/%d/img00%d.bmp",view,t);
	else if(t<100)
		a = sprintf(src,"/%d/img0%d.bmp",view,t);
	else
		a = sprintf(src,"/%d/img%d.bmp",view,t);
	strcat(dst,src);
	std::cout<<dst<<std::endl;
	img = imread(dst,0);
	if (img.data == NULL)
		std::cout<<"Image not loaded! "<<dst<<std::endl;
}

void MultiTracker::saveBgImage(const int t, const int view, const Mat& bg)
{
	char dst[500];
	for(int i =0; i < 500; i++)
		dst[i] = filedir[i];
	char src[500];
	int a;
	if(t<10)
		a = sprintf(src,"/bg%d/img00%d.bmp",view,t);
	else if(t<100)
		a = sprintf(src,"/bg%d/img0%d.bmp",view,t);
	else
		a = sprintf(src,"/bg%d/img%d.bmp",view,t);
	strcat(dst,src);
	if (!imwrite(dst, bg))
		std::cout << "BgImg write failed: " << dst << std::endl;
}

void MultiTracker::readBgImage(const int t, const int view, Mat& bg)
{
	char dst[500];
	for(int i =0; i < 500; i++)
		dst[i] = filedir[i];
	char src[500];
	int a;
	if(t<10)
		a = sprintf(src,"/bg%d/img00%d.bmp",view,t);
	else if(t<100)
		a = sprintf(src,"/bg%d/img0%d.bmp",view,t);
	else
		a = sprintf(src,"/bg%d/img%d.bmp",view,t);
	strcat(dst,src);
	bg = imread(dst,0);
}

void MultiTracker::getCurImages(int t)
{
	//view_num = 2;
	bool is_empty = cur_imgs.empty();
	if(!is_empty)
	{
		pre_imgs = cur_imgs;
		pre_bgs = cur_bgs;
	}
	for(int i = 0; i < view_num; i++)
	{
		Mat img,bg;
		readImage(t,i,img);
		readBgImage(t,i,bg);
		if(is_empty)
		{
			cur_imgs.push_back(img);
			cur_bgs.push_back(bg);
			Mat pre_img;
			Mat pre_bg;
			readImage(t-1,i,pre_img);
			readBgImage(t - 1,i,pre_bg);
			pre_imgs.push_back(pre_img);
			pre_bgs.push_back(pre_bg);
		}
		else
		{
			cur_imgs[i] = img;
			cur_bgs[i] = bg;
		}
	}
}

void MultiTracker::detectTargets(const Mat& bg, Mat& label, int thre, int size_thre, std::vector<Point2f>& pts)
{
	Mat binary;
	label = Mat::zeros(bg.size(),CV_32S);
	threshold(bg,binary,thre,255,CV_THRESH_BINARY);
	std::vector<std::vector<Point> > contours;
	std::vector<Vec4i> hierarchy;
	findConnectComponent( binary, contours);
	Mat test;
	cvtColor(binary,test,CV_GRAY2BGR);
	for(int i = 0; i < contours.size(); i++)
	{
		double area = contours[i].size();
		if(area>size_thre)
		{
			Point2f center;
			double sumx = 0;
			double sumy = 0;
			for(int j = 0; j<contours[i].size(); j++)
			{
				sumx += contours[i][j].x+1;
				sumy += contours[i][j].y+1;
				label.at<int>(contours[i][j].x,contours[i][j].y) = pts.size()+1;
			}
			int cn = contours[i].size();
			sumx = sumx/double(cn);
			sumy = sumy/double(cn);
			//minEnclosingCircle(contours[i],center,radius);
			Point pt;
			pt.y = sumx;
			pt.x = sumy;
			pts.push_back(pt);
			//int ind = label.at<int>(pt.y-1,pt.x-1);
			//std::cout<<ind<<' '<<i<<' '<<pts.size()<<std::endl;
			//circle(test,pt,2,Scalar(0,0,255),2);
		}
	}
	/*for(int i = 0; i < pts.size(); i++)
	{
	int r = pts[i].y-1;
	int c = pts[i].x-1;
	int id = label.at<int>(r,c);
	std::cout<<id<<' '<<i<<std::endl;
	}*/
}

void MultiTracker::findConnectComponent(const Mat& binary, std::vector<std::vector<Point> >& contours)
{
	std::queue<Point> q;
	Mat visit = Mat::zeros(binary.size(),CV_32S);
	int indr[4] = {-1,0,1,0};
	int indc[4] = {0,-1,0,1};
	for(int i = 20; i < binary.rows-20; i++)
		for(int j = 20; j< binary.cols-20; j++)
		{
			int a = binary.at<uchar>(i,j);
			if(visit.at<int>(i,j)==0&&a>100)
			{
				std::vector<Point> tmp;
				q.push(Point(i,j));
				tmp.push_back(Point(i,j));
				while(!q.empty())
				{
					Point t = q.front();
					q.pop();
					for(int k = 0; k < 4; k++)
					{
						int r = t.x+indr[k];
						int c = t.y+indc[k];
						if(r>=0&&r<binary.rows&&c>=0&&c<binary.cols)
						{
							int b = binary.at<uchar>(r,c);
							if(b>100&&visit.at<int>(r,c) == 0)
							{
								q.push(Point(r,c));
								tmp.push_back(Point(r,c));
								visit.at<int>(r,c) = 1;
							}
						}
					}
				}
				contours.push_back(tmp);
			}
		}
		/*Mat test;
		cvtColor(binary,test,CV_GRAY2BGR);
		for(int i = 0; i < contours.size(); i++)
		{
		for(int j = 0; j<contours[i].size(); j++)
		{
		Point pt = contours[i][j];
		Vec<uchar,3> a;
		a[0] = 255;
		a[1] = 0;
		a[2] = 0;
		test.at<Vec<uchar,3>>(pt.x,pt.y) = a;
		}
		imshow("test",test);
		waitKey();
		}*/
}

void MultiTracker::find_unvisit(const Mat& label,int view, const std::vector<Point2f>& pts1, std::vector<Point2f>& pts2)
{
	/*for(int i = 0; i < pts1.size(); i++)
	{
	int r = pts1[i].y-1;
	int c = pts1[i].x-1;
	int id = label.at<int>(r,c);
	std::cout<<id<<' '<<i<<std::endl;
	}*/
	Mat img;
	cvtColor(pre_imgs[view],img,CV_GRAY2BGR);
	for(int i = 0; i < targets.size(); i++)
	{
		int cn = 0;
		for(int j = 0; j < targets[i].trackers.size(); j++)
		{
			if(targets[i].trackers[j].is_alive)
			{
				targets[i].true_ind = j;
				cn++;
			}
		}
		if(cn==1)
		{
			targets[i].is_ambiguous = false;
		}
	}
	std::vector<int> visit(pts1.size(),0);
	//int ind1[9] = {-1,-1,-1,0,1,1,1,0,0};
	//int ind2[9] = {-1,0,1,1,1,0,-1,-1,0};
	std::vector<int> ind1;
	std::vector<int> ind2;
	for(int i = -3; i<3; i++)
		for(int j=-3; j<3; j++)
		{
			ind1.push_back(i);
			ind2.push_back(j);
		}
		for(int i = 0; i < targets.size(); i++)
		{
			int cn1 = 0;

			for(int ind = 0; ind<targets[i].trackers.size(); ind++)
			{

				if(targets[i].trackers[ind].is_alive)
				{
					int s = targets[i].trackers[ind].pts.size()-2;
					Mat X = Mat::ones(4,1,CV_64F);
					X.at<double>(0,0) = targets[i].trackers[ind].pts[s].x;
					X.at<double>(1,0) = targets[i].trackers[ind].pts[s].y;
					X.at<double>(2,0) = targets[i].trackers[ind].pts[s].z;
					Mat x = pms[view]*X;
					x = x/x.at<double>(2,0);
					int c1 = int(x.at<double>(0,0)-1);
					int r1 = int(x.at<double>(1,0)-1);
					int l;
					bool f = true;
					for(int i1 = 0; i1<ind1.size()&&f; i1++)
					{
						int r2 = r1+ind1[i1];
						int c2 = c1+ind2[i1];
						if(r2>0&&r2<label.rows&&c2>0&&c2<label.cols)
							l = label.at<int>(r2,c2);
						if(l>0&&l<=visit.size()) {visit[l-1] = 1; /*f = false;*/}
					}
					Point pt,pt1;
					//std::cout<<l<<std::endl;
					pt1.x = int(pts1[l-1].x-1);
					pt1.y = int(pts1[l-1].y-1);
					pt.x = c1;
					pt.y = r1;
					circle(img,pt,6,Scalar(0,0,255));
					circle(img,pt1,8,Scalar(255,0,0));
					cn1 ++;
				}
			}
			/*std::ostringstream ost;
			ost << cn1;
			string temp(ost.str());
			putText(img,temp,pt,1,1,Scalar(0,0,255));*/
		}
		imshow("img",img);
		waitKey(50);
		int count = 0;
		for(int i = 0; i < visit.size(); i++)
		{
			//std::cout<<visit[i]<<std::endl;
			if(visit[i]==0)
			{
				pts2.push_back(pts1[i]);
				count ++;
			}
		}
		std::cout<<"unvisit "<<count<<std::endl;
}

void MultiTracker::find_unvisit(const Mat& label,int view, const std::vector<Point2f>& pts1, std::vector<Point2f>& pts2, int tim)
{
	/*for(int i = 0; i < pts1.size(); i++)
	{
	int r = pts1[i].y-1;
	int c = pts1[i].x-1;
	int id = label.at<int>(r,c);
	std::cout<<id<<' '<<i<<std::endl;
	}*/
	std::ofstream outfile1;

	std::ofstream outfile2;
	if(tim==10)
	{
        char f1[500];
        sprintf(f1,"%s/visit.txt",filedir);
        char f2[500];
        sprintf(f2,"%s/unvisit.txt",filedir);
		outfile1.open(f1);
		outfile2.open(f2);
	}
	Mat img;
	cvtColor(pre_imgs[view],img,CV_GRAY2BGR);
	for(int i = 0; i < targets.size(); i++)
	{
		int cn = 0;
		for(int j = 0; j < targets[i].trackers.size(); j++)
		{
			if(targets[i].trackers[j].is_alive)
			{
				targets[i].true_ind = j;
				cn++;
			}
		}
		if(cn==1)
		{
			targets[i].is_ambiguous = false;
		}
	}
	std::vector<int> visit(pts1.size(),0);
	std::vector<int> visit1(pts1.size(),0);
	std::vector<int> visit_cn(pts1.size(),0);
	//int ind1[9] = {-1,-1,-1,0,1,1,1,0,0};
	//int ind2[9] = {-1,0,1,1,1,0,-1,-1,0};
	std::vector<int> ind1;
	std::vector<int> ind2;
	for(int i = -3; i<3; i++)
		for(int j=-3; j<3; j++)
		{
			ind1.push_back(i);
			ind2.push_back(j);
		}
		for(int i = 0; i < targets.size(); i++)
		{
			int cn1 = 0;
			int actnum = 0;
			for(int ind = 0; ind<targets[i].trackers.size(); ind++)
			{
				int a = targets[i].trackers[ind].pts[0].t;
				int s = tim-a;
				if(targets[i].trackers[ind].is_alive&&s>=0&&s<targets[i].trackers[ind].pts.size())
				{
					targets[i].trackers[ind].no_update++;
					Mat X = Mat::ones(4,1,CV_64F);
					X.at<double>(0,0) = targets[i].trackers[ind].pts[s].x;
					X.at<double>(1,0) = targets[i].trackers[ind].pts[s].y;
					X.at<double>(2,0) = targets[i].trackers[ind].pts[s].z;
					Mat x = pms[view]*X;
					x = x/x.at<double>(2,0);
					double c1 = x.at<double>(0,0);
					double r1 = x.at<double>(1,0);
					int l=0;
					bool f = true;

					for(int i1 = 0; i1<ind1.size()&&f; i1++)
					{
						int r2 = int(r1+ind1[i1])-1;
						int c2 = int(c1+ind2[i1])-1;
						if(r2>0&&r2<label.rows&&c2>0&&c2<label.cols)
							l = label.at<int>(r2,c2);
						if(l>0&&l<=visit.size()) {visit[l-1] = 1; f = false;visit1[l-1] = i;visit_cn[l-1]++;}
					}
					if(l>0&&l<=visit.size()&&(!targets[i].trackers[ind].is_fixed))
					{
						Point pt,pt1;
						//std::cout<<l<<std::endl;
						pt1.x = int(pts1[l-1].x)-1;
						pt1.y = int(pts1[l-1].y)-1;
						pt.x = c1;
						pt.y = r1;
						circle(img,pt,6,Scalar(0,0,255));
						circle(img,pt1,8,Scalar(255,0,0));
						if(tim==10)
						{
							outfile1<<pt1.x<<' '<<pt1.y<<std::endl;
						}
						cn1 ++;
					}

				}
			}
			/*std::ostringstream ost;
			ost << cn1;
			string temp(ost.str());
			putText(img,temp,pt,1,1,Scalar(0,0,255));*/
		}

		int count = 0;
		for(int i = 0; i < visit.size(); i++)
		{
			//std::cout<<visit[i]<<std::endl;
			if(visit[i]==0)
			{
				pts2.push_back(pts1[i]);
				Point pt;
				pt.x = int(pts1[i].x-1);
				pt.y = int(pts1[i].y-1);
				circle(img,pt,7,Scalar(0,255,0));
				if(tim==10)
				{
					outfile2<<pt.x<<' '<<pt.y<<std::endl;
				}
				count ++;
			}
			if(visit_cn[i]==1)
			{
				int ti = visit1[i];

				for(int i = 0; i < targets[ti].trackers.size(); i++)
				{
					if(targets[ti].trackers[i].no_update>5&&targets[ti].trackers[i].is_alive&&targets[ti].trackers[i].pts[targets[ti].trackers[i].pts.size()-1].score>0.1*particle_num)
						update_ref(targets[ti].trackers[i]);
				}
			}
		}
#ifdef  VISUALISE_RUNNING
		imshow("img",img);
		waitKey(50);
		if(tim==10)
		{
			outfile1.close();
			outfile2.close();
		}
#endif
		//std::cout<<count<<std::endl;
}
void MultiTracker::init_pre(int tim)
{
	int view1 = 0;
	int view2 = 1;//�����ӽ�������ʼ��tracker
	double epipolar_thre = 3;
	Mat F = calc_fund_matrix(pms[view1],pms[view2]);
	//printMatrix(F);
	//getchar();
	std::vector<Point2f> pts0;
	std::vector<Point2f> pts00;
	std::vector<Point2f> pts01;
	std::vector<Point2f> pts10;
	std::vector<Point2f> pts11;
	Mat label;
	int thre = 10;
	int size_thre = 5;
	detectTargets(pre_bgs[view1],label,thre,size_thre,pts0);
	find_unvisit(label,view1, pts0, pts00,tim-1);
	detectTargets(cur_bgs[view1],label,thre,size_thre,pts01);
	detectTargets(pre_bgs[view2],label,thre,size_thre,pts10);
	detectTargets(cur_bgs[view2],label,thre,size_thre,pts11);

	std::vector<int> match1;
	std::vector<int> match2;
	searchMatch(pre_bgs[view1],cur_bgs[view1],pts00,pts01,match1);
	searchMatch(pre_bgs[view2],cur_bgs[view2],pts10,pts11,match2);
	Mat pt1 = Mat::ones(3,1,CV_64F);
	Mat pt2 = Mat::ones(3,1,CV_64F);
	Mat pt3 = Mat::ones(3,1,CV_64F);
	Mat pt4 = Mat::ones(3,1,CV_64F);
	Mat ad,ad1;
	cvtColor(cur_imgs[view1],ad,CV_GRAY2BGR);
			cvtColor(cur_imgs[view2],ad1,CV_GRAY2BGR);

	for(int i = 0; i < pts00.size(); i++)
	{
		pt1.at<double>(0,0) = pts00[i].x;
		pt1.at<double>(1,0) = pts00[i].y;
		//printMatrix(pms[0]);
		//printMatrix(pms[1]);
		Mat tmp = F*pt1;
		double a =tmp.at<double>(0,0);
		double b =tmp.at<double>(1,0);
		double c =tmp.at<double>(2,0);

		bool flag = false;
		//imshow("ad",ad);
		//waitKey();
		Target target;
		if(match1[i]!=-1)
		{

			pt3.at<double>(0,0) = pts01[match1[i]].x;
			pt3.at<double>(1,0) = pts01[match1[i]].y;
			Mat tmp1 = F*pt3;
			double a1 =tmp1.at<double>(0,0);
			double b1 =tmp1.at<double>(1,0);
			double c1 =tmp1.at<double>(2,0);
			for(int j = 0; j < pts10.size(); j++)
			{
				pt2.at<double>(0,0) = pts10[j].x;
				pt2.at<double>(1,0) = pts10[j].y;
				double dist = fabs(pt2.at<double>(0,0)*a+pt2.at<double>(1,0)*b+c);
				dist = dist/sqrt(a*a+b*b);
				if(dist<epipolar_thre)
				{
					int ind = match2[j];
					if(ind!=-1)
					{
						pt4.at<double>(0,0) = pts11[ind].x;
						pt4.at<double>(1,0) = pts11[ind].y;
						double dist = fabs(pt4.at<double>(0,0)*a1+pt4.at<double>(1,0)*b1+c1);
						dist = dist/sqrt(a1*a1+b1*b1);
						if(dist<epipolar_thre)
						{
							Mat pt31 = Mat::ones(4,1,CV_64F);
							Mat pt32 = Mat::ones(4,1,CV_64F);

							triangulatePoints(pt1,pt2,pms[view1],pms[view2],pt31);
							triangulatePoints(pt3,pt4,pms[view1],pms[view2],pt32);
							/*Mat x1 = pms[0]*pt31;
							x1 = x1/x1.at<double>(2,0);
							double dx = x1.at<double>(0,0)-pt1.at<double>(0,0);
							double dy = x1.at<double>(1,0)-pt1.at<double>(1,0);
							double dist = sqrt(dx*dx+dy*dy);
							std::cout<<dist<<std::endl;*/
							Tracker tra;
							/*Point cp1,cp2;
							cp1.x = x1.at<double>(0,0);
							cp1.y = x1.at<double>(1,0);
							cp2.x = pt4.at<double>(0,0);
							cp2.y = pt4.at<double>(1,0);
							circle(ad,cp1,5,Scalar(0,0,255));
							circle(ad1,cp2,5,Scalar(0,0,255));*/
							init_tracker(pt32,pt31,tra,tim);
							target.trackers.push_back(tra);
							flag = true;

						}
					}
				}
			}
		}
		if(target.trackers.size()>0)
			targets.push_back(target);
	}
}

void MultiTracker::update_ref(Tracker& t)
{
	int cn = 0;
	for(double i = -patch_radius; i<=patch_radius; i++)
		for(double j = -patch_radius; j<=patch_radius; j++)
		{
			if(sqrt(i*i+j*j)<=patch_radius) cn++;
		}
		Mat ptm(2,cn,CV_64F);
		Mat ptm1(2,cn,CV_64F);
		int cn1 = 0;
		for(double i = -patch_radius; i<=patch_radius; i++)
			for(double j = -patch_radius; j<=patch_radius; j++)
			{
				if(sqrt(i*i+j*j)<=patch_radius)
				{
					ptm.at<double>(0,cn1) = i;
					ptm.at<double>(1,cn1) = j;
					cn1++;
				}
			}
		Mat pt1 = Mat::ones(4,1,CV_64F);
		pt1.at<double>(0,0) = t.pts[t.pts.size()-1].x;
		pt1.at<double>(1,0) = t.pts[t.pts.size()-1].y;
		pt1.at<double>(2,0) = t.pts[t.pts.size()-1].z;
		t.refs.clear();
		for(int i = 0; i < view_num; i++)
		{
			Mat p2 = pms[i]*pt1;
			p2 = p2/p2.at<double>(2,0);
			ptm1.row(0) = ptm.row(0)+p2.at<double>(0,0);
			ptm1.row(1) = ptm.row(1)+p2.at<double>(1,0);
			Mat ref(1,ptm1.cols,CV_64F);
			get_subpixel(ptm1,cur_bgs[i],ref);
			t.refs.push_back(ref);
		}
		t.no_update = 0;
}

void MultiTracker::init_tracker(const Mat& pt1, const Mat& pt2, Tracker& t, int tim)
{
	int cn = 0;
	for(double i = -patch_radius; i<=patch_radius; i++)
		for(double j = -patch_radius; j<=patch_radius; j++)
		{
			if(sqrt(i*i+j*j)<=patch_radius) cn++;
		}
		Mat ptm(2,cn,CV_64F);
		Mat ptm1(2,cn,CV_64F);
		int cn1 = 0;
		for(double i = -patch_radius; i<=patch_radius; i++)
			for(double j = -patch_radius; j<=patch_radius; j++)
			{
				if(sqrt(i*i+j*j)<=patch_radius)
				{
					ptm.at<double>(0,cn1) = i;
					ptm.at<double>(1,cn1) = j;
					cn1++;
				}
			}

		for(int i = 0; i < view_num; i++)
		{
			Mat p2 = pms[i]*pt1;
			p2 = p2/p2.at<double>(2,0);
			ptm1.row(0) = ptm.row(0)+p2.at<double>(0,0);
			ptm1.row(1) = ptm.row(1)+p2.at<double>(1,0);
			Mat ref(1,ptm1.cols,CV_64F);
			get_subpixel(ptm1,cur_bgs[i],ref);
			t.refs.push_back(ref);
			/*Mat ad;
			cvtColor(cur_imgs[i],ad,CV_GRAY2BGR);

			for(int j = 0; j<ptm1.cols; j++)
			{
				Point pt22;
				double a = ptm.at<double>(0,j);
				double b = ptm.at<double>(1,j);
				if(sqrt(a*a+b*b)>patch_radius-1)
				{
					pt22.x = ptm1.at<double>(0,j);
					pt22.y = ptm1.at<double>(1,j);
					circle(ad,pt22,0,Scalar(0,0,255),1);
				}
			}

			imshow("ad",ad);
			waitKey();*/
		}
		//pt1 is the 3D location at t, pt2 is the 3D location at t-1
		Mat X(6,1,CV_64F);
		X.at<double>(0,0) = pt1.at<double>(0,0);
		X.at<double>(1,0) = pt1.at<double>(1,0);
		X.at<double>(2,0) = pt1.at<double>(2,0);
		X.at<double>(3,0) = pt2.at<double>(0,0);
		X.at<double>(4,0) = pt2.at<double>(1,0);
		X.at<double>(5,0) = pt2.at<double>(2,0);
		repeat(X,1,particle_num,t.S);
		t.W = Mat::ones(1,particle_num,CV_64F);
		t.C = Mat::ones(1,particle_num,CV_64F);
		t.W = t.W/double(particle_num);
		t.C.at<double>(0,0) = t.W.at<double>(0,0);
		for(int i = 1; i < t.W.cols; i++)
		{
			t.C.at<double>(0,i) = t.C.at<double>(0,i-1) + t.W.at<double>(0,i);
		}
		myPoint p1;
		p1.x = pt1.at<double>(0,0);
		p1.y = pt1.at<double>(1,0);
		p1.z = pt1.at<double>(2,0);
		p1.score = 0;
		p1.t = tim;
		myPoint p2;
		p2.x = pt2.at<double>(0,0);
		p2.y = pt2.at<double>(1,0);
		p2.z = pt2.at<double>(2,0);
		p2.score = 0;
		p2.t = tim-1;
		t.pts.push_back(p2);
		t.pts.push_back(p1);
		t.is_alive = true;
		t.is_fixed = false;
		t.bad_frame = 0;
		t.no_update = 0;
}

void MultiTracker::tracker_update(Tracker& tracker, int tim)
{
	int effect_particle = 0;//number of effective particles
	//std::vector<Point2f> tmp;
	std::vector<double> ra;//distances to the center
	int cn1 = 0;
	for(double i = -patch_radius; i<=patch_radius; i++)
		for(double j = -patch_radius; j<=patch_radius; j++)
		{
			if(sqrt(i*i+j*j)<=patch_radius) {cn1++;ra.push_back(sqrt(i*i+j*j));}
		}
		Mat ptm(2,cn1,CV_64F);
		Mat ptm1(2,cn1,CV_64F);
		Mat pf(1,cn1,CV_64F);
		double a1 = 0.9;
		double a2 = -log(0.55/a1)/(patch_radius*patch_radius);
		double pfsum = 0;
		cn1 = 0;
		for(double i = -patch_radius; i<=patch_radius; i++)
			for(double j = -patch_radius; j<=patch_radius; j++)
			{
				if(sqrt(i*i+j*j)<=patch_radius)
				{
					ptm.at<double>(0,cn1) = i;
					ptm.at<double>(1,cn1) = j;
					pf.at<double>(0,cn1) = exp(-ra[cn1]);
					pfsum += pf.at<double>(0,cn1);
					cn1++;
				}
			}

	//int effect_particle = 0;//number of effective particles
	//std::vector<Point2f> tmp;
	//std::vector<double> ra;//distances to the center
	//for(double i = -patch_radius; i<=patch_radius; i++)
	//	for(double j = -patch_radius; j<=patch_radius; j++)
	//	{
	//		if(sqrt(i*i+j*j)<=patch_radius) tmp.push_back(Point2f(i,j));
	//		ra.push_back(sqrt(i*i+j*j));
	//	}
	//	Mat ptm(2,tmp.size(),CV_64F);
	//	Mat ptm1(2,tmp.size(),CV_64F);
	//	Mat pf(1,tmp.size(),CV_64F);
	//	double a1 = 0.9;
	//	double a2 = -log(0.55/a1)/(patch_radius*patch_radius);
	//	double pfsum = 0;
	//	for(int i = 0; i < tmp.size(); i++)
	//	{
	//		ptm.at<double>(0,i) = tmp[i].x;
	//		ptm.at<double>(1,i) = tmp[i].y;
	//		pf.at<double>(0,i) = exp(-ra[i]);
	//		pfsum += pf.at<double>(0,i);
	//		//std::cout<<pf.at<double>(0,i)<<std::endl;
	//	}
		pf = pf/pfsum;
		/////////
		Mat mtx(1,tracker.S.cols,CV_64F);
		randu(mtx, Scalar(0), Scalar(1));

		Mat A = Mat::zeros(6,6,CV_64F);
		A.at<double>(0,0) = 2;
		A.at<double>(1,1) = 2;
		A.at<double>(2,2) = 2;
		A.at<double>(0,3) = -1;
		A.at<double>(1,4) = -1;
		A.at<double>(2,5) = -1;
		A.at<double>(3,0) = 1;
		A.at<double>(4,1) = 1;
		A.at<double>(5,2) = 1;
		Mat mtx1(tracker.S.size(),CV_64F);
		randn(mtx1, Scalar(0), Scalar(sigma));
		mtx1(Range(3,6),Range::all()).setTo(0);
		//po.printMatrix(mtx1);
		Mat S1 = A*tracker.S+mtx1;
		Mat X = Mat::ones(4,1,CV_64F);
		Mat ncc = Mat::zeros(1,S1.cols,CV_64F);
		Mat ncc1 = Mat::zeros(1,S1.cols,CV_64F);
		Mat ncc2 = Mat::zeros(1,S1.cols,CV_64F);
		Mat sil1 = Mat::zeros(1,S1.cols,CV_64F);
		Mat sil2 = Mat::zeros(1,S1.cols,CV_64F);
		Mat sil = Mat::zeros(1,S1.cols,CV_64F);
		Mat W1 = Mat::zeros(1,S1.cols,CV_64F);
		Mat W2 = Mat::zeros(1,S1.cols,CV_64F);
		Mat v(1,ptm1.cols,CV_64F);
		std::vector<Scalar> means;
		std::vector<Scalar> stds;
		for(int i = 0; i < tracker.refs.size(); i++)
		{
			Scalar mean1;
			Scalar std1;
			meanStdDev(tracker.refs[i],mean1,std1);
			means.push_back(mean1);
			stds.push_back(std1);
		}
		for(int i = 0; i < S1.cols; i++)
		{
			X.at<double>(0,0) = S1.at<double>(0,i);
			X.at<double>(1,0) = S1.at<double>(1,i);
			X.at<double>(2,0) = S1.at<double>(2,i);

			bool flag = true;
			for(int j = 0; j < view_num; j++)
			{
				double cn = 0;

				Mat x = pms[j]*X;
				x = x/x.at<double>(2,0);
				if(x.at<double>(0,0)<0||x.at<double>(0,0)>cur_imgs[j].cols||x.at<double>(1,0)<0||x.at<double>(1,0)>cur_imgs[j].rows)
					flag = false;
				ptm1.row(0) = ptm.row(0)+x.at<double>(0,0);
				ptm1.row(1) = ptm.row(1)+x.at<double>(1,0);
				get_subpixel(ptm1,cur_bgs[j],v);

				for(int k = 0; k < v.cols; k++)
				{
					if(v.at<double>(0,k)>10)
					{
						//W2.at<double>(0,i) = W2.at<double>(0,i)+pf.at<double>(0,k);
						cn = cn+pf.at<double>(0,k);
					}
					//else W2.at<double>(0,k) = W2.at<double>(0,k)*(1-pf.at<double>(0,k));
				}


				W2.at<double>(0,i) += cn;
				double nc = calc_ncc(v,tracker.refs[j],means[j],stds[j]);
				if(0.7*cn+0.3*nc<0.3*0.7+0.2*0.3) flag = false;
				ncc.at<double>(0,i) = nc + ncc.at<double>(0,i);
				sil.at<double>(0,i) = cn + sil.at<double>(0,i);
				if(j==0)
				{
					ncc1.at<double>(0,i) += nc;
					sil1.at<double>(0,i) += cn;
				}
				if(j==1)
				{
					ncc2.at<double>(0,i) += nc;
					sil2.at<double>(0,i) += cn;
				}
			}
			if(flag) effect_particle++;
			//if(flag) W2.at<double>(0,i) = 1;
			//else W2.at<double>(0,i) = 0.5;
		}
		W2 = W2/double(view_num);
		//std::cout<<effect_particle<<std::endl;
		//printMatrix(W2);
		ncc = ncc/double(view_num);
		sil = sil/double(view_num);
		double wsum1 = 0;
		double wsum2 = 0;

		for(int i = 0; i < ncc.cols; i++)
		{
			//W1.at<double>(0,i) = exp(3+30*ncc.at<double>(0,i))*W2.at<double>(0,i);
			W1.at<double>(0,i) = exp(3+30*ncc.at<double>(0,i)+40*W2.at<double>(0,i));
			W2.at<double>(0,i) = 3+30*ncc.at<double>(0,i)+10*W2.at<double>(0,i);
			wsum1 += W1.at<double>(0,i);
			wsum2 += W2.at<double>(0,i);
		}
		W1 = W1/wsum1;
		W2 = W2/wsum2;

		tracker.S = S1;
		tracker.W = W1;
		tracker.C.at<double>(0,0) = W1.at<double>(0,0);

		for(int i = 1; i < W1.cols; i++)
		{
			tracker.C.at<double>(0,i) = tracker.C.at<double>(0,i-1) + W1.at<double>(0,i);
		}
		Mat Sa = Mat::zeros(tracker.S.size(),CV_64F);
		for(int i = 0; i < mtx.cols; i++)//resampling
		{
			int j = 0;
			while(j<tracker.C.cols&&mtx.at<double>(0,i)>=tracker.C.at<double>(0,j))
			{
				j++;
			}
			Mat tp = Sa.col(i);
			(tracker.S.col(j)).copyTo(tp);
		}
		tracker.S = Sa;
		tracker.W = 1/double(Sa.cols);
		//tracker
		myPoint pt;
		double sum_x = 0;
		double sum_y = 0;
		double sum_z = 0;
		pt.t = tim;
		pt.score = effect_particle;
		if(effect_particle<0.05*particle_num) tracker.bad_frame++;
		if(effect_particle>0.2*particle_num) tracker.bad_frame = 0;
		Mat X1 = Mat::zeros(4,1,CV_64F);
		for(int i = 0; i < tracker.S.cols; i++)
		{
			X1.at<double>(0,0) += tracker.S.at<double>(0,i)*tracker.W.at<double>(0,i);
			X1.at<double>(1,0) += tracker.S.at<double>(1,i)*tracker.W.at<double>(0,i);
			X1.at<double>(2,0) += tracker.S.at<double>(2,i)*tracker.W.at<double>(0,i);
		}
		//printMatrix(X1);
		for(int i = 1; i < tracker.W.cols; i++)
		{
			sum_x += tracker.S.at<double>(0,i)*tracker.W.at<double>(0,i);
			sum_y += tracker.S.at<double>(1,i)*tracker.W.at<double>(0,i);
			sum_z += tracker.S.at<double>(2,i)*tracker.W.at<double>(0,i);
			tracker.C.at<double>(0,i) = tracker.C.at<double>(0,i-1) + tracker.W.at<double>(0,i);
		}

		pt.x = X1.at<double>(0,0);
		pt.y = X1.at<double>(1,0);
		pt.z = X1.at<double>(2,0);
		if(tim>tracker.pts[tracker.pts.size()-1].t)
			tracker.pts.push_back(pt);
		//myPoint pt3 = tracker.pts[tracker.pts.size()-1];
		//std::cout<<pt.x<<' '<<pt.y<<' '<<pt.z<<std::endl;
		if(tim<tracker.pts[0].t)
		{
			std::vector<myPoint> pts1;
			pts1.push_back(pt);
			for(int i = 0; i < tracker.pts.size(); i++)
			{
				pts1.push_back(tracker.pts[i]);
			}
			tracker.pts = pts1;
		}
		X1.at<double>(3,0) = 1;
		/*showParticles(S1,sil1,tim,0,16,"sil1",X1);
		showParticles(S1,sil2,tim,1,16,"sil2",X1);
		showParticles(S1,ncc1,tim,0,16,"ncc1",X1);
		showParticles(S1,ncc2,tim,1,16,"ncc2",X1);
		showParticles(S1,W2,tim,0,16,"11",X1);
		showParticles(S1,W2,tim,1,16,"22",X1);
		waitKey();*/
}

void MultiTracker::track_targets(int tim)
{
	active_count = 0;
	for(int i = 0; i<targets.size(); i++)
	{
		for(int j = 0; j<targets[i].trackers.size(); j++)
		{
			if(targets[i].trackers[j].is_alive&&(!targets[i].trackers[j].is_fixed))
			{
				tracker_update(targets[i].trackers[j],tim);
				if(targets[i].trackers[j].bad_frame>=5) targets[i].trackers[j].is_alive = false;
				else active_count++;
			}
		}
	}
}

void MultiTracker::track_targets_backwards(int tim)
{

	//for(int i = 0; i < targets.size(); i++)
	//std::cout<<targets[i].trackers[0].is_alive<<std::endl;
	int cn = 0;
	for(int i = 0; i < targets.size(); i++)
	{
		int begin = targets[i].trackers[0].pts[0].t;
		//std::cout<<begin<<' '<<tim<<std::endl;
		if(begin==tim)
		{
			myPoint pt1 = targets[i].trackers[0].pts[0];
			myPoint pt2 = targets[i].trackers[0].pts[1];
			Mat X(6,1,CV_64F);
			X.at<double>(0,0) = pt1.x;
			X.at<double>(1,0) = pt1.y;
			X.at<double>(2,0) = pt1.z;
			X.at<double>(3,0) = pt2.x;
			X.at<double>(4,0) = pt2.y;
			X.at<double>(5,0) = pt2.z;
			repeat(X,1,particle_num,targets[i].trackers[0].S);
			targets[i].trackers[0].W = Mat::ones(1,particle_num,CV_64F);
			targets[i].trackers[0].C = Mat::ones(1,particle_num,CV_64F);
			targets[i].trackers[0].W = targets[i].trackers[0].W/double(particle_num);
			targets[i].trackers[0].C.at<double>(0,0) = targets[i].trackers[0].W.at<double>(0,0);
			//std::cout<<targets[i].trackers[0].W.cols<<' '<<targets[i].trackers[0].C.cols<<targets[i].trackers[0].pts[0].t<<std::endl;
			for(int j = 1; j < targets[i].trackers[0].W.cols; j++)
			{
				//std::cout<<i<<std::endl;
				targets[i].trackers[0].C.at<double>(0,j) = targets[i].trackers[0].C.at<double>(0,j-1) + targets[i].trackers[0].W.at<double>(0,j);
			}
			targets[i].trackers[0].is_alive = true;
			targets[i].trackers[0].bad_frame = 0;
		}
		else
		{
			if(targets[i].trackers[0].is_alive == true)
			{
				tracker_update(targets[i].trackers[0],tim);
				if(targets[i].trackers[0].bad_frame>=5) targets[i].trackers[0].is_alive = false;
				cn++;
			}
		}
	}
	std::cout<<cn<<std::endl;
}

void MultiTracker::filter_traj(int len_thre)
{
	std::vector<Target> targets1;
	for(int i = 0; i<targets.size(); i++)
	{
		int max_len = 0;
		int max_ind = -1;
		for(int j = 0; j<targets[i].trackers.size(); j++)
		{
			int len = targets[i].trackers[j].pts.size();
			if(len>max_len)
			{
				max_len = len;
				max_ind = j;
			}
		}
		if(max_ind!=-1&&max_len>len_thre)
		{
			Target target;
			target.is_ambiguous = false;
			target.trackers.push_back(targets[i].trackers[max_ind]);
			//target.trackers[0].is_alive = false;
			if(!target.trackers[0].is_alive)
			{
				std::vector<myPoint>::iterator ite = target.trackers[0].pts.end()-1;
				//int j = target.trackers[0].pts.size()-1;
				while((*ite).score<0.05*particle_num)
				{
					target.trackers[0].pts.erase(ite);
					ite = target.trackers[0].pts.end()-1;
				}
				//for(int j = target.trackers[0].pts.size()-1; ;j--)
				//{
				//	target.trackers[0].pts[j].score
				//}
			}
			target.trackers[0].is_alive = false;
			if(target.trackers[0].pts.size()>15)
				targets1.push_back(target);
		}
	}
	targets = targets1;
	std::cout<<targets.size()<<std::endl;
	//for(int i = 0; i < targets.size(); i++)
	//std::cout<<targets[i].trackers[0].is_alive<<std::endl;
}

void MultiTracker::link_traj()
{
	for(int i = 0; i<targets.size(); i++)
	{
		int it = 0;
		std::vector<myPoint> pts1;
		int s = targets[i].trackers[0].pts.size();
		while(targets[i].trackers[0].pts[it].score<0.05*particle_num&&it<s)
		{
			it++;
		}
		for(int j = it; j < s; j++)
		{
			pts1.push_back(targets[i].trackers[0].pts[j]);
		}
		targets[i].trackers[0].pts = pts1;
	}
	for(int i = 0; i < targets.size(); i++)
	{
		Mat X = Mat::ones(4,targets[i].trackers[0].pts.size(),CV_64F);
		for(int j = 0; j < X.cols; j++)
		{
			X.at<double>(0,j) = targets[i].trackers[0].pts[j].x;
			X.at<double>(1,j) = targets[i].trackers[0].pts[j].y;
			X.at<double>(2,j) = targets[i].trackers[0].pts[j].z;
		}
		for(int j = 0; j<view_num; j++)
		{
			Mat x = pms[j]*X;
			x.row(0) = x.row(0)/x.row(2);
			x.row(1) = x.row(1)/x.row(2);
			targets[i].trackers[0].pts2.push_back(x);
		}
	}
	for(int it = 0; it<1; it++)
	{
		std::vector<int> e;
		std::vector< std::vector<int> > cons(targets.size(),e);
		std::vector<int> is_outlier(targets.size(),0);
		std::vector<Target> targets1;
		for(int i = 0; i < targets.size(); i++)
			for(int j = i+1; j < targets.size(); j++)
			{
				if(is_outlier[i]==0)
				{
					Target t3;
					int a = find_relation(targets[i],targets[j],t3);
					if(a!=NO_OVERLAP)
					{
						cons[i].push_back(j);
						cons[j].push_back(i);
						//con.at<double>(i,j) = 1;
						//con.at<double>(j,i) = 1;
					}
				}
			}
			std::queue<int> q;
			std::vector<int> visit(targets.size(),0);
			for(int i = 0; i < targets.size(); i++)
			{
				if(visit[i]==0)
				{
					std::vector<int> traj_ids;
					traj_ids.push_back(i);
					q.push(i);
					visit[i] = 1;
					while(!q.empty())
					{
						int a = q.front();
						q.pop();
						for(int j = 0; j<cons[a].size(); j++)
						{
							int b = cons[a][j];

							if(visit[b]==0)
							{
								traj_ids.push_back(b);

								q.push(b);
								visit[b]=1;
								//std::cout<<b<<visit[b]<<std::endl;
							}
						}

					}
					std::cout<<traj_ids.size()<<std::endl;
					if(traj_ids.size()>=2)
					{
						std::cout<<"dads"<<std::endl;
						std::vector<Target> ts;
						link_n_traj(traj_ids, ts);
						for(int i1 = 0; i1 < ts.size(); i1++)
						{
							if(ts[i1].trackers[0].pts.size()>20)
								targets1.push_back(ts[i1]);
						}
						for(int i2 = 0; i2 < traj_ids.size(); i2++)
							is_outlier[traj_ids[i2]] = 1;
					}
				}
			}
			for(int i = 0; i < targets.size(); i++)
			{
				if(is_outlier[i]==0) targets1.push_back(targets[i]);
			}
			targets = targets1;
			std::cout<<targets.size()<<std::endl;
	}

	for(int i = 0; i < targets.size(); i++)
	{
		targets[i].trackers[0].is_alive = true;
		targets[i].trackers[0].is_fixed = true;
		if(targets[i].trackers[0].pts.size()==0)
			std::cout<<"dfadfa"<<std::endl;
	}
}

void MultiTracker::show_traj(int t)
{
	getCurImages(t);
	Mat img1,img2;
	cvtColor(cur_imgs[0],img1,CV_GRAY2BGR);
	cvtColor(cur_imgs[1],img2,CV_GRAY2BGR);
	for(int i = 0; i < targets.size(); i++)
	{
		bool find = false;
		int k = 0;
		//std::cout<<i<<' '<<targets[i].trackers[0].pts.size()<<std::endl;
		int j = targets[i].trackers[0].pts[k].t;

		while(j!=t&&(k+1)<targets[i].trackers[0].pts.size())
		{
			k++;
			j = targets[i].trackers[0].pts[k].t;
			if(j==t) find = true;
		}
		if(find)
		{
			Mat X = Mat::ones(4,1,CV_64F);
			X.at<double>(0,0) = targets[i].trackers[0].pts[k].x;
			X.at<double>(1,0) = targets[i].trackers[0].pts[k].y;
			X.at<double>(2,0) = targets[i].trackers[0].pts[k].z;
			Mat x = pms[0]*X;
			x = x/x.at<double>(2,0);
			Point pt1;
			pt1.x = x.at<double>(0,0);
			pt1.y = x.at<double>(1,0);
			int r = 255*(i%10)/10;
			int g = 255*(i%5)/5;
			int b = 255*(i%15)/15;
			if(r<40&&g<40&&b<40) r=255-r;
			circle(img1,pt1,6,Scalar(r,g,b));
			x = pms[1]*X;
			x = x/x.at<double>(2,0);
			Point pt2;
			pt2.x = x.at<double>(0,0);
			pt2.y = x.at<double>(1,0);
			circle(img2,pt2,6,Scalar(r,g,b));
		}
	}
	imshow("img1",img1);
	/*char dst[500];
	for(int i =0; i < 500; i++)
		dst[i] = filedir[i];
	char src[500];
	int a;
	a = sprintf(src,"/result/0/%d.bmp",t);
	strcat(dst,src);
	imwrite(dst,img1);

	for(int i =0; i < 500; i++)
		dst[i] = filedir[i];
	char src1[500];
	a = sprintf(src1,"/result/1/%d.bmp",t);
	strcat(dst,src1);
	imwrite(dst,img1);*/
	waitKey(100);
}

bool MultiTracker::find_overlap(const Target& t1, const Target& t2, int len, int pos1, int pos2)
{
	std::vector<myPoint> pts1 = t1.trackers[0].pts;
	std::vector<myPoint> pts2 = t2.trackers[0].pts;
	int min1 = pts1[0].t;
	int max1 = pts1[pts1.size()-1].t;
	int min2 = pts1[0].t;
	int max2 = pts1[pts1.size()-1].t;
	int begin = min1>min2?min1:min2;
	int end = max1<max2?max1:max2;
	if(min1>max2||min2>max1)
		return false;
	else
	{
		int begin1 = 0;
		while(pts1[begin1].t!=begin&&begin1<pts1.size())
			begin1++;
		int begin2 = 0;
		while(pts2[begin2].t!=begin&&begin2<pts2.size())
			begin2++;
		Mat X1 = Mat::ones(4,1,CV_64F);
		Mat X2 = Mat::ones(4,1,CV_64F);
		Mat x1 = Mat::ones(3,1,CV_64F);
		Mat x2 = Mat::ones(3,1,CV_64F);
		int cn = 0;
		len = 0;
		int b1,b2;
		for(int i = 0; pts1[begin1+i].t<=end&&pts2[begin2+i].t<=end&&begin1+i<pts1.size()&&begin2+i<pts2.size(); i++)
		{
			X1.at<double>(0,0) = pts1[begin1+i].x;
			X1.at<double>(1,0) = pts1[begin1+i].y;
			X1.at<double>(2,0) = pts1[begin1+i].z;
			X2.at<double>(0,0) = pts2[begin2+i].x;
			X2.at<double>(1,0) = pts2[begin2+i].y;
			X2.at<double>(2,0) = pts2[begin2+i].z;
			bool flag = true;
			for(int j = 0; j < view_num; j++)
			{
				x1 = pms[j]*X1;
				x1 = x1/x1.at<double>(2,0);
				x2 = pms[j]*X2;
				x2 = x2/x2.at<double>(2,0);
				Mat tmp = x1-x2;
				double dist = sqrt(tmp.at<double>(0,0)*tmp.at<double>(0,0)+tmp.at<double>(1,0)*tmp.at<double>(1,0));
				if(dist>5)
				{
					flag = false;
					if(cn>len)
					{len = cn; pos1 = b1; pos2 = b2;}
					cn = 0;
				}
				else
				{
					cn ++;
					if(cn==1){b1 = begin1+i; b2 = begin2+i;}
				}
			}
		}
		return true;
	}
}

void MultiTracker::link_two_traj(const Target& t1, const Target& t2, Target& t3)
{
	std::vector<myPoint> pts1 = t1.trackers[0].pts;
	std::vector<myPoint> pts2 = t2.trackers[0].pts;
	int min1 = pts1[0].t;
	int max1 = pts1[pts1.size()-1].t;
	int min2 = pts2[0].t;
	int max2 = pts2[pts2.size()-1].t;
	int begin = min1>min2?min1:min2;
	int end = max1<max2?max1:max2;
	int len = end-begin+1;
	int b1 = begin-min1;
	int b2 = begin-min2;
	int first1 = -1;
	int first2 = -1;
	int last1 = -1;
	int last2 = -1;
	int overlap_len = 0;
	int cn = 0;

	for(int i = 0; i < len; i++)
	{
		bool flag = true;
		for(int j = 0; j < view_num; j++)
		{
			//std::cout<<t1.trackers[0].pts[b1+i].t<<t2.trackers[0].pts[b2+i].t<<std::endl;
			/*if(t1.trackers[0].pts[b1+i].t!=t2.trackers[0].pts[b2+i].t)
			{
			int da = 10;
			std::cout<<"daas";
			}*/
			double x1 = t1.trackers[0].pts2[j].at<double>(0,b1+i);
			double y1 = t1.trackers[0].pts2[j].at<double>(1,b1+i);
			double x2 = t2.trackers[0].pts2[j].at<double>(0,b2+i);
			double y2 = t2.trackers[0].pts2[j].at<double>(1,b2+i);
			double dist = sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
			if(dist>5)
			{
				flag = false;
			}
		}
		if(flag)
		{
			cn ++;
			if(cn==1){first1 = b1+i; first2 = b2+i;}
			last1=b1+i;
			last2=b2+i;
		}
	}
	Tracker tra;
	int mid1 = (first1+last1)/2;
	int mid2 = mid1-first1+first2;
	for(int i = 0; i < mid1; i++)
	{
		myPoint pt;
		pt.x = t1.trackers[0].pts[i].x;
		pt.y = t1.trackers[0].pts[i].y;
		pt.z = t1.trackers[0].pts[i].z;
		pt.t = t1.trackers[0].pts[i].t;
		tra.pts.push_back(pt);
	}
	for(int i = mid2; i < pts2.size(); i++)
	{
		myPoint pt;
		pt.x = t2.trackers[0].pts[i].x;
		pt.y = t2.trackers[0].pts[i].y;
		pt.z = t2.trackers[0].pts[i].z;
		pt.t = t2.trackers[0].pts[i].t;
		if(pt.t-tra.pts[tra.pts.size()-1].t!=1)
			std::cout<<"error"<<std::endl;
		tra.pts.push_back(pt);
	}
	t3.trackers.push_back(tra);
	Mat X = Mat::ones(4,t3.trackers[0].pts.size(),CV_64F);
	for(int j = 0; j < X.cols; j++)
	{
		X.at<double>(0,j) = t3.trackers[0].pts[j].x;
		X.at<double>(1,j) = t3.trackers[0].pts[j].y;
		X.at<double>(2,j) = t3.trackers[0].pts[j].z;
	}
	for(int j = 0; j<view_num; j++)
	{
		Mat x = pms[j]*X;
		x.row(0) = x.row(0)/x.row(2);
		x.row(1) = x.row(1)/x.row(2);
		t3.trackers[0].pts2.push_back(x);
	}

}

int MultiTracker::find_relation(const Target& t1, const Target& t2, Target& t3)
{
	std::vector<myPoint> pts1 = t1.trackers[0].pts;
	std::vector<myPoint> pts2 = t2.trackers[0].pts;
	if(pts1.empty()||pts2.empty()) return NO_OVERLAP;
	int min1 = pts1[0].t;
	int max1 = pts1[pts1.size()-1].t;
	int min2 = pts2[0].t;
	int max2 = pts2[pts2.size()-1].t;
	int begin = min1>min2?min1:min2;
	int end = max1<max2?max1:max2;
	if(begin>end)
	{
		return NO_OVERLAP;
	}
	else
	{
		int len = end-begin+1;
		int b1 = begin-min1;
		int b2 = begin-min2;
		int first1 = -1;
		int first2 = -1;
		int last1 = -1;
		int last2 = -1;
		int overlap_len = 0;
		int cn = 0;
		int cn1 = 0;
		for(int i = 0; i < len; i++)
		{
			bool flag = true;
			int f1=0,f2=0,l1=0,l2=0;
			for(int j = 0; j < view_num; j++)
			{
				//std::cout<<t1.trackers[0].pts[b1+i].t<<t2.trackers[0].pts[b2+i].t<<std::endl;
				/*if(t1.trackers[0].pts[b1+i].t!=t2.trackers[0].pts[b2+i].t)
				{
				int da = 10;
				std::cout<<"daas";
				}*/
				double x1 = t1.trackers[0].pts2[j].at<double>(0,b1+i);
				double y1 = t1.trackers[0].pts2[j].at<double>(1,b1+i);
				double x2 = t2.trackers[0].pts2[j].at<double>(0,b2+i);
				double y2 = t2.trackers[0].pts2[j].at<double>(1,b2+i);
				double dist = sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
				if(dist>5)
				{
					flag = false;
				}
			}
			if(flag)
			{
				cn ++;
				cn1 ++;
				if(cn==1){f1 = b1+i; f2 = b2+i;}
				l1=b1+i;
				l2=b2+i;
				if(cn>overlap_len)
				{
					first1 = f1;
					first2 = f2;
					last1 = l1;
					last2 = l2;
					overlap_len = cn;
				}
			}
			else cn = 0;
		}
		//overlap_len = cn;
		int len1 = pts1.size();
			int len2 = pts2.size();
		/*if(overlap_len>2)
		{
			if(first1>first2&&fabs(last1-len1)<3&&first2<3&&first1>10&&fabs(len2-last2)>10)
			{
				link_two_traj(t1,t2,t3);
				return LINKABLE1;
			}
			else if(first2>first1&&fabs(last2-len2)<3&&first2>10&&first1<3&&fabs(len1-last1)>10)
			{
				link_two_traj(t2,t1,t3);
				return LINKABLE2;
			}
		}*/
		if(overlap_len<12) return NO_OVERLAP;
		else return PROBLEMATIC;
	}
}

void MultiTracker::link_n_traj(std::vector<int> traj_ids, std::vector<Target>& ts)//�ϲ�����
{
	int t_min = 100000;
	int t_max = -1;
	int mi = -1;
	//Mat img = Mat::zeros(1024,1020,CV_8UC3);
				//cvtColor(cur_imgs[k],img,CV_GRAY2BGR);
	std::vector<Scalar> colors;
	colors.push_back(Scalar(0,0,255));
	colors.push_back(Scalar(0,255,0));
	colors.push_back(Scalar(255,0,0));
	for(int i = 0; i < traj_ids.size(); i++)
	{
		int id = traj_ids[i];
		int a = targets[id].trackers[0].pts[0].t;
		int b = targets[id].trackers[0].pts[targets[id].trackers[0].pts.size()-1].t;
		if(a<t_min) {t_min = a; mi = i;}
		if(b>t_max) t_max = b;
		//draw2dtraj(img, targets[traj_ids[i]].trackers[0].pts2[0], colors[i%3]);
	}
	//imshow("img",img);
	//waitKey();
	Mat X1 = Mat::ones(4,1,CV_64F);
	Mat X2 = Mat::ones(4,1,CV_64F);
	Mat x1 = Mat::ones(3,1,CV_64F);
	Mat x2 = Mat::ones(3,1,CV_64F);
	MyGraph graph;
	GraphNode n;
	n.t = t_min;
	n.tids.push_back(mi);
	n.pre = -1;
	n.next = -1;
	graph.add_node(n);
	std::vector<int> cur_nodes;
	std::vector<int> pre_nodes;
	pre_nodes.push_back(0);
	for(int t = t_min+1; t<=t_max; t++)
	{
		//GraphNode n;
		//n.t = t;
		cur_nodes.clear();
		std::vector<int> ind;
		std::vector<int> visit;
		std::vector<myPoint> ptm;
		for(int i = 0; i < traj_ids.size(); i++)
		{
			int id = traj_ids[i];
			int a = targets[id].trackers[0].pts[0].t;
			int b = targets[id].trackers[0].pts[targets[id].trackers[0].pts.size()-1].t;
			if(t>=a&&t<=b)
			{
				ind.push_back(i);
				ptm.push_back(targets[id].trackers[0].pts[t-a]);
				visit.push_back(0);
			}
		}
		std::queue<int> nodequeue;

		for(int i = 0; i < ind.size(); i++)
		{
			if(visit[i]==0)
			{
				visit[i] = 1;
				GraphNode n;
				n.t = t;
				n.tids.push_back(ind[i]);
				nodequeue.push(i);
				int a1 = targets[traj_ids[ind[i]]].trackers[0].pts[0].t;
				myPoint pt = targets[traj_ids[ind[i]]].trackers[0].pts[t-a1];
				X1.at<double>(0,0) = pt.x;
				X1.at<double>(1,0) = pt.y;
				X1.at<double>(2,0) = pt.z;
				while(!nodequeue.empty())
				{
					int cur = nodequeue.front();
					for(int j = 0; j < ind.size(); j++)
					{
						if(j!=cur&&visit[j]==0)
						{
							int a2 = targets[traj_ids[ind[j]]].trackers[0].pts[0].t;
							myPoint pt1 = targets[traj_ids[ind[j]]].trackers[0].pts[t-a2];
							X2.at<double>(0,0) = pt1.x;
							X2.at<double>(1,0) = pt1.y;
							X2.at<double>(2,0) = pt1.z;
							bool flag = true;
							for(int v = 0; v < view_num; v++)
							{
								x1 = pms[v]*X1;
								x1 = x1/x1.at<double>(2,0);
								x2 = pms[v]*X2;
								x2 = x2/x2.at<double>(2,0);
								double dx = x1.at<double>(0,0)-x2.at<double>(0,0);
								double dy = x1.at<double>(1,0)-x2.at<double>(1,0);
								double dist = sqrt(dx*dx+dy*dy);
								if(dist>5) flag = false;
							}
							if(flag)
							{
								visit[j] = 1;
								nodequeue.push(j);
								n.tids.push_back(ind[j]);
							}
						}
					}
					nodequeue.pop();
				}
				bool flag = true;
				int id = -1;
				for(int it = 0; it<pre_nodes.size()&&flag; it++)
				{
					for(int i1 = 0; i1<n.tids.size()&&flag; i1++)
						for(int i2 = 0; i2<graph.nodes[pre_nodes[it]].tids.size()&&flag; i2++)
						{
							if(n.tids[i1]==graph.nodes[pre_nodes[it]].tids[i2]) {flag = false; id = pre_nodes[it];}
						}

				}
				n.pre = id;
				n.next = -1;
				if(id!=-1)
				{
					graph.add_edge(id,graph.nodes.size());
					graph.nodes[id].next = graph.nodes.size();
				}// graph.add_edge(cur_gnode,graph.nodes.size()-1);
				cur_nodes.push_back(graph.nodes.size());
				graph.add_node(n);
			}
		}
		pre_nodes = cur_nodes;
	}
	std::vector< std::vector<int> > paths;
	graph.split(paths);
	for(int i = 0; i<paths.size(); i++)
	{
		Tracker tra;
		for(int j = 0; j<paths[i].size(); j++)
		{
			myPoint pt;
			pt.x = 0;
			pt.y = 0;
			pt.z = 0;
			int cn = 0;
			int t = graph.nodes[paths[i][j]].t;
			//std::cout<<paths[i][j]<<' '<<graph.nodes.size()<<std::endl;
			for(int j1 = 0; j1 < graph.nodes[paths[i][j]].tids.size(); j1++)
			{

				int ti = graph.nodes[paths[i][j]].tids[j1];
				int a2 = targets[traj_ids[ti]].trackers[0].pts[0].t;
				//std::cout<<traj_ids[ti]<<' '<<ti<<' '<<a2<<' '<<t<<' '<<targets[traj_ids[ti]].trackers[0].pts.size()<<std::endl;
				myPoint pt1 = targets[traj_ids[ti]].trackers[0].pts[t-a2];
				pt.x += pt1.x;
				pt.y += pt1.y;
				pt.z += pt1.z;
				cn ++;
			}
			pt.x = pt.x/cn;
			pt.y = pt.y/cn;
			pt.z = pt.z/cn;
			pt.t = t;
			tra.pts.push_back(pt);
		}
		if(tra.pts.size()>20);
		{
			Target t;
			t.trackers.push_back(tra);
			ts.push_back(t);
		}
	}
	//Mat img2 = Mat::zeros(1024,1020,CV_8UC3);
	//for(int it = 0; it<ts.size(); it++)
	//{
	//	Mat X = Mat::ones(4,ts[it].trackers[0].pts.size(),CV_64F);
	//	for(int j = 0; j < X.cols; j++)
	//	{
	//		X.at<double>(0,j) = ts[it].trackers[0].pts[j].x;
	//		X.at<double>(1,j) = ts[it].trackers[0].pts[j].y;
	//		X.at<double>(2,j) = ts[it].trackers[0].pts[j].z;
	//	}

	//	Mat x = pms[0]*X;
	//	x.row(0) = x.row(0)/x.row(2);
	//	x.row(1) = x.row(1)/x.row(2);
	//	//std::cout<<tra.pts.size()<<std::endl;
	//	//draw2dtraj(img2, x, colors[it%3]);
	//}
	//imshow("img1",img2);
	//waitKey();
}

void MultiTracker::templateMatch(const Mat& img1, const Mat& img2, const std::vector<Point2f>& pts1, std::vector<Point2f>& pts2)
{
	int win = 30;
	for(int i = 0; i<pts1.size(); i++)
	{
		int r = int(pts1[i].y);
		int c = int(pts1[i].x);
		Mat tmp = img1(Range(r-5,r+5),Range(c-5,c+5));
		Mat src = img2(Range(r-win/2,r+win/2+1),Range(c-win/2,c+win/2+1));
		int H = src.rows;
		int W = src.cols;
		int h = tmp.rows;
		int w = tmp.cols;
		Mat result(H-h+1,W-w+1,CV_32F);
		matchTemplate(src,tmp,result,CV_TM_CCORR_NORMED);
		Point minloc;
		Point maxloc;
		double minval;
		double maxval;
		minMaxLoc(result, &minval, &maxval, &minloc, &maxloc);
		Point2f ptm;
		ptm.x = maxloc.x+w/2+c-win/2+1;
		ptm.y = maxloc.y+h/2+r-win/2+1;
		pts2.push_back(ptm);
	}
}

void MultiTracker::searchMatch(const Mat& img1, const Mat& img2, const std::vector<Point2f>& pts1, const std::vector<Point2f>& pts2, std::vector<int>& match)
{
	int cn = 0;
	for(double i = -patch_radius; i<=patch_radius; i++)
		for(double j = -patch_radius; j<=patch_radius; j++)
		{
			if(sqrt(i*i+j*j)<=patch_radius) cn++;
		}
		Mat ptm(2,cn,CV_64F);
		Mat ptm1(2,cn,CV_64F);
		int cn1 = 0;
		for(double i = -patch_radius; i<=patch_radius; i++)
			for(double j = -patch_radius; j<=patch_radius; j++)
			{
				if(sqrt(i*i+j*j)<=patch_radius)
				{
					ptm.at<double>(0,cn1) = i;
					ptm.at<double>(1,cn1) = j;
					cn1++;
				}
			}
	/*std::vector<Point2f> tmp;
	for(double i = -patch_radius; i<=patch_radius; i++)
		for(double j = -patch_radius; j<=patch_radius; j++)
		{
			if(sqrt(i*i+j*j)<patch_radius) tmp.push_back(Point2f(i,j));
		}
		Mat ptm(2,tmp.size(),CV_64F);
		for(int i = 0; i < tmp.size(); i++)
		{
			ptm.at<double>(0,i) = tmp[i].x;
			ptm.at<double>(1,i) = tmp[i].y;
		}*/
		Mat pt1(2,cn1,CV_64F);
		Mat pt2(2,cn1,CV_64F);
		Mat ref(1,cn1,CV_64F);
		Mat value(1,cn1,CV_64F);
		for(int i = 0; i < pts1.size(); i++)
		{
			pt1.row(0) = ptm.row(0)+pts1[i].x;
			pt1.row(1) = ptm.row(1)+pts1[i].y;
			get_subpixel(pt1,img1,ref);
			double max_ncc = -1;
			int max_ind = -1;
			for(int j = 0; j<pts2.size(); j++)
			{
				double dist = (pts2[j].x-pts1[i].x)*(pts2[j].x-pts1[i].x)+(pts2[j].y-pts1[i].y)*(pts2[j].y-pts1[i].y);
				dist = sqrt(dist);
				if(dist<20)
				{
					pt2.row(0) = ptm.row(0)+pts2[j].x;
					pt2.row(1) = ptm.row(1)+pts2[j].y;
					get_subpixel(pt2,img2,value);
					double ncc = calc_ncc(ref,value);
					if(ncc>max_ncc)
					{
						max_ncc = ncc;
						max_ind = j;
					}
				}
			}
			match.push_back(max_ind);
		}
}

int MultiTracker::get_subpixel( const Mat& pts, const Mat& img ,Mat &value )
{
	int return_value = 1;
	int count=0;
	for(size_t i = 0; i < pts.cols; i++)
	{
		return_value = 1;
		double x = pts.at<double>(0,i);
		double y = pts.at<double>(1,i);
		double v1,v2,v3,v4;
		int x1 = int(x);
		int y1 = int(y);
		int x2 = int(x)+1;
		int y2 = int(y);
		int x3 = int(x);
		int y3 = int(y)+1;
		int x4 = int(x)+1;
		int y4 = int(y)+1;
		//imshow("1",img);
		//waitKey();
		if(x1-1>=0&&x1-1<img.cols&&y1-1>=0&&y1-1<img.rows)
		{
			v1 = img.at<uchar>(y1-1,x1-1);
		}
		else
		{
			v1 = 0;
			return_value = 0;
		}
		if(x2-1>=0&&x2-1<img.cols&&y2-1>=0&&y2-1<img.rows)
		{
			v2 = img.at<uchar>(y2-1,x2-1);
		}
		else
		{
			v2 = 0;
			return_value = 0;
		}
		if(x3-1>=0&&x3-1<img.cols&&y3-1>=0&&y3-1<img.rows)
		{
			v3 = img.at<uchar>(y3-1,x3-1);
		}
		else
		{
			v3 = 0;
			return_value = 0;
		}
		if(x4-1>=0&&x4-1<img.cols&&y4-1>=0&&y4-1<img.rows)
		{
			v4 = img.at<uchar>(y4-1,x4-1);
		}
		else
		{
			v4 = 0;
			return_value = 0;
		}
		double v5 = v1+(v2-v1)*(x-x1);
		double v6 = v3+(v4-v3)*(x-x1);
		double v = v5 + (v6-v5)*(y-y1);
		value.at<double>(0,i) = v;
		if(return_value==0)count++;
	}
	if(count>0.99*pts.cols)return 0;
	else return 1;
}

double MultiTracker::calc_ncc( const Mat& v1, const Mat& v2 )
{
	Scalar mean1;
	Scalar std1;
	Scalar mean2;
	Scalar std2;
	meanStdDev(v1,mean1,std1);
	meanStdDev(v2,mean2,std2);
	double sum = 0;
	if(mean1.val[0]>0&&mean2.val[0]>0&&mean1.val[0]<255&&mean1.val[0]<255)
	{
		if(std1.val[0]!=0&&std2.val[0]!=0)
		{
			for(size_t i = 0; i < v1.cols; i++)
			{
				sum += (v1.at<double>(0,i)-mean1.val[0])*(v2.at<double>(0,i)-mean2.val[0])/(std1.val[0]*std2.val[0]);
			}
			return sum/(v1.cols-1);
		}
		else if(std1.val[0]==0&&std2.val[0]!=0)
		{
			for(size_t i = 0; i < v1.cols; i++)
			{
				sum += (v2.at<double>(0,i)-mean2.val[0])/(std2.val[0]);
			}
			return 0;
		}
		else if(std1.val[0]!=0&&std2.val[0]==0)
		{
			for(size_t i = 0; i < v1.cols; i++)
			{
				sum += (v1.at<double>(0,i)-mean1.val[0])/(std1.val[0]);
			}
			return 0;
		}
		else return 0;
	}
	else return 0;
}

double MultiTracker::calc_ncc( const Mat& v1, const Mat& v2 ,const Scalar& mean1,const Scalar& std1)
{
	//Scalar mean1;
	//Scalar std1;
	Scalar mean2;
	Scalar std2;
	//meanStdDev(v1,mean1,std1);
	meanStdDev(v2,mean2,std2);
	double sum = 0;
	if(mean1.val[0]>0&&mean2.val[0]>0&&mean1.val[0]<255&&mean1.val[0]<255)
	{
		if(std1.val[0]!=0&&std2.val[0]!=0)
		{
			for(size_t i = 0; i < v1.cols; i++)
			{
				sum += (v1.at<double>(0,i)-mean1.val[0])*(v2.at<double>(0,i)-mean2.val[0])/(std1.val[0]*std2.val[0]);
			}
			return sum/(v1.cols-1);
		}
		else if(std1.val[0]==0&&std2.val[0]!=0)
		{
			for(size_t i = 0; i < v1.cols; i++)
			{
				sum += (v2.at<double>(0,i)-mean2.val[0])/(std2.val[0]);
			}
			return 0;
		}
		else if(std1.val[0]!=0&&std2.val[0]==0)
		{
			for(size_t i = 0; i < v1.cols; i++)
			{
				sum += (v1.at<double>(0,i)-mean1.val[0])/(std1.val[0]);
			}
			return 0;
		}
		else return 0;
	}
	else return 0;
}

Mat MultiTracker::calc_fund_matrix(const Mat& p1, const Mat& p2)
{
	Mat M = p1(Range::all(),Range(0,3));
	Mat p4 = p1.col(3);
	Mat C = Mat::ones(4,1,CV_64F);
	C(Range(0,3),Range::all()) = -M.inv()*p4;
	Mat e2 = p2*C;
	//printMatrix(e2);
	if(e2.at<double>(2,0)!=0)
	{
		e2.row(0) = e2.row(0)/e2.row(2);
		e2.row(1) = e2.row(1)/e2.row(2);
		e2.row(2) = e2.row(2)/e2.row(2);
	}
	Mat ecross = cal_skew_matrix(e2);
	//printMatrix(e2);

	Mat F = ecross*p2*(p1.inv(DECOMP_SVD));
	return F;
}
Mat MultiTracker::cal_skew_matrix(Mat e)
{
	Mat tmp(3,3,CV_64F);
	if(e.cols==1)
	{
		tmp.at<double>(0,0) = 0;
		tmp.at<double>(0,1) = -e.at<double>(2,0);
		tmp.at<double>(0,2) = e.at<double>(1,0);
		tmp.at<double>(1,0) = e.at<double>(2,0);
		tmp.at<double>(1,1) = 0;
		tmp.at<double>(1,2) = -e.at<double>(0,0);
		tmp.at<double>(2,0) = -e.at<double>(1,0);
		tmp.at<double>(2,1) = e.at<double>(0,0);
		tmp.at<double>(2,2) = 0;
	}
	else
	{
		tmp.at<double>(0,0) = 0;
		tmp.at<double>(0,1) = -e.at<double>(0,2);
		tmp.at<double>(0,2) = e.at<double>(0,1);
		tmp.at<double>(1,0) = e.at<double>(0,2);
		tmp.at<double>(1,1) = 0;
		tmp.at<double>(1,2) = -e.at<double>(0,0);
		tmp.at<double>(2,0) = -e.at<double>(0,1);
		tmp.at<double>(2,1) = e.at<double>(0,0);
		tmp.at<double>(2,2) = 0;
	}
	return tmp;
}

void MultiTracker::triangulatePoints( const Mat& pt_left, const Mat& pt_right, const Mat& p1, const Mat& p2, Mat &pt3)
{
	int n = pt_left.cols;
	Mat A(6,3,CV_64F);
	Mat tmp(6,4,CV_64F);
	Mat b(6,1,CV_64F);
	Mat result(3,1,CV_64F);
	pt3 = 1;
	for (int i = 0; i<n; i++)
	{
		double a1[3][3] = {{0,-1,pt_left.at<double>(1,i)},{1,0,-pt_left.at<double>(0,i)},{-pt_left.at<double>(1,i),pt_left.at<double>(0,i),0}};
		double a2[3][3] = {{0,-1,pt_right.at<double>(1,i)},{1,0,-pt_right.at<double>(0,i)},{-pt_right.at<double>(1,i),pt_right.at<double>(0,i),0}};
		Mat A1(3,3,CV_64F,a1);
		Mat A2(3,3,CV_64F,a2);
		A1 = A1*p1;
		A2 = A2*p2;
		Mat tp1 = tmp(Range(0,3),Range::all());
		A1.copyTo(tp1);
		Mat tp2 = tmp(Range(3,6),Range::all());
		A2.copyTo(tp2);
		A = tmp(Range::all(),Range(0,3));
		b = -tmp.col(3);
		solve(A,b,result,DECOMP_SVD);
		for(int j = 0; j <3; j++)pt3.at<double>(j,i) = result.at<double>(j,0);
	}
	//Mat img2;
	//cvtColor(cur_imgs[1],img2,CV_GRAY2BGR);
}

void MultiTracker::triangulatePoints( const Mat& pt_left, const Mat& pt_right, const Mat& p1, const Mat& p2, Mat &pt3, double a, double b, double c)
{
	Mat A1(2,2,CV_64F);
	Mat b1(2,1,CV_64F);
	A1.at<double>(0,0) = a;
	A1.at<double>(1,0) = -b;
	A1.at<double>(0,1) = b;
	A1.at<double>(1,1) = a;
	b1.at<double>(0,0) = -c;
	b1.at<double>(1,0) = a*pt_right.at<double>(1,0)-b*pt_right.at<double>(0,0);
	Mat pt_right1(2,1,CV_64F);
	solve(A1,b1,pt_right1,DECOMP_SVD);
	printMatrix(pt_right1);
	printMatrix(pt_right);
	//Mat pt_right1(3,1,)
	int n = pt_left.cols;
	Mat A(6,3,CV_64F);
	Mat tmp(6,4,CV_64F);
	Mat b2(6,1,CV_64F);
	Mat result(3,1,CV_64F);
	pt3 = 1;
	for (int i = 0; i<n; i++)
	{
		double a1[3][3] = {{0,-1,pt_left.at<double>(1,i)},{1,0,-pt_left.at<double>(0,i)},{-pt_left.at<double>(1,i),pt_left.at<double>(0,i),0}};
		double a2[3][3] = {{0,-1,pt_right1.at<double>(1,i)},{1,0,-pt_right1.at<double>(0,i)},{-pt_right1.at<double>(1,i),pt_right1.at<double>(0,i),0}};
		Mat A1(3,3,CV_64F,a1);
		Mat A2(3,3,CV_64F,a2);
		A1 = A1*p1;
		A2 = A2*p2;
		Mat tp1 = tmp(Range(0,3),Range::all());
		A1.copyTo(tp1);
		Mat tp2 = tmp(Range(3,6),Range::all());
		A2.copyTo(tp2);
		A = tmp(Range::all(),Range(0,3));
		b2 = -tmp.col(3);
		solve(A,b2,result,DECOMP_SVD);
		for(int j = 0; j <3; j++)pt3.at<double>(j,i) = result.at<double>(j,0);
	}
}

void MultiTracker::getData(char* filename, cv::Mat& m)
{
	std::ifstream infile;
	infile.open(filename);
	if(!infile)
	{
		std::cerr << "error: unable to open input file: "<<filename<<std::endl;
	}
	else
	{
		for(int i = 0; i < m.rows; i++)
			for(int j = 0; j < m.cols; j++)
			{
				double a;
				infile>>a;
				m.at<double>(i,j) = a;
			}
	}
	infile.close();
}

void MultiTracker::printMatrix(const Mat& m)
{
	for(int i = 0; i < m.rows; i++)
	{
		for(int j = 0; j < m.cols; j++)
		{
			std::cout<<m.at<double>(i,j)<<' ';
		}
		std::cout<<std::endl;
	}
}

void MultiTracker::drawEpipolar(Mat& img, double a, double b, double c)
{
	Point pt1;
	Point pt2;
	double x1 = 1;
	double y1 = (-a*x1-c)/b;
	double x2 = img.cols-2;
	double y2 = (-a*x2-c)/b;
	pt1.x = x1;
	pt1.y = y1;
	pt2.x = x2;
	pt2.y = y2;
	line(img,pt1,pt2,Scalar(0,0,255));
}

void MultiTracker::drawParticles(Mat& img, Tracker tracker, int view)
{
	Mat X(4,1,CV_64F);
	X.at<double>(3,0) = 1;
	Mat x(3,1,CV_64F);
	Mat X1 = Mat::zeros(4,1,CV_64F);
	for(int i = 0; i < tracker.S.cols; i++)
	{
		X.at<double>(0,0) = tracker.S.at<double>(0,i);
		X.at<double>(1,0) = tracker.S.at<double>(1,i);
		X.at<double>(2,0) = tracker.S.at<double>(2,i);
		X1.at<double>(0,0) += tracker.S.at<double>(0,i)*tracker.W.at<double>(0,i);
		X1.at<double>(1,0) += tracker.S.at<double>(1,i)*tracker.W.at<double>(0,i);
		X1.at<double>(2,0) += tracker.S.at<double>(2,i)*tracker.W.at<double>(0,i);
		x = pms[view]*X;
		x = x/x.at<double>(2,0);
		int c = x.at<double>(0,0)-1;
		int r = x.at<double>(1,0)-1;
		Vec<uchar,3> v;
		v = img.at< Vec<uchar,3> >(r,c);
		int a = int(v[0]);
		a = (a+255)/2;
		v[0]  = a;
		img.at< Vec<uchar,3> >(r,c) = a;
	}
	myPoint pt1 = tracker.pts[tracker.pts.size()-1];

	X.at<double>(0,0) = pt1.x;
	X.at<double>(1,0) = pt1.y;
	X.at<double>(2,0) = pt1.z;
	//printMatrix(X1);
	//printMatrix(X);
	X1.at<double>(3,0) = 1;
	x = pms[view]*X1;
	x = x/x.at<double>(2,0);
	Point pt;
	pt.x = x.at<double>(0,0);
	pt.y = x.at<double>(1,0);
	circle(img,pt,7,Scalar(255,0,0));
}

void MultiTracker::saveTrajs(char* filename)
{
	std::ofstream outfile;
	outfile.open(filename);
	outfile<<targets.size()<<std::endl;
	for(int i = 0; i < targets.size(); i++)
	{
		int s = targets[i].trackers[0].pts.size();
		outfile<<s<<std::endl;
		for(int j = 0; j < s; j++)
		{
			outfile<<targets[i].trackers[0].pts[j].t<<' '<<targets[i].trackers[0].pts[j].x<<' '<<targets[i].trackers[0].pts[j].y<<' '<<targets[i].trackers[0].pts[j].z<<std::endl;
		}
	}
	outfile.close();
}

void MultiTracker::readTrajs(char* filename)
{
	std::ifstream infile;
	infile.open(filename);
	if(!infile)
	{
		std::cerr << "error: unable to open input file: "<<filename<<std::endl;
	}
	else
	{
		targets.clear();
		int tarnum;
		infile>>tarnum;
		for(int i = 0; i < tarnum; i++)
		{
			Target t;
			int len;
			infile>>len;
			Tracker tra;
			for(int j = 0; j < len; j++)
			{
				myPoint pt;
				infile>>pt.t;
				infile>>pt.x;
				infile>>pt.y;
				infile>>pt.z;
				pt.score = particle_num;
				tra.pts.push_back(pt);
			}
			t.trackers.push_back(tra);
			targets.push_back(t);
		}
	}
	infile.close();
}

void MultiTracker::draw2dtraj(Mat& img, const Mat& pts, Scalar color)
{
	for(int i = 0; i < pts.cols-1; i++)
	{
		Point pt1,pt2;
		pt1.x = pts.at<double>(0,i);
		pt1.y = pts.at<double>(1,i);
		pt2.x = pts.at<double>(0,i+1);
		pt2.y = pts.at<double>(1,i+1);
		line(img,pt1,pt2,color);
	}
}

void MultiTracker::showGraph(Mat& img, const MyGraph graph, Scalar color)
{
	Tracker tra;
}

void MultiTracker::eventRetrieve(int event_type)
{
	for(int i = 0; i < targets.size(); i++)
	{
		Mat X = Mat::ones(4,targets[i].trackers[0].pts.size(),CV_64F);
		for(int j = 0; j < X.cols; j++)
		{
			X.at<double>(0,j) = targets[i].trackers[0].pts[j].x;
			X.at<double>(1,j) = targets[i].trackers[0].pts[j].y;
			X.at<double>(2,j) = targets[i].trackers[0].pts[j].z;
		}
		for(int j = 0; j<view_num; j++)
		{
			Mat x = pms[j]*X;
			x.row(0) = x.row(0)/x.row(2);
			x.row(1) = x.row(1)/x.row(2);
			targets[i].trackers[0].pts2.push_back(x);
		}
	}
	std::vector<Event> events;
	for(int i = 0; i < targets.size(); i++)
	{
		std::cout<<i<<std::endl;
		int begin = targets[i].trackers[0].pts[0].t;
		int end = targets[i].trackers[0].pts[targets[i].trackers[0].pts.size()-1].t;
		for(int t = begin; t<end; t++)
		{
			int cn = 0;
			for(int v = 0; v<view_num; v++)
			{
				bool flag = false;
				for(int j = 0; j < targets.size(); j++)
				{
					if(i!=j)
					{
						double dist1 = calcDistance(targets[i],targets[j],t,v);
						if(dist1>=3&&dist1<5) {flag = true; break;}
					}
				}
				if(flag) cn++;
			}
			if(cn>1)
			{
				std::cout<<i<<' '<<t<<std::endl;
				Event e;
				e.event_type = OCC_BOTH;
				e.t = t;
				e.target_id = i;
				events.push_back(e);
			}
		}
	}
	std::cout<<events.size()<<std::endl;
}

void MultiTracker::showParticles(const Mat& S, const Mat& W, int tim, int view, int scale, char* win_name, const Mat& X1)
{
	Mat X(4,1,CV_64F);
	X.at<double>(3,0) = 1;
	Mat x(3,1,CV_64F);
	Mat x1(3,1,CV_64F);
	//Mat X1 = Mat::zeros(4,1,CV_64F);
	/*for(int i = 0; i < S.cols; i++)
	{
		X1.at<double>(0,0) += S.at<double>(0,i)*W.at<double>(0,i);
		X1.at<double>(1,0) += S.at<double>(1,i)*W.at<double>(0,i);
		X1.at<double>(2,0) += S.at<double>(2,i)*W.at<double>(0,i);
	}
	X1.at<double>(3,0) = 1;*/
	x = pms[view]*X1;
	x = x/x.at<double>(2,0);
	Mat img(40*scale,40*scale,CV_8UC3);
	Mat img1(40*scale,40*scale,CV_8UC1);
	Mat v(1,1,CV_64F);
	for(int i = -20; i<20; i++)
		for(int j = -20; j<20; j++)
		{
			x1.at<double>(0,0) = x.at<double>(0,0)+i;
			x1.at<double>(1,0) = x.at<double>(1,0)+j;

			get_subpixel(x1,cur_imgs[view],v);
			//std::cout<<view<<std::endl;
			//printMatrix(X1);
			Vec<uchar,3> tmp;
			tmp[0] = int(v.at<double>(0,0));
			tmp[1] = int(v.at<double>(0,0));
			tmp[2] = int(v.at<double>(0,0));
			int r1 = scale*(j+20);
			int r2 = scale*(j+21);
			int c1 = scale*(i+20);
			int c2 = scale*(i+21);
			img(Range(r1,r2), Range(c1,c2)).setTo(tmp);
			//img1(Range(r1,r2), Range(c1,c2)).setTo(tmp[0]);
		}
		//equalizeHist(img1,img1);
		//cvtColor(img1,img,CV_GRAY2BGR);
	int radius = 2;
	std::vector<int> ind1;
	std::vector<int> ind2;
	for(int i = -radius; i<radius; i++)
		for(int j = -radius; j<radius; j++)
		{
			int d = i*i+j*j;
			if(d<=radius*radius)
			{
				ind1.push_back(i);
				ind2.push_back(j);
			}
		}
	//int ind1[5] = {-1,0,0,0,1};
	//int ind2[5] = {0,-1,0,1,0};
	double wmin;
	double wmax;
	minMaxLoc(W,&wmin,&wmax);
	//std::cout<<wmin<<' '<<wmax<<std::endl;
	for(int i = 0; i < S.cols; i++)
	{
		X.at<double>(0,0) = S.at<double>(0,i);
		X.at<double>(1,0) = S.at<double>(1,i);
		X.at<double>(2,0) = S.at<double>(2,i);
		x1 = pms[view]*X;
		x1 = x1/x1.at<double>(2,0);
		double r1 = x1.at<double>(1,0)-x.at<double>(1,0);
		double c1 = x1.at<double>(0,0)-x.at<double>(0,0);
		int r2 = scale*r1+20*scale+scale/2;
		int c2 = scale*c1+20*scale+scale/2;
		Vec<uchar,3> color;
		calcColor(W.at<double>(0,i),wmin,wmax,color);
		//color[0] = 0;
		//color[1] = 0;
		//color[2] = 255;
		for(int j = 0; j<ind1.size(); j++)
		{
			int r3 = r2 + ind1[j];
			int c3 = c2 + ind2[j];
			if(r3>=0&&r3<img.rows&&c3>=0&&c3<img.cols)
			{
				Vec<uchar,3> a = img.at< Vec<uchar,3> >(r3,c3);
				int v1 = (int(a[0])+color[0])/2;
				int v2 = (int(a[1])+color[1])/2;
				int v3 = (int(a[2])+color[2])/2;
				a[0] = v1;
				a[1] = v2;
				a[2] = v3;
				img.at< Vec<uchar,3> >(r3,c3) = a;
			}
		}
	}
	imshow(win_name,img);
	//waitKey();
}

void MultiTracker::reTrack(int i)
{
	Target t;
	Tracker tra;
	Mat pt1 = Mat::ones(4,1,CV_64F);
	Mat pt2 = Mat::ones(4,1,CV_64F);
	pt1.at<double>(0,0) = targets[i].trackers[0].pts[1].x;
	pt1.at<double>(1,0) = targets[i].trackers[0].pts[1].y;
	pt1.at<double>(2,0) = targets[i].trackers[0].pts[1].z;
	pt2.at<double>(0,0) = targets[i].trackers[0].pts[0].x;
	pt2.at<double>(1,0) = targets[i].trackers[0].pts[0].y;
	pt2.at<double>(2,0) = targets[i].trackers[0].pts[0].z;
	int tim = targets[i].trackers[0].pts[1].t;
	int tim1 = targets[i].trackers[0].pts[targets[i].trackers[0].pts.size()-1].t;
	pre_imgs.clear();
	pre_bgs.clear();
	cur_imgs.clear();
	cur_bgs.clear();
	getCurImages(tim);
	init_tracker(pt1, pt2, tra, tim);
	Mat X = Mat::ones(4,1,CV_64F);
	Mat x = Mat::ones(3,1,CV_64F);
	for(int i = tim+1; i <= tim1; i++)
	{

		/*X.at<double>(0,0) = tra.pts[tra.pts.size()-1].x;
		X.at<double>(1,0) = tra.pts[tra.pts.size()-1].y;
		X.at<double>(2,0) = tra.pts[tra.pts.size()-1].z;
		x = pms[0]*X;
		x = x/x.at<double>(2,0);
		Point pt;
		pt.x = x.at<double>(0,0);
		pt.y = x.at<double>(1,0);
		Mat img;
		cvtColor(cur_imgs[0],img,CV_GRAY2BGR);
		circle(img,pt,8,Scalar(0,0,255));
		imshow("img",img);
		waitKey();*/
		getCurImages(i);
		tracker_update(tra,i);
		//showParticles(tra.S,tra.W,tim,0,8);
	}
}

void MultiTracker::calcColor(double w, double wmin, double wmax, Vec<uchar,3>& color)
{

	/*double r1 = 0;
	double g1 = 0;
	double b1 = 255;
	double r2 = 255;
	double g2 = 0;
	double b2 = 0;*/
	double w1 = (w-wmin)/(wmax-wmin);
	int r = color_ref.rows/2;
	int c = color_ref.cols*w1;
	color = color_ref.at< Vec<uchar,3> >(r,c);
	//Mat img(100, 300, CV_8UC3);
	/*for(int i = 0; i < 100; i++)
	{
		double w1 = i/double(100);
		int r = color_ref.rows/2;
		int c = color_ref.cols*w1;
		Vec<uchar,3> a = color_ref.at<Vec<uchar,3>>(r,c);
		img(Range::all(),Range(3*i,3*i+3)).setTo(a);
	}
	imshow("img",img);
	waitKey();*/
}

double MultiTracker::calcDistance(const Target& t1,const Target& t2, int tim, int view)
{
	std::vector<myPoint> pts1 = t1.trackers[0].pts;
	std::vector<myPoint> pts2 = t2.trackers[0].pts;
	if(pts1.empty()||pts2.empty()) return NO_OVERLAP;
	int min1 = pts1[0].t;
	int max1 = pts1[pts1.size()-1].t;
	int min2 = pts2[0].t;
	int max2 = pts2[pts2.size()-1].t;
	if(tim<min1||tim>max1||tim<min2||tim>max2)
	{
		return -1;
	}
	else
	{
		int ind1 = tim-min1;
		int ind2 = tim-min2;
		double x1 = t1.trackers[0].pts2[view].at<double>(0,ind1);
		double y1 = t1.trackers[0].pts2[view].at<double>(1,ind1);
		double x2 = t2.trackers[0].pts2[view].at<double>(0,ind2);
		double y2 = t2.trackers[0].pts2[view].at<double>(1,ind2);
		double dist = sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
		return dist;
	}
}

MultiTracker::~MultiTracker(void)
{
	//delete []filedir;
}
