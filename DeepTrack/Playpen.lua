require('torch')
cmd = torch.CmdLine()

require('lfs')
require('nngraph')
require('image')
require('Recurrent')
local matio = require 'matio'
require('evaluationUtils')

cmd:option('-gpu', 1, 'use GPU')
cmd:option('-N', 100, 'training sequence length')
cmd:option('-model', './model.lua', 'neural network model')
cmd:option('-weights', './weights_model/7000.dat', 'Weights to load')
cmd:option('-X', 31, 'X dimension size')
cmd:option('-Y', 31, 'Y dimension size')
cmd:option('-Z', 31, 'Z dimension size')


params = cmd:parse(arg)
print(params)

if params.gpu > 0 then
  print('Using GPU ' .. params.gpu)
  require('cunn')
  require('cutorch')
  require('cudnn')
  cutorch.setDevice(params.gpu)
  --DEFAULT_TENSOR_TYPE = 'torch.CudaTensor'
  DEFAULT_TENSOR_TYPE = 'torch.FloatTensor'
else
  print('Using CPU')
  DEFAULT_TENSOR_TYPE = 'torch.FloatTensor'
end

torch.setdefaulttensortype(DEFAULT_TENSOR_TYPE)


-- load neural network model

--package.path = package.path .. ";../Recurrent.lua"

package.path = package.path .. ";"..params.model
if not require(paths.basename(params.model,'.lua')) then
  print("Error couldn't load model.lua from: "..params.model)
  os.exit()
end
print(package.path)

h0 = getInitialState(params.X, params.Y,params.Z)
-- one step of RNN
step = getStepModule(params.X, params.Y,params.Z)

w, _ = step:getParameters()
print("Model has ".. (#w)[1] .. ' parameters')
if #params.weights > 0 then
  print('Loading weights ' .. params.weights)
  local loadedW = torch.load(params.weights)
  print('Loading '.. (#loadedW)[1] .. ' parameters')
  w:copy(loadedW)
end

-- chain N steps into a recurrent neural network
-- N = sequence length; so 1 step per time step? Is this the memory capacity?
model = Recurrent(step, params.N,torch.LongStorage({params.X, params.Y,params.Z}),h0:size(),torch.LongStorage({params.X, params.Y,params.Z,2}))


--------------
-- FUNCTIONS
--------------

function alphanumsort(o)
  local function padnum(d) local dec, n = string.match(d, "(%.?)0*(.+)")
    return #dec > 0 and ("%.12f"):format(d) or ("%s%03d%s"):format(dec, #n, n) end
  table.sort(o, function(a,b)
    return tostring(a):gsub("%.?%d+",padnum)..("%3d"):format(#b)
         < tostring(b):gsub("%.?%d+",padnum)..("%3d"):format(#a) end)
  return o
end

function getFilesInDir(dir,ext)
  files = {}

  -- Go over all files in directory. We use an iterator, paths.files().
  for file in paths.files(dir) do
     -- We only load files that match the extension
     if file:find(ext .. '$') then
        -- and insert the ones we care about in our table
        table.insert(files, paths.concat(dir,file))
     end
  end

  -- Check files
  if #files == 0 then
     error('given directory doesnt contain any files of type: ' .. ext)
  end
  
  -- 3. Sort file names

  -- We sort files alphabetically, it's quite simple with table.sort()

  --table.sort(files, function (a,b) return a < b end)
  
  files = alphanumsort(files)

  print('Found files:')
  print(#files)
  
  return files
end

function run(input)
  lfs.mkdir('playpen')

  local maskedOutput = {}
  local groundTruth = {}

	table.insert(input, h0)
  local stopWatch = torch.Timer()
	local output = model:forward(input)
        print("Model forward time "..stopWatch:time().real.." sec")
	-- temporarily switch to FloatTensor as image does not work otherwise.
	torch.setdefaulttensortype('torch.FloatTensor')
	for i = 1,#input-1 do
		--image.save('video_' .. params.model .. '/input' .. i .. '.png',  input[i][2] / 2 + input[i][1])
		--image.save('video_' .. params.model .. '/output' .. i .. '.png', input[i][2] / 2 + output[i])
    maskedOutput[i] = extractVisiblePredictions(output[i]:float(),input[i][2]:float())
    groundTruth[i] = input[i][1]:float()

    matio.save('playpen'.. '/input' .. i .. '.mat',  (input[i][2] / 2 + input[i][1]):float())
    matio.save('playpen'..'/output' .. i .. '.mat', (input[i][2] / 2 + output[i]):float())
    matio.save('playpen'..'/output_raw' .. i .. '.mat', output[i]:float())
    matio.save('playpen'..'/input_raw' .. i .. '.mat', input[i][1]:float())
  end
	torch.setdefaulttensortype(DEFAULT_TENSOR_TYPE)
  print("F1 Score "..(f1Score(maskedOutput,groundTruth)))
  
  return output
end

function loadGrids(dir,num)
  local gridFiles = getFilesInDir(dir,'.grid')
  local grids = {}
  
  for i = 1,num do
    grids[i] = torch.load(gridFiles[i])
    if params.gpu > 0 then
      grids[i] = grids[i]:cuda()
    end
  end
  return grids
end
