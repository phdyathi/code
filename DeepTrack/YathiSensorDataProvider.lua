require('GetFilesInDir')
_SDPFileList=nil
function getSequence(i)
	local input = {}
	for j = 1,params.N do
		input[j] = torch.load(_SDPFileList[(i-1) * params.N + j]):cuda()
	end
	return input
end

function InitSDP(dir)
  _SDPFileList =  getFilesInDir(dir,'.grid');
  local data = torch.load(_SDPFileList[1])
  local width  = (#data)[3] -- occupancy 2D grid width
  local height = (#data)[4] -- occupancy 2D grid height
  local depth = (#data)[2] --occupancy 2D grid depth
  local M = math.floor((#_SDPFileList) / params.N) -- total number of training sequences
  return width,height,depth,M
end
