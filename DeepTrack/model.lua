--[[
DeepTracking: Seeing Beyond Seeing Using Recurrent Neural Networks.
Copyright (C) 2016  Peter Ondruska, Mobile Robotics Group, University of Oxford
email:   ondruska@robots.ox.ac.uk.
webpage: http://mrg.robots.ox.ac.uk/

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
--]]

nngraph.setDebug(true)

-- h0
function getInitialState(width, height,depth)
    return torch.zeros(8, height, width,depth):cuda()
end

-- One step of RNN --
-- {h0,x1} -> {h1,y1}
function getStepModule(width, height, depth)
	local h0 = nn.Identity()() --input 1 hidden state from previous step
	local x1 = nn.Identity()() -- input 2 

	local e  = nn.Sigmoid()( nn.VolumetricConvolution(2, 4, 7, 7, 7, 1, 1, 1, 3, 3, 3)(x1) )
	local j  = nn.JoinTable(1)({e,h0}) -- join t-1's h1 output onto e
	local h1 = nn.Sigmoid()( nn.VolumetricConvolution(12, 8, 7, 7, 7, 1, 1, 1, 3, 3, 3)(j) ) -- 48 cuz 16(input) + 32(t-1 hidden state)
	local y1 = nn.Sigmoid()( nn.VolumetricConvolution(8, 1, 7, 7, 7, 1, 1, 1, 3, 3, 3)(h1) )

	local model = nn.gModule({h0, x1}, {h1, y1})
	model = cudnn.convert(model,cudnn)
	print(model:get(2))
	return model:cuda()
end
