require('torch')
cmd = torch.CmdLine()

require('lfs')
require('nngraph')
require('image')
require('Recurrent')
--local matio = require 'matio'
require('evaluationUtils')
require('GetFilesInDir')

cmd:option('-gpu', 1, 'use GPU')
cmd:option('-N', 100, 'training sequence length')
cmd:option('-model', './model.lua', 'neural network model')
cmd:option('-evalName', 'evalResults', 'neural network model')
cmd:option('-evalDir','./eval/','the output dir')
cmd:option('-weightsPath', './weights/10.dat', 'Weights to load')
cmd:option('-testDataDir', './trainingData/', 'Where the test data is located')
cmd:option('-SDP','YathiSensorDataProvider','What sensor data provider to use')
cmd:option('-X', 31, 'X dimension size')
cmd:option('-Y', 31, 'Y dimension size')
cmd:option('-Z', 31, 'Z dimension size')
cmd:option('-timeStart', 0, 'Time horizon start')
cmd:option('-timeEnd', 2.5, 'Time horizon end')
cmd:option('-timeInterval', 0.5, 'Time horizon interval')
cmd:option('-timeWeightFile', '', 'Time horizon weight file, if this is not specified eval output is used')

params = cmd:parse(arg)
print(params)

if params.gpu > 0 then
  print('Using GPU ' .. params.gpu)
  require('cunn')
  require('cutorch')
  require('cudnn')
  cutorch.setDevice(params.gpu)
  --DEFAULT_TENSOR_TYPE = 'torch.CudaTensor'
  DEFAULT_TENSOR_TYPE = 'torch.FloatTensor'
else
  print('Using CPU')
  DEFAULT_TENSOR_TYPE = 'torch.FloatTensor'
end

torch.setdefaulttensortype(DEFAULT_TENSOR_TYPE)

--load sensor data provider
require(params.SDP)
width,height,depth,M = InitSDP(params.testDataDir)
print('Occupancy grid has size ' .. width .. 'x' .. height .. 'x' .. depth)
print('Number of sequences ' .. M)
SequenceSizeBytes = (width*height*depth*params.N*2*4) --total size of a single training sequence

-- load neural network model
package.path = package.path .. ";"..params.model
if not require(paths.basename(params.model,'.lua')) then
  print("Error couldn't load model.lua from: "..params.model)
  os.exit()
end
print(package.path)

h0 = getInitialState(width, height,depth)
-- one step of RNN
step = getStepModule(width, height,depth)

w, _ = step:getParameters()
function loadWeights(fileW)

  print("Model has ".. (#w)[1] .. ' parameters')
  if #fileW > 0 then
    print('Loading weights ' .. fileW)
    local loadedW = torch.load(fileW)
    print('Loading '.. (#loadedW)[1] .. ' parameters')
    w:copy(loadedW)
  end
end
-- chain N steps into a recurrent neural network
-- N = sequence length; so 1 step per time step? Is this the memory capacity?
model = Recurrent(step, params.N,torch.LongStorage({width,height,depth}),h0:size(),torch.LongStorage({width,height,depth,2}))


--------------
-- FUNCTIONS
--------------
batch = nil
batchSize=nil
function getBatch(currCounter)
  
  if not batchSize then
    local freeMem, totalMem = cutorch.getMemoryUsage(params.gpu)
    --leave some memory free
    freeMem = freeMem - 10e8
    batchSize =  math.floor(freeMem/SequenceSizeBytes)  
  end
  
  if not batch then
    batch = {}
  end
  
  print("Loading new batch of size "..batchSize)
  for i = 1,batchSize do
    local batchIndx = currCounter
    if not batch[i] then
      batch[i] = getSequence(torch.IntTensor().random(M))
    else
      local temp = getSequence(torch.IntTensor().random(M))
      for j = 1,params.N do
        batch[i][j]:copy(temp[j])
      end
    end
  end
end

batchCount = nil
function retrieveSequence()
  --if !batchCount fails to short circuit this will crash
  print("not batchCount "..tostring(not batchCount))
  print("(batchSize and (batchCount > batchSize)) "..tostring(batchSize and (batchCount > batchSize)))
  if not batchCount or (batchSize and (batchCount > batchSize)) then
    batchCount = 1
    
    getBatch()
  end
  
  print(batchCount)
  print(#batch)
  
  local temp = batch[batchCount]
  batchCount = batchCount + 1
  return temp
end

-- blanks part of the sequence for predictive training
function dropoutInput(target, numFramesToBlank)
	local input = {}
  local blankAfter = #target - numFramesToBlank
	for i=1,#target do
		input[i] = target[i]:clone()
		if i > blankAfter then
        --print("Blank out "..i)
		    input[i]:zero()
		end
	end
	return input
end

function evalSequence(input,pGroundTruth)
  lfs.mkdir('playpen')

  local maskedOutput = {}
  local groundTruth = {}
  local rawOutput = {}

	table.insert(input, h0)
  local stopWatch = torch.Timer()
	local output = model:forward(input)
	local runtime= stopWatch:time().real
        print("Model forward time "..runtime.." sec")
	-- temporarily switch to FloatTensor as image does not work otherwise.
	torch.setdefaulttensortype('torch.FloatTensor')
	for i = 1,#input-1 do
    rawOutput[i] = output[i]:float()
    if(pGroundTruth == nil) then
      maskedOutput[i] = extractVisiblePredictions(output[i]:float(),input[i][2]:float())
      groundTruth[i] = input[i][1]:float()
    else
      maskedOutput[i] = extractVisiblePredictions(output[i]:float(),pGroundTruth[i][2]:float())
      groundTruth[i] = pGroundTruth[i][1]:float()
    end
    --matio.save('playpen'.. '/input' .. i .. '.mat',  (input[i][2] / 2 + input[i][1]):float())
    --matio.save('playpen'..'/output' .. i .. '.mat', (input[i][2] / 2 + output[i]):float())
    --matio.save('playpen'..'/output_raw' .. i .. '.mat', output[i]:float())
    --matio.save('playpen'..'/input_raw' .. i .. '.mat', input[i][1]:float())
  end
	torch.setdefaulttensortype(DEFAULT_TENSOR_TYPE)
  local f1score = (f1Score(maskedOutput,groundTruth))
  
  return f1score,runtime,rawOutput
end

_RENDER_FRAME_RATE = 23
function evalSeqTimeHorizon(input,timeToBlankOutSec)
  local num_frames_to_blank = math.floor(timeToBlankOutSec*_RENDER_FRAME_RATE)
  print("Blanking out ",num_frames_to_blank," frames")
  print("Blanking out ",num_frames_to_blank,'/',#input,' frames')
  if(#input <= num_frames_to_blank) then
    print("Attempting to blank out too many frames! Exiting...")
    os.exit()
  end
  
  local blankedInput = dropoutInput(input,num_frames_to_blank)
  
  return evalSequence(blankedInput,input)
end

function outputInputOutputGrids(outDir,M,input,output)
    for k=1,#output do
      torch.save(outDir..'output_'..M..'_'..k..'.grid',output[k]:float())
    end 
    for k=1,#input do
      torch.save(outDir..'input_'..M..'_'..k..'.grid',input[k]:float())
    end 
end

function runTimeHorizonEval(timeHorizonStart,timeHorizonEnd,timeInterval,weightFile)
  local f1scoreList = torch.Tensor(M)
	local runtimeList = torch.Tensor(M) 
  
  loadWeights(weightFile)
  
  local timeRange = torch.range(timeHorizonStart,timeHorizonEnd,timeInterval)
  
  for t = 1,(#timeRange)[1] do
    for i = 1,M do
      local input =  getSequence(i)
      local f1,time,output = evalSeqTimeHorizon(input,timeRange[t])
      f1scoreList[i] = f1
      runtimeList[i] = time
      rawOutDir = params.evalDir..'timeHorizon/' 
      lfs.mkdir(rawOutDir)
      outputInputOutputGrids(rawOutDir,i,input,output);
    end
    

    local f1Max = torch.max(f1scoreList)
    local runtimeMax = torch.max(runtimeList)

    local f1Min = torch.min(f1scoreList)
    local runtimeMin = torch.min(runtimeList)
    
    local f1Mean = torch.mean(f1scoreList)
    local runtimeMean = torch.mean(runtimeList)

    print('f1 Max:'..f1Max)
    print('f1 Min:'..f1Min)
    print('f1 Mean:'..f1Mean)
    
    print('Time Max:'..runtimeMax)
    print('Time Min:'..runtimeMin)
    print('Time Mean:'..runtimeMean)
    
    lfs.mkdir(params.evalDir)
    
    local fd = io.open(params.evalDir..params.evalName..'_TimeHorizon'..'.txt','a+')
    fd:write(timeRange[t]..','..f1Mean..','..runtimeMean..'\n')
    fd:close()
  end
  
end

function runEvaluation(weightsFile)
	local f1scoreList = torch.Tensor(M)
	local runtimeList = torch.Tensor(M) 
  
  loadWeights(weightsFile)
  
  lfs.mkdir(params.evalDir)

	for i = 1,M do
		local input = getSequence(i)
		local f1,time,output = evalSequence(input)
		f1scoreList[i] = f1
		runtimeList[i] = time
        rawOutDir = params.evalDir..'normal/' 
        lfs.mkdir(rawOutDir)
        outputInputOutputGrids(rawOutDir,i,input,output)
	end
	

	local f1Max = torch.max(f1scoreList)
	local runtimeMax = torch.max(runtimeList)

	local f1Min = torch.min(f1scoreList)
	local runtimeMin = torch.min(runtimeList)
	
	local f1Mean = torch.mean(f1scoreList)
	local runtimeMean = torch.mean(runtimeList)

	print('f1 Max:'..f1Max)
	print('f1 Min:'..f1Min)
    print('f1 Mean:'..f1Mean)
	
	print('Time Max:'..runtimeMax)
	print('Time Min:'..runtimeMin)
	print('Time Mean:'..runtimeMean)
  
  local fd = io.open(params.evalDir..params.evalName..'.txt','a+')
  fd:write(f1Max..','..f1Min..','..f1Mean..','..runtimeMax..','..runtimeMin..','..runtimeMean..'\n')
  fd:close()
  
  return f1Mean;
end

function loadGrids(dir,num)
  local gridFiles = getFilesInDir(dir,'.grid')
  local grids = {}
  
  for i = 1,num do
    grids[i] = torch.load(gridFiles[i])
    if params.gpu > 0 then
      grids[i] = grids[i]:cuda()
    end
  end
  return grids
end


local meanf1 = runEvaluation(params.weightsPath)

