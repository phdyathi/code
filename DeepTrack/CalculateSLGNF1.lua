require('torch')
cmd = torch.CmdLine()
require('lfs')
require('evaluationUtils')
require('GetFilesInDir')
require('io')
require('os')
require('Simple-CSV')

cmd:option('-X', 31, 'X dimension size')
cmd:option('-Y', 31, 'Y dimension size')
cmd:option('-Z', 31, 'Z dimension size')
cmd:option('-N', 10, 'training sequence length')
cmd:option('-outputDir', './SLGN_Eval/', 'Output dir')
cmd:option('-evalName', 'evalResults', 'name of experiment')
cmd:option('-groundDir','./testGrids/','directory with the')
cmd:option('-csvFile', './slgn_result.csv', 'Dir where csv files are')
cmd:option('-slgnTrainingGridsDir', './SLGN_trainingGrids/', 'directory where TORCH grids use to train SLGN')
params = cmd:parse(arg)
print(params)

trainingGridList = getFilesInDir(params.slgnTrainingGridsDir,'grid')
groundTruthGridList = getFilesInDir(params.groundDir,'grid')

lfs.mkdir(params.outputDir)

print('Reading '..params.csvFile)
csvReader = SimpleCSV.new()
csvReader:load_csvfile(params.csvFile)
print('Found '..#csvReader.csv_table..' records')

f1scoreList = torch.Tensor(math.floor((#csvReader.csv_table) / params.N))
mseScoreList = torch.Tensor(math.floor((#csvReader.csv_table) / params.N))
tppScoreList = torch.Tensor(math.floor((#csvReader.csv_table) / params.N))

local maskedOutput = {}
local groundTruth = {}
count = 1
resultNum = 1

for record=1,(#csvReader.csv_table) do
    print('Processing record '..record)
    --print(csvReader.csv_table[record])

    --get the grid associated with SLGN prediction
    slgnAssociatedGrid = csvReader.csv_table[record][1]
    slgnAssociatedGrid = torch.load(trainingGridList[tonumber(slgnAssociatedGrid)])
    --the ground truth is +1 the current frame
    groundTruth[count] = torch.load(groundTruthGridList[record+1])
    maskedOutput[count] = extractVisiblePredictions(slgnAssociatedGrid[1],groundTruth[count][2])
    groundTruth[count] = groundTruth[count][1]

    --reset count
    if((count % params.N) == 0) then
        --calculate F1 for N frames
        f1scoreList[resultNum] = f1Score(maskedOutput,groundTruth)
        mseScoreList[resultNum] = mseScore(maskedOutput,groundTruth)
        tppScoreList[resultNum] = truePositivePrecentage(maskedOutput,groundTruth)
        resultNum = resultNum + 1
        count  = 0
        maskedOutput = {}
        groundTruth = {}
    end
    count = count + 1
end

local f1Max = torch.max(f1scoreList)

local f1Min = torch.min(f1scoreList)

local f1Mean = torch.mean(f1scoreList)

local mseMax = torch.max(mseScoreList)

local mseMin = torch.min(mseScoreList)

local mseMean = torch.mean(mseScoreList)

local tppMax = torch.max(tppScoreList)
local tppMin = torch.min(tppScoreList)
local tppMean = torch.mean(tppScoreList)

print(#f1scoreList)
print('f1 Max:'..f1Max)
print('f1 Min:'..f1Min)
print('f1 Mean:'..f1Mean)

print(#mseScoreList)
print('mse Max:'..mseMax)
print('mse Min:'..mseMin)
print('mse Mean:'..mseMean)

print(#tppScoreList)
print('tpp Max:'..tppMax)
print('tpp Min:'..tppMin)
print('tpp Mean:'..tppMean)


local fd = io.open(params.outputDir..params.evalName..'.txt','a+')
fd:write(tppMax..','..tppMin..','..tppMean..','..f1Max..','..f1Min..','..f1Mean..','..mseMax..','..mseMin..','..mseMean..'\n')
fd:close()

--print full file
fd = io.open(params.outputDir..params.evalName..'_full.txt','a+')
for i = 1,(#f1scoreList)[1] do
    fd:write(tppScoreList[i]..','..f1scoreList[i]..','..mseScoreList[i]..'\n')
end
fd:close()
