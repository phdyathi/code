-- return i-th training sequence
-- TODO: sensor data is 1D array. We may not be able to fit 2D 
require('SensorData')
data = nil

function getSequence(i)
	local input = {}
	for j = 1,params.N do
		input[j] = data[(i-1) * params.N + j]:cuda()
	end
	return input
end

function InitSDP()
  -- load training data
  print('Loading training data from file ' .. 'data.t7')
  --data = torch.load(params.data) -- load pre-processed 2D grid sensor input
  data = LoadSensorData("data.t7", params)
  local width  = (#data)[4] -- occupancy 2D grid width
  local height = (#data)[3] -- occupancy 2D grid height
  local depth = (#data)[5] --occupancy 2D grid depth
  local M = math.floor((#data)[1] / params.N) -- total number of training sequences
  return width,height,depth,M
end