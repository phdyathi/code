import os

testDataDir ="./evalData/"
destDir = "./eval/"
calibTool = "evalJob.script"
weightsDir = "./weights_model/"

testDirs = [d for d in os.listdir(testDataDir) if os.path.isdir(os.path.join(testDataDir, d))]


for d in testDirs:
    print(testDirs)
    testName = d+"Results"
    testDir  = testDataDir+d+"/"
    command = "sbatch "+calibTool+" '"+testName+"' '"+weightsDir+"' '"+testDir+"'"
    print(command)
    os.system(command)
