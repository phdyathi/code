#launches a massive job

import os
import glob
import re
slurmScript = "HGNPostprocessor.script"
outputDir = "/home/yathindh/Monash120/deepTrackFlyExperiment/OcclusionDataFix_V2_transposed_additionalTraining/1k_10k_results_withTPP/hgn/"
hgn_csvDir = "/home/yathindh/Monash120/deepTrackFlyExperiment/OcclusionDataFix_V2_transposed_additionalTraining/1000_10000_results/hgn/raw/"
testGridFilesDir = "/home/yathindh/Monash120/DeepTrackEvalData/1000_10000_1000_5kFramesGrid/data/"
slgnTrainingDir = "/home/yathindh/Monash120/DeepTrackTrainingData/calibratedSwarmData_4/trainingData/torchTrainingData/"

#get all csv files in hgn_csvDir
hgnResultFiles = [i for i in glob.glob(hgn_csvDir+'*.csv')]
print(hgnResultFiles)

for csvFile in hgnResultFiles:
    base  = os.path.basename(csvFile)
    filename = os.path.splitext(base)[0]
    testDir = testGridFilesDir + (re.match(r'(\d+)Result',filename)).group(1) + "/SLGN/sampledGrids/"
    command = "sbatch "+slurmScript+" "+outputDir+" "+filename+" "+testDir+" "+slgnTrainingDir+" "+csvFile
    print(command)
    os.system(command)
