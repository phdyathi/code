require('lfs')
require('torch')
cmd = torch.CmdLine()
require('evaluationUtils')
require('GetFilesInDir')

cmd:option('-groundTruthDir', './groundTruth/', 'Directory with ground truth grids')
cmd:option('-testDir', './testDir/', 'Directory with experimental out put')
cmd:option('-N', 100, 'sequence length')
cmd:option('-evalName', 'chenEtAlEval', 'eval output folder name (this will be created if not pre-existing)')
cmd:option('-outDir','./','Output directory')

params = cmd:parse(arg)
print(params)

testFiles = getFilesInDir(params.testDir,'.grid')
groundTruthFiles = getFilesInDir(params.groundTruthDir,'.grid')

--Validate
if #testFiles <= 0 or #groundTruthFiles <= 0 or #testFiles ~= #groundTruthFiles then
  print('Check that:\n Test directory is not empty\n Ground Truth dir is not empty\n That the number of files in those 2 dirs matches\n Exiting...')
  os.exit()
end
print("Test files: ",#testFiles)
print("Ground Truth files: ",#groundTruthFiles)


function getSequence(i)
	local testData = {}
  local groundTruthData = {}
  print('Loading sequence from '..((i-1)*params.N+1)..' to '..((i-1)*params.N+params.N))
  print(testFiles[(i-1)*params.N+1])
  print(testFiles[(i-1) * params.N + params.N])
  print(testFiles[(i-1)*params.N+81])
  for j = 1,params.N do
		testData[j] = torch.load(testFiles[(i-1) * params.N + j])
    groundTruthData[j] = torch.load(groundTruthFiles[(i-1) * params.N + j])
	end
	return testData,groundTruthData
end

function evalSequence(testSequence,groundTruthSeq)
  local maskedTestData = {}
  local groundTruth = {}

  for i =1, #testSequence do
    --The test output is based off the centroids of the detected objects as per Cheng et al
    --so maybe as long as some part of the test overlaps a ground truth blob it's ok?
    --This doesn't mean ignore false positives!
    --print('testData',#(testSequence[i]))
    --print('groundTruthSeq',#(groundTruthSeq[i][2]))
    --print('i=',i)

    maskedTestData[i] = extractVisiblePredictions(testSequence[i][1],groundTruthSeq[i][2])
    groundTruth[i] = groundTruthSeq[i][1]
  end

  local f1score = f1Score(maskedTestData,groundTruth)
  local mse = mseScore(maskedTestData,groundTruth)
  print('F1 score',f1score)
  print('MSE ',mse)
  return f1score,mse
end

function runEvaluation()
  local M = math.floor((#testFiles) / params.N)

  local f1scoreList = torch.Tensor(M)
  local mseList = torch.Tensor(M)

  for i = 1, M do
    local testSequence, groundTruthSeq = getSequence(i)
    local f1,mse = evalSequence(testSequence,groundTruthSeq)

    f1scoreList[i] = f1
    mseList[i] = mse
  end

  local f1Max = torch.max(f1scoreList)

  local f1Min = torch.min(f1scoreList)

  local f1Mean = torch.mean(f1scoreList)

  local mseMax = torch.max(mseList)

  local mseMin = torch.min(mseList)

  local mseMean = torch.mean(mseList)

  print(f1scoreList)
  print(mseList)

  print('f1 Max:'..f1Max)
  print('f1 Min:'..f1Min)
  print('f1 Mean:'..f1Mean)

  print('mse Max:'..mseMax)
  print('mse Min:'..mseMin)
  print('msef1 Mean:'..mseMean)

  evalDir = params.outDir
  lfs.mkdir(evalDir)

  local fd = io.open(evalDir..params.evalName..'.txt','a+')
  fd:write(f1Max..','..f1Min..','..f1Mean..','..mseMax..','..mseMin..','..mseMean..',0,0,0\n')
  fd:close()

  return f1Mean,mseMean;

end

runEvaluation()
