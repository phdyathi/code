function PlotTimeHorizon(evalDir,outDir)

    files=dir([evalDir,'*.txt']);
    filenames = cell(1,length(files))
    for i=1:length(files)        
        filenames{i} = [files(i).name];
    end
    filenames = natsortfiles(filenames)
    sz = size(files)
    disp(sz(1))
    numFiles = sz(1)
    
    data = []
    disp(filenames)
    % read in all csv files
    % each file read is appended to the 3rd dimension
    for i=1:length(files)
        csvdata=csvread([evalDir,filenames{i}]);
        %now do something
        if isempty(data)
            data = csvdata
        else
            data = cat(3,data,csvdata)
        end
    end
    disp(size(data))
    

    %draw gragh
    selData = data(:,:,[1,5,10,11,12,13])
    dataSz = size(selData)
    figure
    for i=1:dataSz(3)
        plot(selData(:,1,i),selData(:,2,i))
        hold all
    end
    xlabel('Prediction time horizon [sec]') % x-axis label
    ylabel('Mean F1 Score') % y-axis label
    legend('Swarm Size=10','Swarm Size=50','Swarm Size=100','Swarm Size=200','Swarm Size=300','Swarm Size=400','Location','northeast')
    saveas(gcf,strcat(outDir,'timeHorizon'),'epsc')
    close
    
end