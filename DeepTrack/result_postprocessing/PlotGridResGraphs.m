function PlotGridResGraphs(evalDir1,evalDir2,outDir)
    files1 = readSortedFilenames(evalDir1);
    files2 = readSortedFilenames(evalDir2);

    data1 = readData(evalDir1,files1);
    data2 = readData(evalDir2,files2);

    %plot mean F1 with increasing swarm size for both resolutions in one
    %graph
    %plot changes to F1 after 50k training iterations 
    meanF150kData1 = reshape(data1(97,3,:),[length(files1),1])
    swarmSizeData1 = [10,20,30,40,50,60,70,80,90,100,200,300,400]
    meanF150kData2 = reshape(data2(97,3,:),[length(files2),1])
    swarmSizeData2 = [50,100,150,200,250,300,350,400,450,500,550,600]
    figure
    plot(swarmSizeData1,meanF150kData1,'*-',swarmSizeData2,meanF150kData2,'+-')
    xlabel('Swarm Size') % x-axis label
    ylabel('Mean F1 Score') % y-axis label
    legend('31x31x31','62x62x31','Location','southeast')
    saveas(gcf,strcat(outDir,'gridSize_'),'epsc')
    close
end

function filenames = readSortedFilenames(dirPath)
    files=dir([dirPath,'*.txt']);
    filenames = cell(1,length(files))
    for i=1:length(files)        
        filenames{i} = [files(i).name];
    end
    filenames = natsortfiles(filenames)
end

function data = readData(dirPath,filenames)
    data = []
    
    % read in all csv files
    % each file read is appended to the 3rd dimension
    for i=1:length(filenames)
        csvdata=csvread([dirPath,filenames{i}]);
        %now do something
        if isempty(data)
            data = csvdata
        else
            data = cat(3,data,csvdata)
        end
    end
end