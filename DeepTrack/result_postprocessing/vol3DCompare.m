function vol3DCompare( groundTruthVol, testVol)
%VOL3DCOMPARE Summary of this function goes here
%   Plots the two volumes over each other

if(ndims(groundTruthVol) == 4)
    disp('Converting 4D vol to 3D..')
    gSize = size(groundTruthVol)
    groundTruthVol = reshape(groundTruthVol(1,:,:,:),[gSize(2),gSize(3),gSize(4)])
end

fig=figure;
subplot(1,2,1);
vol3d('CData',groundTruthVol);
axis([0 31 0 31 0 31])
zoom(2.3)
y = ylim;
ylim(y+5);
title('Ground Truth')
grid on;
view(3);
%freezeColors;
subplot(1,2,2);
vol3d('CData',testVol);
axis([0 31 0 31 0 31])
zoom(2.3)
y = ylim;
ylim(y+5);
title('Network Output')
grid on;
%colormap cool;
view(3);
end