function PlotSingleEval( inputFileName , outDir)
%PLOTSINGLEEVAL Plot eval for a single file

csvdata=csvread(inputFileName);

sz = size(csvdata);
fprintf('Read csv: %i\n',sz);

xAxisData = linspace(500,21500,sz(1))

% plot F1 for each swarm size
figure
plot(xAxisData,csvdata(:,1),xAxisData,csvdata(:,2),xAxisData,csvdata(:,3))
xlabel('Training Iterations') % x-axis label
ylabel('F1 Score') % y-axis label
legend('Max F1','Min F1','Mean F1','Location','southeast')
[pathstr,name,ext] = fileparts(inputFileName)
saveas(gcf,strcat(outDir,'F1MaxMinMean_',name),'epsc')
close

end

