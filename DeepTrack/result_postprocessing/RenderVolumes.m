% Renders all the mat files in a directory to volumes 

% IMPORTANT CONFIGURE THESE VARS BEFORE RUN!
% input dir
inputDir = './normal/';
seqNum = 3;
groundTruthFileNameTemplate = 'input_%d_%d.mat'
outputFileNameTemplate = 'output_%d_%d.mat'
% output dir
outputDir = './renders/';
% volume size
volSize = [31,31,31]

mkdir(outputDir);

files=dir([inputDir,'*.mat'])

numFiles = length(files)/4

for i=1:100
    groundTruthVol  = load(sprintf(groundTruthFileNameTemplate,seqNum,i));
    groundTruthVol = groundTruthVol.x;
    groundTruthVol = reshape(groundTruthVol(1,:,:,:),[31,31,31])
    testVol = load(sprintf(outputFileNameTemplate,seqNum,i));
    testVol = testVol.x;
    vol3DCompare(groundTruthVol,testVol);
    %title(sprintf(groundTruthFileNameTemplate,i))
    %set(gcf,'PaperPositionMode','auto')
    saveas(gcf,sprintf('%s%d_%d.png',outputDir,seqNum,i),'png');
    %print(sprintf('%s%d.jpg',outputDir,i),'-djpeg')
    close all;
end

disp('Done!')