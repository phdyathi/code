function ReadAndPlotEval(evalDir,outDir,bPlotMixMaxMean,bPlotMeanComp,bPlotSwarmSize)

    if(exist('bPlotMixMaxMean','var')==0)
        bPlotMixMaxMean = 1;
    end
    if(exist('bPlotMeanComp','var')==0)
        bPlotMeanComp = 1;
    end
    if(exist('bPlotSwarmSize','var')==0)
        bPlotSwarmSize = 1;
    end
    

    trainingItersMin=100
    trainingItersMax=100000
    trainingItersNum=7

    fullSampleSize = 50
    fullFiles = dir([strcat(evalDir,'full/'),'*.txt']); 
    fullSampleFileNames = cell(1,length(fullFiles))
    for i=1:length(fullFiles)        
        fullSampleFileNames{i} = [fullFiles(i).name];
    end
    fullSampleFileNames = natsortfiles(fullSampleFileNames)
    
    files=dir([evalDir,'*.txt']);
    filenames = cell(1,length(files))
    for i=1:length(files)        
        filenames{i} = [files(i).name];
    end
    filenames = natsortfiles(filenames)
    sz = size(files)
    disp(sz(1))
    numFiles = sz(1)
    
    compSwarmTrainingInterval = 500;
    compMeanSwarmSizeInterval = 1000;
    swarmSizeRange = linspace(1000,10000,length(files));
    
    swarmCount =1;
    swarSizeStrings = cell(1,1);
    for i=1:length(swarmSizeRange)
        if(mod(swarmSizeRange(i),compMeanSwarmSizeInterval) == 0)
            swarmSizeStrings{swarmCount} =strcat('Swarm Size = ',num2str(swarmSizeRange(i)));
            swarmCount = swarmCount + 1;
        end
    end
    
    data = zeros(trainingItersNum,9,1)
    if(exist(strcat(outDir,'MinMaxMean.mat'), 'file'))
        data =load(strcat(outDir,'MinMaxMean.mat'))
        data = data.data;
    else
        % read in all csv files
        % each file read is appended to the 3rd dimension
        for i=1:length(files)
            csvdata=csvread([evalDir,filenames{i}]);
            %now do something
            if i==1
                data(:,:,1) = csvdata
            else
                data = cat(3,data,csvdata)
            end
        end
        save(strcat(outDir,'MinMaxMean.mat'),'data');
    end
    disp(size(data))
    dataSz = size(data)
    
    xAxisData = linspace(trainingItersMin,trainingItersMax,trainingItersNum)
    if(bPlotMixMaxMean)
        %loop through the 3rd dimension producing F1 and time grapghs for each 
        for i=1:length(files)
            % plot F1 for each swarm size
            plotF1MinMaxMean(xAxisData,data,i,filenames,outDir);
            plotMSEMinMaxMean(xAxisData,data,i,filenames,outDir);
        end
    end
    
    compMeanF1Data = data(:,3,:)
    compMeanMseDta = data(:,6,:)
    %compMeanF1Data = data(:,3,:)
    %plot to compare mean F1 in all swarm size
    compMeanSize = size(compMeanF1Data)
   
    if(bPlotMeanComp)
        plotF1MeanCompare(swarmSizeRange,compMeanSwarmSizeInterval,swarmSizeStrings,xAxisData,outDir,compMeanF1Data)
        plotMseMeanCompare(swarmSizeRange,compMeanSwarmSizeInterval,swarmSizeStrings,xAxisData,outDir,compMeanMseDta)
    end
    
    if(bPlotSwarmSize)
        for i = 1:length(xAxisData)
            if(mod(xAxisData(i),compSwarmTrainingInterval)==0)
                plotF1SwarmSize(data,i,swarmSizeRange,trainingItersMax,trainingItersNum,xAxisData,outDir,files)
                plotMseSwarmSize(data,i,swarmSizeRange,trainingItersMax,trainingItersNum,xAxisData,outDir,files)
            end
        end
    end
end

function plotF1MinMaxMean(xAxisData,data,i,filenames,outDir)
    figure
    plot(xAxisData,data(:,1,i),xAxisData,data(:,2,i),xAxisData,data(:,3,i))
    xlabel('Training Iterations') % x-axis label
    ylabel('F1 Score') % y-axis label
    legend('Max F1','Min F1','Mean F1','Location','southeast')
    [pathstr,name,ext] = fileparts(filenames{i})
    saveas(gcf,strcat(outDir,'F1MaxMinMean_',name),'epsc')
    savefig(gcf,strcat(outDir,'F1MaxMinMean_',name))
    close
end

function plotMSEMinMaxMean(xAxisData,data,i,filenames,outDir)
    figure
    plot(xAxisData,data(:,4,i),xAxisData,data(:,5,i),xAxisData,data(:,6,i))
    xlabel('Training Iterations') % x-axis label
    ylabel('MSE') % y-axis label
    legend('Max MSE','Min MSE','Mean MSE','Location','southeast')
    [pathstr,name,ext] = fileparts(filenames{i})
    saveas(gcf,strcat(outDir,'MseMaxMinMean_',name),'epsc')
    savefig(gcf,strcat(outDir,'MseMaxMinMean_',name))
    close
end

function plotF1MeanCompare(swarmSizeRange,compMeanSwarmSizeInterval,swarmSizeStrings,xAxisData,outDir,compMeanF1Data)
    figure
    for i=1:length(swarmSizeRange)
        if(mod(swarmSizeRange(i),compMeanSwarmSizeInterval)==0)
            plot(xAxisData,compMeanF1Data(:,:,i))
            hold all
        end
    end

    xlabel('Training Iterations') % x-axis label
    ylabel('Mean F1 Score') % y-axis label
    %legend('Swarm size = 500','Swarm size = 550','Swarm size = 600','Swarm size = 650','Swarm size = 700','Swarm size = 750','Swarm size = 800','Swarm size = 850','Swarm size = 900','Swarm size = 950','Swarm size = 1000','Location','southeast')
    legend(swarmSizeStrings,'Location','southeast')
    saveas(gcf,strcat(outDir,'F1Mean'),'epsc')
    savefig(gcf,strcat(outDir,'F1Mean'))
    close
end

function plotMseMeanCompare(swarmSizeRange,compMeanSwarmSizeInterval,swarmSizeStrings,xAxisData,outDir,compMeanMseDta)
    figure
    for i=1:length(swarmSizeRange)
        if(mod(swarmSizeRange(i),compMeanSwarmSizeInterval)==0)
            plot(xAxisData,compMeanMseDta(:,:,i))
            hold all
        end
    end

    xlabel('Training Iterations') % x-axis label
    ylabel('Mean MSE Score') % y-axis label
    %legend('Swarm size = 500','Swarm size = 550','Swarm size = 600','Swarm size = 650','Swarm size = 700','Swarm size = 750','Swarm size = 800','Swarm size = 850','Swarm size = 900','Swarm size = 950','Swarm size = 1000','Location','southeast')
    legend(swarmSizeStrings,'Location','southeast')
    saveas(gcf,strcat(outDir,'MseMean'),'epsc')
    savefig(gcf,strcat(outDir,'MseMean'))
    close
end

function plotF1SwarmSize(data,i,swarmSizeRange,trainingItersMax,trainingItersNum,xAxisData,outDir,files)
    %plot changes to F1 after 50k training iterations 
    meanF150kData = reshape(data(i,3,:),[length(files),1])
    minF1Data  = reshape(data(i,2,:),[length(files),1])
    maxF1Data = reshape(data(i,1,:),[length(files),1])
    %swarmSizeData = [500,550,600,650,700,750,800,850,900,950,1000]
    swarmSizeData = swarmSizeRange;
    %swarmSizeData = [50,100,150,200,250,300,350,400,450,500,550,600]
    figure
    plot(swarmSizeData,meanF150kData,swarmSizeData,minF1Data,swarmSizeData,maxF1Data)
    xlabel('Swarm Size') % x-axis label
    ylabel('Mean F1 Score') % y-axis label
    legend('Mean F1','Min F1','Max F1','Location','southeast')
    title(strcat('Training iterations: ',num2str(i*(trainingItersMax/trainingItersNum))))
    saveas(gcf,strcat(outDir,'F1SwarmSize_',num2str(xAxisData(i))),'epsc')
    savefig(gcf,strcat(outDir,'F1SwarmSize_',num2str(xAxisData(i))))
    close
end

function plotMseSwarmSize(data,i,swarmSizeRange,trainingItersMax,trainingItersNum,xAxisData,outDir,files)
    %plot changes to F1 after 50k training iterations 
    meanF150kData = reshape(data(i,6,:),[length(files),1])
    minF1Data  = reshape(data(i,5,:),[length(files),1])
    maxF1Data = reshape(data(i,4,:),[length(files),1])
    %swarmSizeData = [500,550,600,650,700,750,800,850,900,950,1000]
    swarmSizeData = swarmSizeRange;
    %swarmSizeData = [50,100,150,200,250,300,350,400,450,500,550,600]
    figure
    plot(swarmSizeData,meanF150kData,swarmSizeData,minF1Data,swarmSizeData,maxF1Data)
    xlabel('Swarm Size') % x-axis label
    ylabel('Mean MSE Score') % y-axis label
    legend('Mean MSE','Min MSE','Max MSE','Location','southeast')
    title(strcat('Training iterations: ',num2str(i*(trainingItersMax/trainingItersNum))))
    saveas(gcf,strcat(outDir,'MseSwarmSize_',num2str(xAxisData(i))),'epsc')
    savefig(gcf,strcat(outDir,'MseSwarmSize_',num2str(xAxisData(i))))
    close
end