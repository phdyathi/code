function volGrid3D( vol )

occ = reshape(vol(2,:,:,:),[31,31,31]);
occ(occ==0)=2;
occ(occ==1)=0;
occ(occ==2)=0.5;

figure;
vol3d('CData',reshape(vol(1,:,:,:),[31,31,31]));
freezeColors;
vol3d('CData',occ);
colormap cool;

end