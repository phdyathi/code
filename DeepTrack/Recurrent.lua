--[[
DeepTracking: Seeing Beyond Seeing Using Recurrent Neural Networks.
Copyright (C) 2016  Peter Ondruska, Mobile Robotics Group, University of Oxford
email:   ondruska@robots.ox.ac.uk.
webpage: http://mrg.robots.ox.ac.uk/

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
--]]

-- N steps of RNN
-- {x1, x2, ..., xN, h0} -> {y1, y2, ..., yN}
--[[
-- A classical implementation of the recurrent module; requires O(N * M(step)) memory.
function Recurrent(step, N)
   local h = nn.Identity()()
   local hx, y = { [params.N+1] = h }, {}
   for i=1,N do
      hx[i] = nn.Identity()()
      local hy = step:clone('weight', 'bias', 'gradWeight', 'gradBias')({h, hx[i]})
      h    = nn.SelectTable(1)(hy)
      y[i] = nn.SelectTable(2)(hy)
   end
   return nn.gModule(hx, y)
end
--]]

-- A memory-efficient (but slower) version of the recurrent module; requires O(N * M(h0)) memory.
local Recurrent, parent = torch.class('Recurrent', 'nn.Module')

function Recurrent:__init(step, N,outputSize,hiddenSize,inputSize)
   parent.__init(self)
   self.N = N
   self.step = step --the neural structure for a "step" (1 timestep)
   self.hidden    = {}
   self.output    = {}
   self.gradInput = {}
   for i=1,N do
      self.hidden[i] = torch.CudaTensor(hiddenSize)
      self.output[i] = torch.CudaTensor(outputSize)
      self.gradInput[i] = torch.CudaTensor(inputSize)
   end
   self.hidden[0] = torch.CudaTensor(hiddenSize)
   self.gradInput[N+1] = torch.CudaTensor(hiddenSize)
end

-- resize A to size of B and sets it to 0
function zero(A,B)
   A:zero()
end

-- copy B to A
function copy(A,B)
   A:copy(B)
end

function getNumThatFitsInMem(numelHidden,numelInput) 

   if not numInputsThatFit then
           local inputSizeBytes = (numelHidden + numelInput) * 4
	   --get free/total mem in bytes
	   local freeMem, totalMem = cutorch.getMemoryUsage(params.gpu)
	   --leave some memory free
	   freeMem = freeMem - 10e8
	   numInputsThatFit = math.floor(freeMem/inputSizeBytes)
	   --local numInputsThatFit = 500
	   print(freeMem)
	   print(inputSizeBytes)
	   print(numInputsThatFit)
   end

   return numInputsThatFit
end

--this recieves a sequence and for each volume in sequence runs forward 
function Recurrent:forward(input)
   --print(torch.getdefaulttensortype())
   --print(input[self.N+1])
   print(self.hidden[0]:type(),input[self.N+1]:type())
   copy(self.hidden[0], input[self.N+1])
   

   --CudaTensor and FloatTensor store 4 byte floating point numbers
   -- calc size for each inputs hidden@t-1 + input@t
   local hSize = self.hidden[0]:size()
   local iSize = input[1]:size()


   local cudaBufferEnd = 0
   local cudaBuffIdx = 1
   for i = 1,self.N do
      --self.hiddenCuda:resize(self.hidden[i-1]:size()):copy(self.hidden[i-1])
      --self.inputCuda:resize(input[i]:size()):copy(input[i])
      --print(cudaBuffIdx)
      --print(#self.hiddenCuda)
      --print(#self.inputCuda)
      --print(#self.hiddenCuda[cudaBuffIdx])
      --print(#self.inputCuda[cudaBuffIdx])
      local stepInput  = {self.hidden[i-1], input[i]} --the input is last step's hidden out + input
      --print(#stepInput)
      --print(i)
      --print(#stepInput[1])
      --print(#stepInput[2])
      --print(stepInput)
      local stepOutput = self.step:forward(stepInput)
      copy(self.hidden[i], stepOutput[1]) -- hidden is stored
      copy(self.output[i], stepOutput[2])
      
   end

   return self.output
end

function Recurrent:backward(input, gradOutput)
   zero(self.gradInput[self.N+1], input[self.N+1])
   --CudaTensor and FloatTensor store 4 byte floating point numbers
   -- calc size for each inputs hidden@t-1 + input@t
   local hSize = self.hidden[0]:size()
   local iSize = input[1]:size()
   
   

   local cudaBufferEnd = self.N+1
   local cudaBuffIdx = numInputsThatFit

   --this iterates backwards
   for i = self.N,1,-1 do
      

      --self.hiddenCuda:resize(self.hidden[i-1]:size()):copy(self.hidden[i-1])
      --self.inputCuda:resize(input[i]:size()):copy(input[i])

      --print(cudaBuffIdx)
      local stepInput  = {self.hidden[i-1], input[i]}
      local stepOutput = self.step:forward(stepInput)
      --TODO: should this be batched too?
      local stepGradOutput = {self.gradInput[self.N+1], gradOutput[i]}
      local stepGradInput  = self.step:backward(stepInput, stepGradOutput)
      copy(self.gradInput[self.N+1], stepGradInput[1])
      --print("saveH",(cudaBufferEnd-numInputsThatFit)+cudaBuffIdx)
      --print(#self.hiddenCuda)
      --self.hiddenCuda[(cudaBufferEnd-numInputsThatFit)+cudaBuffIdx]:copy(self.hidden[i])
      copy(self.gradInput[i], stepGradInput[2])

   end
   return self.gradInput 
end
