--[[
DeepTracking: Seeing Beyond Seeing Using Recurrent Neural Networks.
Copyright (C) 2016  Peter Ondruska, Mobile Robotics Group, University of Oxford
email:   ondruska@robots.ox.ac.uk.
webpage: http://mrg.robots.ox.ac.uk/

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
--]]

require('torch')
cmd = torch.CmdLine()

require('lfs')
require('nngraph')
require('optim')
require('image')
require('WeightedBCECriterion')
require('Recurrent')
--local matio = require 'matio'
require('evaluationUtils')

cmd:option('-gpu', 1, 'use GPU')
cmd:option('-iter', 100000, 'the number of training iterations')
cmd:option('-evalEvery', 100, ' evalute the model every x training iterations')
cmd:option('-N', 100, 'training sequence length')
cmd:option('-batchSize',10,'Number of sequences in a train batch')
cmd:option('-model', 'model', 'neural network model')
cmd:option('-SDP','YathiSensorDataProvider','What sensor data provider to use')
cmd:option('-learningRate', 0.01, 'learning rate')
cmd:option('-initweights', '', 'initial weights')
cmd:option('-initstate', '', 'initial optimizer state')
cmd:option('-trainingData','trainingData/','the directory where the training data lives')
cmd:option('-evalData','evalData/','the directory where the evaluation data lives')


params = cmd:parse(arg)

cmd:log('log_' .. params.model .. '.txt', params)

-- We will write the loss to a text file and read from there to plot the loss as training proceeds
logger = optim.Logger('loss_log.txt')

-- switch to GPU
if params.gpu > 0 then
	print('Using GPU ' .. params.gpu)
	require('cunn')
	require('cutorch')
	require('cudnn')
	cutorch.setDevice(params.gpu)
	--DEFAULT_TENSOR_TYPE = 'torch.CudaTensor'
	DEFAULT_TENSOR_TYPE = 'torch.FloatTensor'
else
	print('Using CPU')
	DEFAULT_TENSOR_TYPE = 'torch.FloatTensor'
end

torch.setdefaulttensortype(DEFAULT_TENSOR_TYPE)

--load sensor data provider
require(params.SDP)
width,height,depth,M = InitSDP(params.trainingData)
print('Occupancy grid has size ' .. width .. 'x' .. height .. 'x' .. depth)
print('Number of sequences ' .. M)
SequenceSizeBytes = (width*height*depth*params.N*2*4) --total size of a single training sequence
-- load neural network model
require(params.model)
-- initial hidden state
h0 = getInitialState(width, height,depth)
-- one step of RNN
step = getStepModule(width, height,depth)

-- network weights + gradients
w, dw = step:getParameters()
print('Model has ' .. w:numel() .. ' parameters')

if #params.initweights > 0 then
	print('Loading weights ' .. params.initweights)
	w:copy(torch.load(params.initweights))
end

-- chain N steps into a recurrent neural network
-- N = sequence length; so 1 step per time step? Is this the memory capacity?
model = Recurrent(step, params.N,torch.LongStorage({width,height,depth}),h0:size(),torch.LongStorage({width,height,depth,2}))

-- cost function
-- {y1, y2, ..., yN},{t1, t2, ..., tN} -> cost
criterion = nn.ParallelCriterion()
for i=1,params.N do
	criterion:add(WeightedBCECriterion(), 1/params.N)
end
criterion:cuda()


batch = nil
batchSize=nil
function getBatch()

  if not batchSize then
    local freeMem, totalMem = cutorch.getMemoryUsage(params.gpu)
    --leave some memory free
    freeMem = freeMem - 10e8
    batchSize =  math.floor(freeMem/SequenceSizeBytes)
  end

  if not batch then
    batch = {}
  end

  print("Loading new batch of size "..batchSize)
  for i = 1,batchSize do
    if not batch[i] then
      batch[i] = getSequence(torch.IntTensor().random(M))
    else
      local temp = getSequence(torch.IntTensor().random(M))
      for j = 1,params.N do
        batch[i][j]:copy(temp[j])
      end
    end
  end
end

-- filter and save model performance on a sample sequence
function evalModel(weights)
	input = getSequence(torch.IntTensor().random(M))
	table.insert(input, h0)
	w:copy(weights)
  local stopWatch = torch.Timer()
	local output = model:forward(input)
        print("Model forward time "..stopWatch:time().real.." sec")
	-- temporarily switch to FloatTensor as image does not work otherwise.
	torch.setdefaulttensortype('torch.FloatTensor')

  local maskedOutput = {}
  local groundTruth = {}

	for i = 1,#input-1 do
    maskedOutput[i] = extractVisiblePredictions(output[i]:float(),input[i][2]:float())
    groundTruth[i] = input[i][1]:float()
		--image.save('video_' .. params.model .. '/input' .. i .. '.png',  input[i][2] / 2 + input[i][1])
		--image.save('video_' .. params.model .. '/output' .. i .. '.png', input[i][2] / 2 + output[i])
    torch.save('video_' .. params.model .. '/input' .. i .. '.t7',  (input[i][2] / 2 + input[i][1]):float())
    torch.save('video_' .. params.model .. '/output' .. i .. '.t7', (input[i][2] / 2 + output[i]):float())
    torch.save('video_' .. params.model .. '/output_raw' .. i .. '.t7', output[i]:float())
    torch.save('video_' .. params.model .. '/input_raw' .. i .. '.t7', input[i][1]:float())
  end

  local f1Score = f1Score(maskedOutput,groundTruth)
	torch.setdefaulttensortype(DEFAULT_TENSOR_TYPE)
  return f1Score
end

-- blanks part of the sequence for predictive training
function dropoutInput(target)
	local input = {}
	for i=1,#target do
		input[i] = target[i]:clone()
		if (i-1) % 20 >= 10 then
        --print("Blank out "..i)
		    input[i]:zero()
		end
	end
	return input
end
batchCount = nil
function retrieveSequence()
  --if !batchCount fails to short circuit this will crash
  print("not batchCount "..tostring(not batchCount))
  print("(batchSize and (batchCount > batchSize)) "..tostring(batchSize and (batchCount > batchSize)))
  if not batchCount or (batchSize and (batchCount > batchSize)) then
    batchCount = 1

    getBatch()
  end

  print(batchCount)
  print(#batch)

  local temp = batch[batchCount]
  batchCount = batchCount + 1
  return temp
end

-- evaluates model on a random input
function trainModel(weights)
	-- input and target
	local target = retrieveSequence() --getSequence(torch.IntTensor().random(M))
	local input  = dropoutInput(target) --blank out parts of the input
	table.insert(input, h0)
	-- forward pass
	w:copy(weights)
	print("input size: ", #input)
	local output = model:forward(input)
	local cost   = criterion:forward(output, target)
	-- backward pass
	dw:zero()

	model:backward(input, criterion:backward(output, target) )
	-- return cost and weight gradients
	return {cost}, dw
end

function runEvaluationSet(evalDir,evalNum,weights)
  local _,_,_,numData = InitSDP(params.evalData)

  local totalF1 = 0
  for i=1,evalNum do
    totalF1 = totalF1 + evalModel(w)
  end

  local avgF1 = totalF1/evalNum

  --reset
  InitSDP(params.trainingData)

  return avgF1
end

-- create directory to save weights, optimizer state and videos
lfs.mkdir('weights_' .. params.model)
lfs.mkdir('state_'   .. params.model)
lfs.mkdir('video_'   .. params.model)

local total_cost, config, state = 0, { learningRate = params.learningRate }, {}
collectgarbage()

if #params.initstate > 0 then
	print('Loading optimizer state ' .. params.initstate)
	state=torch.load(params.initstate)
end

timer = torch.Timer()
for k = 1,params.iter do
	xlua.progress(k, params.iter)
	timer:reset()
  local _, cost = optim.adagrad(trainModel, w, config, state)
  total_cost = total_cost + cost[1][1]
  print("Iter time "..timer:time().real.." sec")
	-- save the training progress
	if k % params.evalEvery == 0 then
		print('Iteration ' .. k .. ', cost: ' .. total_cost / params.evalEvery)
		-- report average error on epoch
    local current_loss = total_cost / params.evalEvery
    print('current loss = ' .. current_loss)

    --GNU plot wont work on MASSIVE so just output txt
    --logger:style{['training error'] = '-'}
    --logger:plot()
    -- visualise performance
		local f1Score = evalModel(w)-- report average error on epoch
    print('current f1 = '.. f1Score)
    --logging
    logger:add{['training error'] = current_loss,['F1 Score']=f1Score}

    total_cost = 0
		-- save weights and optimizer state
		torch.save('weights_' .. params.model .. '/' .. k .. '.dat', w:type('torch.FloatTensor'))
		torch.save('state_' .. params.model .. '/' .. k .. '.dat', state)


	end

	-- not to run out of memory
	collectgarbage()
end
