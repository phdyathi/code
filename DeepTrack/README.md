3DDeepSwarmTracking
==================================================================

This implementation extends the 2D tracker described in the paper below to 3D tracking for swarms of objects:
[P. Ondruska and I. Posner, *"Deep Tracking: Seeing Beyond Seeing Using Recurrent Neural Networks"*, in The Thirtieth AAAI Conference on Artificial Intelligence (AAAI), Phoenix, Arizona USA, 2016.](http://www.robots.ox.ac.uk/~mobile/Papers/2016AAAI_ondruska.pdf)
* **webpage**: https://github.com/pondruska/DeepTracking

Installation
------------
Install [Torch 7](http://torch.ch/) and the following dependencies (using `luarocks install [package]`):
* nngraph
* image
* cunn
* cutorch

Data
----
Download and unzip the training data for the simulated moving balls scenario:
```
https://drive.google.com/file/d/0B5861FVvRr8WcHFDbnA1bzNhZUk/view?usp=sharing
```
This is a native Torch 7 file format.

Training
--------
To train the model run:
```
th train.lua -iter 50000 -evalEvery 500
```

License
-------
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
