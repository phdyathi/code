--[[
DeepTracking: Seeing Beyond Seeing Using Recurrent Neural Networks.
Copyright (C) 2016  Peter Ondruska, Mobile Robotics Group, University of Oxford
email:   ondruska@robots.ox.ac.uk.
webpage: http://mrg.robots.ox.ac.uk/

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
--]]

cmd:option('-grid_minX', -15, 'occupancy grid bounds [m]')
cmd:option('-grid_maxX',  15, 'occupancy grid bounds [m]')
cmd:option('-grid_minZ',  -15, 'occupancy grid bounds [m]')
cmd:option('-grid_maxZ',  15, 'occupancy grid bounds [m]')
cmd:option('-grid_minY', -25, 'occupancy grid bounds [m]')
cmd:option('-grid_maxY',   5, 'occupancy grid bounds [m]')

cmd:option('-grid_step', 1, 'resolution of the occupancy grid [m]')
cmd:option('-sensor_start', -180, 'first depth measurement [degrees]')
cmd:option('-sensor_step', 0.5, 'resolution of depth measurements [degrees]')
sensorParams = cmd:parse(arg)
SensorData = {}

--called by #
-- {total number of training frames, ?dimensions?, grid height, grid width}
function SensorData.__len(self)
	return { self.data:size(1), 2, self.height, self.width, self.depth }
end

-- called by []
function SensorData.__index(self, i)
	local dist = self.data[i]:index(1, self.index):reshape(self.height, self.width, self.depth)
	local input = torch.FloatTensor(2, self.height, self.width, self.depth)
	input[1] = torch.lt(torch.abs(dist - self.dist), sensorParams.grid_step * 0.7071) --the input channel 1 has just sensor hits
	input[2] = torch.gt(dist + sensorParams.grid_step * 0.7071, self.dist) --the input channel 2 has the occusion map (vissibility to the robot)
	return input
end

function LoadSensorData(file, params)
	local self = {}
	-- load raw 1D depth sensor data
	self.data = torch.load(file)
	self.width  = (sensorParams.grid_maxX - sensorParams.grid_minX) / sensorParams.grid_step + 1
	self.height = (sensorParams.grid_maxY - sensorParams.grid_minY) / sensorParams.grid_step + 1
	self.depth = (sensorParams.grid_maxZ - sensorParams.grid_minZ) / sensorParams.grid_step + 1
	-- pre-compute lookup arrays
  	-- basically sort the 
	self.dist  = torch.FloatTensor(self.height, self.width, self.depth)
	self.index = torch.LongTensor(self.height, self.width, self.depth)
	for y = 1,self.height do
		for x = 1,self.width do
			for z = 1,self.depth do
				local px = (x - 1) * sensorParams.grid_step + sensorParams.grid_minX
				local py = (y - 1) * sensorParams.grid_step + sensorParams.grid_minY
				local pz = (z - 1) * sensorParams.grid_step + sensorParams.grid_minZ
				local angle = math.deg(math.atan2(px, py, pz))
				self.dist[x][y][z]  = math.sqrt(px * px + py * py + pz * pz)
				self.index[x][y][z] = math.floor((angle - sensorParams.sensor_start) / sensorParams.sensor_step + 1.5)
			end
		end
	end
	self.index = self.index:reshape(self.width * self.height * self.depth)
	setmetatable(self, SensorData)
	return self
end
