require('torch')
cmd = torch.CmdLine()
require('lfs')
require('io')
require('os')

function SobelFilter3D(inputTensor)
  SobelFilter = torch.Tensor({{{1,2,1},{2,4,2},{1,2,1}},{{0,0,0},{0,0,0},{0,0,0}},{{-1,-2,-1},{-2,-4,-2},{-1,-2,-1}}})

  local c = torch.conv3(inputTensor,SobelFilter,'F')
  c = c:sub(2,c:size(1)-1,2,c:size(2)-1,2,c:size(2)-1)
  c = torch.clamp(torch.abs(c),0,1)
  return c
end

function mysplit(inputstr, sep)
        if sep == nil then
                sep = "%s"
        end
        local t={} ; i=1
        for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
                t[i] = str
                i = i + 1
        end
        return t
end

function alphanumsort(o)
  local function padnum(d) local dec, n = string.match(d, "(%.?)0*(.+)")
    return #dec > 0 and ("%.12f"):format(d) or ("%s%03d%s"):format(dec, #n, n) end
  table.sort(o, function(a,b)
    return tostring(a):gsub("%.?%d+",padnum)..("%3d"):format(#b)
         < tostring(b):gsub("%.?%d+",padnum)..("%3d"):format(#a) end)
  return o
end

function getFilesInDir(dir,ext)
  files = {}

  -- Go over all files in directory. We use an iterator, paths.files().
  for file in paths.files(dir) do
     -- We only load files that match the extension
     if file:find(ext .. '$') then
        -- and insert the ones we care about in our table
        table.insert(files, paths.concat(dir,file))
     end
  end

  -- Check files
  if #files == 0 then
     error('given directory doesnt contain any files of type: ' .. ext)
  end

  -- 3. Sort file names

  -- We sort files alphabetically, it's quite simple with table.sort()

  --table.sort(files, function (a,b) return a < b end)

  files = alphanumsort(files)

  print('Found files:')
  print(#files)

  return files
end

function loadTorchFileRange(fileList)
  t = {}

  for i=1,#fileList do
    t[i] = torch.load(fileList[i])
  end

  return t
end

function convertTorchToSLGNTrainingFile(torchFile,outputFileName,label)
  local tGroup = {}
  for i = 1, #torchFile do
    tGroup[i]  = torch.load(torchFile[i])
    tGroup[i]  = tGroup[i][1]
    tGroup[i] = SobelFilter3D(tGroup[i])
  end

  local dims = #tGroup[1]

  local f = io.open(outputFileName,'a')

  f:write('S\n')
  f:write(label..'\n')
  for n=1,#tGroup do
    for x=1,dims[1] do
      for y=1,dims[2] do
        for z=1,dims[3] do
          if(tGroup[n][x][y][z] == 1) then
            f:write('X')
          else
            f:write(',')
          end
        end
        f:write('\n')
      end
    end
  end

  f:close()

end

function convertTorchToSLGNTestFile(torchFile,outputFileName)
  local tGroup = {}
  for i = 1, #torchFile do
    tGroup[i]  = torch.load(torchFile[i])
    tGroup[i]  = tGroup[i][1]
    tGroup[i] = SobelFilter3D(tGroup[i])
  end

  local dims = #tGroup[1]

  local f = io.open(outputFileName,'a')

  f:write('R\n')

  for n=1,#tGroup do
    for x=1,dims[1] do
      for y=1,dims[2] do
        for z=1,dims[3] do
          if(tGroup[n][x][y][z] == 1) then
            f:write('X')
          else
            f:write(',')
          end
        end
        f:write('\n')
      end
    end
  end

  f:close()

end

function subrange(t, first, last)
  local sub = {}
  for i=first,last do
    sub[#sub + 1] = t[i]
  end
  return sub
end

function downSample(filesList,baseFPS,targetFPS,copyDir)
  local  dropFramesEvery  = 10
  local newFileList = {}

  local count = 1
  for i = dropFramesEvery,math.floor(#filesList/dropFramesEvery)*dropFramesEvery,dropFramesEvery do
    newFileList[count] = filesList[i]
    count = count + 1
  end

  --copy sampled files to seperate folder
  lfs.mkdir(copyDir)
  for i=1,#newFileList do
    local filename = mysplit(newFileList[i],"/")
    filename = filename[#filename]
    local t = torch.load(newFileList[i])
    torch.save(copyDir..filename,t)
  end

  print(#filesList..' Down sampled to: '..#newFileList)
  return newFileList
end

function groupFilesIntoSamples(filesList,sampleSize)

  local grouped = {}
  local groupCount = 1
  local numGroups = #filesList/sampleSize
  print(numGroups.." Groups with"..(#filesList%sampleSize.." frames left over"))

  for i = 1,numGroups do
    local startIndex = ((i-1)*sampleSize)+1
    local endIndex = startIndex + (sampleSize-1)
    grouped[i] = subrange(filesList,startIndex,endIndex)
  end

  return grouped
end

-- Params
cmd:option('-data', './data/', 'Where the data directories are')
cmd:option('-overrideDataDir', '', 'Where the grid files are, this overrides -data option')
cmd:option('-overrideDataOutDir', '', 'Where the SLGN files go, this overrides -data option')
cmd:option('-dataType','training','Type of SLGN input either: test or training defaults to training')
cmd:option('-outDirName','SLGN','Name of new folder to store SLGN files')
cmd:option('-N',1,'The number of frames to make into a sample')
cmd:option('-gridFrameRate',23,'FPS the original simulation rendered the grids in')
cmd:option('-targetFrameRate',2,'FPS for SLGN')
cmd:option('-usedGridCopyDir','./lowFPSGrids/','If temporal downsampled the grids used in the sample are copied to this directory')


params = cmd:parse(arg)
print(params)

torchDataPath = '/rnn/torch/'

if(params.overrideDataDir == '') then

  dataFolders = getFilesInDir(params.data,'')
  --the first 2 entries are .. and . dirs removing them
  table.remove(dataFolders,1)
  table.remove(dataFolders,1)

  print(dataFolders)

  --for each data dir
  for i=1,#dataFolders do
    dataDir = dataFolders[i]..'/'..params.outDirName..'/'
    print('Creating SLGN data dir in:\n'..dataDir)
    lfs.mkdir(dataDir)
    filesToConvert = getFilesInDir(dataFolders[i]..torchDataPath,'grid')
    filesToConvert = downSample(filesToConvert,params.gridFrameRate,params.targetFrameRate,dataDir..'sampled/')
    print(filesToConvert)
    fileGrouped = groupFilesIntoSamples(filesToConvert,params.N)
    --outFileName = mysplit(filesToConvert[j],'/')
    --outFileName = outFileName[#outFileName]
    --outFileName = dataDir..outFileName
    for j=1,#fileGrouped do

      if(params.dataType == 'training') then
        outputFileName = dataDir..'storage.txt'
        convertTorchToSLGNTrainingFile(fileGrouped[j],outputFileName,j+1)
      elseif(params.dataType == 'test') then
        outputFileName = dataDir..'test.txt'
        convertTorchToSLGNTestFile(fileGrouped[j],outputFileName)
      else
        print("ERROR DATATYPE INCORRECT: "..params.dataType)
        os.exit()
      end
    end
  end

else
  if(params.overrideDataOutDir == '') then
    print('ERROR no overrideDataOutDir given!')
    os.exit()
  end

  lfs.mkdir(params.overrideDataOutDir)

  filesToConvert = getFilesInDir(params.overrideDataDir,'grid')
  filesToConvert = downSample(filesToConvert,params.gridFrameRate,params.targetFrameRate,params.usedGridCopyDir)
  fileGrouped = groupFilesIntoSamples(filesToConvert,params.N)
  for j=1,#fileGrouped do

      if(params.dataType == 'training') then
        outputFileName = params.overrideDataOutDir..'storage.txt'
        convertTorchToSLGNTrainingFile(fileGrouped[j],outputFileName,j+1)
      elseif(params.dataType == 'test') then
        outputFileName = params.overrideDataOutDir..'test.txt'
        convertTorchToSLGNTestFile(fileGrouped[j],outputFileName)
      else
        print("ERROR DATATYPE INCORRECT: "..params.dataType)
        os.exit()
      end
  end
end
