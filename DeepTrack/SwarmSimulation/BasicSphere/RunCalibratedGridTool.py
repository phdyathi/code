import argparse, os
import time
import sys
import subprocess

if(len(sys.argv) == 1):
    sourceDir ="./renders_exr/"
    destDir = "./grids/"
    gridSize = 31


if(len(sys.argv) == 4):
    sourceDir = sys.argv[1]
    destDir = sys.argv[2]
    gridSize = sys.argv[3]


calibTool = "./calibratedGridTool/build/calGridTool"
batchSize = 2

gridtxtFiles = []

for f in os.listdir(sourceDir):
    if(f.endswith('.exr')):
        gridtxtFiles.append(f)

if(len(gridtxtFiles)==0):
    exit()

fullBatches = len(gridtxtFiles)/batchSize
leftOvers = len(gridtxtFiles)%batchSize

for gtf in range(0,len(gridtxtFiles),batchSize):
    procList = []
    for i in range(gtf-batchSize,gtf):
        print("Running "+gridtxtFiles[i]+" \n")
        procList.append( subprocess.Popen([calibTool,sourceDir+gridtxtFiles[i],destDir,sourceDir+"temp/",str(gridSize)]) )
    while True:
        pCount = 0
        for p in procList:
            if(p.poll() is not None):
                if(p.poll() != 0):
                     sys.exit(p.poll())
                pCount = pCount+1
            else:
                time.sleep(1)
        if(pCount == batchSize):
            print("Done with batch\n")
            break

print("Done!\n")
sys.exit(0)
