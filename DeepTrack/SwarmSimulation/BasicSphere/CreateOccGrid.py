import numpy, argparse, os

parser = argparse.ArgumentParser(description='Convert exr.npy files to 3DOccGrid npyFiles')
parser.add_argument('-s', default='./txt/',help='The source directory')
parser.add_argument('-d', default='./grids/',help='The destination directory')
parser.add_argument('-debug',dest='debugRender', action='store_true',help='Whether to output debug renders')
parser.add_argument('-r', default='./debugRender/',help='The debug render destination directory')
parser.set_defaults(feature=False)

args = parser.parse_args()
sourceDir = args.s
destDir = args.d
debugRender = args.debugRender
debugRenderDest = args.r

import re
numbers = re.compile(r'(\d+)')
def numericalSort(value):
    parts = numbers.split(value)
    parts[1::2] = map(int, parts[1::2])
    return parts

if not os.path.exists(destDir):
    os.makedirs(destDir)

if debugRender:
    import scipy.misc 
if not os.path.exists(debugRenderDest) and debugRender:
    os.makedirs(debugRenderDest)

exrnpyFiles = []
for file in os.listdir(sourceDir):
    if file.endswith(".npy"):
        exrnpyFiles.append(file)

exrnpyFiles = sorted(exrnpyFiles)

#maybe look up notes on converting pixels to m from journal?
MAX_GRID_DEPTH = 15
dummyImg = numpy.load(sourceDir+exrnpyFiles[0])[0]
print(dummyImg.shape)
GRID_SIZE = (31,dummyImg.shape[0],dummyImg.shape[1]) #Z X Y
bins = numpy.linspace(1,MAX_GRID_DEPTH,GRID_SIZE[0])


frame=1
for depthFile in exrnpyFiles:
    occVol   = numpy.zeros(GRID_SIZE)
    depthImg = numpy.load(sourceDir+depthFile)
    depthImg = depthImg[0] #retrieve z data
    depthImg[ depthImg == numpy.max(depthImg)] = 0 #zero out the things that a impossibly faraway (in blender thats 99999997952.0)
    print(depthImg.shape,bins.shape)
    depthImg = numpy.digitize(depthImg,bins)
    #print(depthImg)
    occupied = numpy.nonzero(depthImg)
    print(occVol.shape)
    #print(occupied)
    #exit()
    for i in range(len(occupied[0])):
        depthVal = depthImg[occupied[0][i]][occupied[1][i]]-1 #get the depth value from 
        occVol[(depthVal,(GRID_SIZE[2]-1)-occupied[0][i],occupied[1][i])] = 1 #index into the volume and set occupancy

    numpy.save(destDir + depthFile,occVol)
    if debugRender:
        scipy.misc.imsave(debugRenderDest + str(frame)+'.jpg',depthImg)
    print("Saved",destDir + str(frame))
    frame = frame + 1

print("Done!")
