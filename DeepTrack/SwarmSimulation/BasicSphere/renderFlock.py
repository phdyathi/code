import bpy
import os
import shutil

def renderParticles(count,sample):
    scene = bpy.context.scene
    fp = os.getcwd() +'renders/'#scene.render.filepath # get existing output path
    print('Path',fp)
    scene.render.image_settings.file_format = 'OPEN_EXR' # set output format to .png

    parts = bpy.data.objects["Cube"].particle_systems[0]
    parts.settings.count = count
    parts.seed = int.from_bytes(os.urandom(1),'little')
    #print(parts)
    #print("seeds", parts.seed)
    #exit()

    #objName = "CrazyFlie2-Assembly"
    #obj = bpy.data.objects[objName]

    for frame_nr in range(scene.frame_start, scene.frame_end+1):

        # set current frame to frame 5
        scene.frame_set(frame_nr)

        #bpy.ops.render.render(write_still=True) # render still
        bpy.ops.render.render()

        # get viewer pixels
        #pixels = bpy.data.images['Viewer Node'].pixels
        #print(bpy.data.images['Viewer Node'])
        scene.render.filepath = fp +str(count)+"_"+str(sample)+"_"+ str(frame_nr)
        print(scene.render.filepath)
        bpy.ops.render.render(write_still=True) # render still


    # restore the filepath
    scene.render.filepath = fp


def copytree(src, dst, symlinks=False, ignore=None):
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            shutil.copytree(s, d, symlinks, ignore)
        else:
            shutil.copy2(s, d)


def runGridToolChain():
    os.system('sh runGridToolChain.sh')
    copytree('torch','flockData/trainingData')
    os.system('rm ./torch/*')

#for p in range(50,600+1,50):
#	for s in range(1,1+1,1):
#    		renderParticles(p,s)
#    		runGridToolChain()
renderParticles(50,1)
runGridToolChain()
