npy4th = require 'npy4th'

function alphanumsort(o)
  local function padnum(d) local dec, n = string.match(d, "(%.?)0*(.+)")
    return #dec > 0 and ("%.12f"):format(d) or ("%s%03d%s"):format(dec, #n, n) end
  table.sort(o, function(a,b)
    return tostring(a):gsub("%.?%d+",padnum)..("%3d"):format(#b)
         < tostring(b):gsub("%.?%d+",padnum)..("%3d"):format(#a) end)
  return o
end

function getFilesInDir(dir,ext)
  files = {}

  -- Go over all files in directory. We use an iterator, paths.files().
  for file in paths.files(dir) do
     -- We only load files that match the extension
     if file:find(ext .. '$') then
        -- and insert the ones we care about in our table
        table.insert(files, paths.concat(dir,file))
     end
  end

  -- Check files
  if #files == 0 then
     error('given directory doesnt contain any files of type: ' .. opt.ext)
  end
  
  -- 3. Sort file names

  -- We sort files alphabetically, it's quite simple with table.sort()

  --table.sort(files, function (a,b) return a < b end)
  
  files = alphanumsort(files)

  print('Found files:')
  print(#files)
  
  return files
end

function produceVisibilityGrid(occ)
  local vis = torch.Tensor(#occArray)
  vis:fill(1)
  local width = (#vis)[2]
  local height = (#vis)[3]
  local depth = (#vis)[1]
  for w=1,width do
    for h=1,height do
      local visible = true
      for d=1,depth do
        if not visible then
          vis[{d,w,h}] = 0
        end
        if visible and (occ[{d,w,h}] ~= 0) then
          visible = false
        end
        
      end
    end
  end
  
  return vis
end

files = getFilesInDir('./grids/','.npy')
outputDir = './torch/'

for i=1,#files do 
  occArray = npy4th.loadnpy(files[i])
  visArray = produceVisibilityGrid(occArray)
  array = torch.Tensor(2,(#occArray)[1],(#occArray)[2],(#occArray)[3])
  array[{1}] = occArray
  array[{2}] = visArray
  torch.save(files[i]..'.grid',array)
  print('Saving '..files[i])
end

os.execute('cp ./grids/*.grid ./torch/')
os.execute('rm ./grids/*.grid')

print('Done!')
