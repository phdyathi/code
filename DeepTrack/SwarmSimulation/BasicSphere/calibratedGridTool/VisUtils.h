#ifndef VISUTILS_H
#define VISUTILS_H

#include <pcl/visualization/pcl_visualizer.h>

boost::shared_ptr<pcl::visualization::PCLVisualizer> SimpleVis (pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud)
{
  // --------------------------------------------
  // -----Open 3D viewer and add point cloud-----
  // --------------------------------------------
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
  viewer->setBackgroundColor (0, 0, 0);
  viewer->addPointCloud<pcl::PointXYZRGB> (cloud, "sample cloud");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "sample cloud");
  //ffffffviewer->addCoordinateSystem (1);
  viewer->initCameraParameters ();
  //viewer->setCameraPosition(0.0,0,0,)
  return (viewer);
}

boost::shared_ptr<pcl::visualization::PCLVisualizer> SimpleVis ()
{
  // --------------------------------------------
  // -----Open 3D viewer-----
  // --------------------------------------------
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
  viewer->setBackgroundColor (0, 0, 0);
  //ffffffviewer->addCoordinateSystem (1);
  viewer->initCameraParameters ();
  //viewer->setCameraPosition(0.0,0,0,)
  return (viewer);
}

#endif
