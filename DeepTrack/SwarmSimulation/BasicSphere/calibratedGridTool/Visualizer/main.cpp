#include <iostream>
#include <string>
#define BOOST_THREAD_PROVIDES_FUTURE
#include <boost/thread.hpp>
#include <boost/thread/future.hpp>
#include <boost/timer/timer.hpp>
#include <math.h>

#include <pcl/common/common.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/grid_minimum.h>
#include <pcl/filters/crop_box.h>

#define TINYEXR_IMPLEMENTATION
#include "../tinyexr/tinyexr.h"

#include "../VisUtils.h"

//if you want vis
#define VISUALISE_PCD
#ifdef VISUALISE_PCD
#include <pcl/visualization/cloud_viewer.h>
#endif

#define FOCAL_LENGTH 2218.71201218605
#define BOX_SIZE 20.0f

typedef struct _ExrFile
{
	EXRHeader header;
	EXRVersion version;
	EXRImage image;
	std::string file;
    short zIndex=-1;
} ExrFile;

typedef struct _CalibValues
{
	double focalLength;
	double principalPointX;
	double principalPointY;
}CalibValues;

typedef struct _DownSampleSettings
{
    //global cube's minimum corner
    float minX;
    float minY;
    float minZ;

    //number of grid cells per dimension
    int numIntX;
    int numIntY;
    int numIntZ;

    //the size of the divisions (size of each voxel)
    float xDiv;
    float yDiv;
    float zDiv;

    //minimum points
    int minimumPoints;
}DownSampleSettings;

typedef struct _tempcube
{
    float minX;
    float minY;
    float minZ;
    int count;

    int gridX;
    int gridY;
    int gridZ;
}TempCube;

bool LoadExrFile(const std::string& filename, ExrFile* output);

bool CreatePointCloud(const ExrFile& exr,const CalibValues& calibValues,pcl::PointCloud<pcl::PointXYZ>& outCloud);

void DownSampleCloud(const pcl::PointCloud<pcl::PointXYZ>& inCloud,const DownSampleSettings& settings,
                                        const char* fileName,boost::promise<std::vector<TempCube> >& outList);

void VisualizeDownSampledCloud(boost::shared_ptr<pcl::visualization::PCLVisualizer>& vis,const char* cloudName,const std::vector<TempCube>& cubes,
                               const DownSampleSettings& settings,
                               const int threshHold,double r,double g,double b);

void ColorizeCloud(const pcl::PointCloud<pcl::PointXYZ>& inCloud, pcl::PointCloud<pcl::PointXYZRGB>& outCloud,uint8_t r=255,uint8_t g=255,uint8_t b=255);

void ComputeOcclusionGrid(const std::vector<TempCube>& sensorGrid,const DownSampleSettings& sensorGridSettings,const CalibValues& calibValues,std::vector<TempCube>& occlusionGridOut);

void ComputeOcclusionCloud(const ExrFile& exr, pcl::PointCloud<pcl::PointXYZ>& outCloud,const CalibValues& calibValues);

void WriteCubes(const char* fileName,const std::vector<TempCube>& cubes);

std::vector<TempCube> ReadCubes(const char *fileName,const DownSampleSettings& settings);

void outputGridFile(const char* fileName,const std::vector<TempCube>& cloudVoxels,const std::vector<TempCube>& occlVoxels
                    ,const DownSampleSettings& settings);

void readGridFile(const char* fileName,std::vector<TempCube>& cloudVoxels,std::vector<TempCube>& occlVoxels
        ,DownSampleSettings& settings);

std::string getNewPath(const std::string& oriPath,const std::string& outPath,const std::string& newExt="")
{
    boost::filesystem::path p(oriPath);
    std::ostringstream oss;
    oss << outPath << p.filename().string()<<newExt;
    return oss.str();
}

int main (int argc, char** argv)
{
	if(argc == 1 || argc > 3)
	{
		printf("gridVis [inputGrid] [inputExr (optional)]\n");
		printf("Eg:\ngridVis ../grids/10_1_1.exr.gridtxt ./renders_exr/10_1_1.exr\n");
		return -1;
	}

    printf("Using pcl verison %d\n",PCL_VERSION);
    printf("Grid file: %s\n",argv[1]);
    printf("EXR file: %s\n",argv[2]);

    DownSampleSettings settings;
    std::vector<TempCube> cloudVoxel;
    std::vector<TempCube> cloudOcclusionVoxel;

    //load only the grid
    if(argc >= 2)
    {
        printf("Loading grid: %s\n",argv[1]);
        readGridFile(argv[1],cloudVoxel,cloudOcclusionVoxel,settings);
        printf("Cloud voxels: %d\n",cloudVoxel.size());
        printf("Occlousion voxels: %d\n",cloudOcclusionVoxel.size());
        
    }

    //exr related vars
    pcl::PointCloud<pcl::PointXYZ> cloud;
    pcl::PointCloud<pcl::PointXYZ> cloudOcclusion;
    pcl::PointCloud<pcl::PointXYZ> cloudFilter;
    bool loadedExr= false;
    //read in the exr file
    if(argc == 3)
    {
        ExrFile file;
        if(!LoadExrFile(argv[2],&file))
        {
            printf("File: %s failed to load! Exiting..\n",argv[1]);
            return -1;
        }
        const float imgWidth = (float)file.image.width;
        const float imgHieght = (float)file.image.height;

        CalibValues calibVals;
        calibVals.focalLength = FOCAL_LENGTH;
        calibVals.principalPointX = imgWidth/2.0f;
        calibVals.principalPointY = imgHieght/2.0f;

        CreatePointCloud(file,calibVals,cloud);
        ComputeOcclusionCloud(file,cloudOcclusion,calibVals);
        loadedExr = true;
    }

    //visualisation stuff
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = SimpleVis();

    if(loadedExr) {
        pcl::PointCloud<pcl::PointXYZRGB> cloudColor;
        pcl::PointCloud<pcl::PointXYZRGB> cloudFilteredColor;
        pcl::PointCloud<pcl::PointXYZRGB> cloudOccColor;
        ColorizeCloud(cloud, cloudColor);
        ColorizeCloud(cloudOcclusion, cloudOccColor, 100, 100, 100);

        viewer->addPointCloud(cloudColor.makeShared(),"cloudMain");
        viewer->addPointCloud(cloudOccColor.makeShared(),"cloudOther");
        //viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "cloudMain");
    }

    viewer->addCube(settings.minX,settings.minX+(settings.numIntX*settings.xDiv),
        settings.minY,settings.minY+(settings.numIntY*settings.yDiv),
        settings.minZ,settings.minZ+(settings.numIntZ*settings.zDiv));
    VisualizeDownSampledCloud(viewer, "cloudOcclusion", cloudOcclusionVoxel, settings, 0, 1.0, 0.1, 0.1);

    VisualizeDownSampledCloud(viewer, "cloud", cloudVoxel, settings, 0, 0.1, 1.0, 0.1);

    viewer->addLine(pcl::PointXYZ(0,0,0),pcl::PointXYZ(1,0,0),"x");
    viewer->addLine(pcl::PointXYZ(0,0,0),pcl::PointXYZ(0,1,0),"y");
    viewer->addLine(pcl::PointXYZ(0,0,0),pcl::PointXYZ(0,0,1),"z");
    while (!viewer->wasStopped())
    {
        viewer->spinOnce(100);
        boost::this_thread::sleep (boost::posix_time::microseconds (100000));
    }


    return (0);
}

void WriteCubes(const char* fileName,const std::vector<TempCube>& cubes)
{
    FILE *fp;

    fp = fopen(fileName, "w");
    for(int i=0; i<cubes.size();i++)
    {
        fprintf(fp, "%f %f %f %d\n",cubes[i].minX,cubes[i].minY,cubes[i].minZ,cubes[i].count);
    }

    fclose(fp);
}

std::vector<TempCube> ReadCubes(const char *fileName,const DownSampleSettings& settings)
{
    FILE *fp;

    fp = fopen(fileName, "r");
    std::vector<TempCube> cubes;
    if(fp != 0) {
        printf("Reading precalc cubes\n");
        TempCube c;
        while (fscanf(fp, "%f %f %f %d\n", &c.minX, &c.minY, &c.minZ, &c.count) != EOF) {

            c.gridX = (int)round((c.minX-settings.minX)/settings.xDiv)+1;
            c.gridY = (int)round((c.minY-settings.minY)/settings.yDiv)+1;
            c.gridZ = (int)round((c.minZ-settings.minZ)/settings.zDiv)+1;
            cubes.push_back(c);
        }
        fclose(fp);
    }
    printf("Read %lu cubes\n",cubes.size());
    return cubes;
}

void VisualizeDownSampledCloud(boost::shared_ptr<pcl::visualization::PCLVisualizer>& vis,const char* cloudName,const std::vector<TempCube>& cubes,
                               const DownSampleSettings& settings,
                               const int threshHold,double r,double g,double b)
{
    for(int i=0;i<cubes.size();i++)
    {
        std::ostringstream oss;
        oss << cloudName<<"Cube" << i;
        std::string str = oss.str();
        if(cubes[i].count > threshHold)
        {
            vis->addCube(cubes[i].minX, cubes[i].minX + settings.xDiv, cubes[i].minY, cubes[i].minY + settings.yDiv,
                         cubes[i].minZ, cubes[i].minZ + settings.zDiv, r, g, b, str);
        }

    }
}

void DownSampleCloud(const pcl::PointCloud<pcl::PointXYZ>& inCloud,const DownSampleSettings& settings,
                     const char* fileName,boost::promise<std::vector<TempCube> >& outList)
{
    std::vector<TempCube> cubeList;
    int i=0;
    printf("DownSampling Cloud...\n");
    float curX=settings.minX,curY=settings.minY,curZ=settings.minZ;
    int cId =0;
    //pcl::CropBox<pcl::PointXYZ> cropBox;
    //cropBox.setInputCloud(inCloud.makeShared());
    for(int x = 0;x<settings.numIntX;x++)
    {
        for(int y=0; y < settings.numIntY; y++)
        {

            for(int z=0;z<settings.numIntZ;z++)
            {
                std::vector< int > ind;
                //pcl::PointCloud<pcl::PointXYZ> boxCloud;
                Eigen::Vector4f min(curX,curY,curZ,0.0);
                Eigen::Vector4f max(curX+settings.xDiv,curY+settings.yDiv,curZ+settings.zDiv,0.0);
                //cropBox.setMin(min);
                //cropBox.setMax(max);
                //cropBox.filter(boxCloud);
                pcl::getPointsInBox(inCloud,min,max,ind);

                if(ind.size() > settings.minimumPoints) {
                    //printf("Grid:<%d,%d,%d> %lu\n", x, y, z, boxCloud.size());
                    i++;
                }

                if(ind.size() > settings.minimumPoints) {
                    TempCube tC;
                    tC.minX = curX;
                    tC.minY = curY;
                    tC.minZ = curZ;
                    tC.count = (int)ind.size();
                    tC.gridX = x;
                    tC.gridY = y;
                    tC.gridZ = z;
                    cubeList.push_back(tC);
                }
                curZ += settings.zDiv;
                cId++;
            }
            curY += settings.yDiv;
            curZ = settings.minZ;
        }
        curX += settings.xDiv;
        curY = settings.minY;
    }
    printf("Found %d points\n",i);
    outList.set_value(cubeList);
}

void ColorizeCloud(const pcl::PointCloud<pcl::PointXYZ>& inCloud, pcl::PointCloud<pcl::PointXYZRGB>& outCloud,uint8_t r,uint8_t g,uint8_t b)
{
    for(int i=0; i<inCloud.size();i++)
    {

        pcl::PointXYZRGB point;
        point.x = inCloud.points[i].x;
        point.y = inCloud.points[i].y;
        point.z = inCloud.points[i].z;
        point.r = r;
        point.g = g;
        point.b = b;

        outCloud.push_back(point);

    }
}


static const char* GetPixelType(int id)
{
  if (id == TINYEXR_PIXELTYPE_HALF) {
    return "HALF";
  } else if (id == TINYEXR_PIXELTYPE_FLOAT) {
    return "FLOAT";
  } else if (id == TINYEXR_PIXELTYPE_UINT) {
    return "UINT";
  }

  return "???";
}

bool LoadExrFile(const std::string& filename, ExrFile* output)
{
	output->file=filename;

	//load version
	int ret = ParseEXRVersionFromFile(&output->version, filename.c_str());
	if (ret != 0) {
		fprintf(stderr, "Invalid EXR file: %s, ret=%d\n", filename.c_str(),ret);
		return false;
	}

	printf("version: tiled = %d, long_name = %d, non_image = %d, multipart = %d\n",
		output->version.tiled,
		output->version.long_name,
		output->version.non_image,
		output->version.multipart);

	//load header
  InitEXRHeader(&output->header);
	const char* err;
  ret = ParseEXRHeaderFromFile(&output->header, &output->version, filename.c_str(), &err);
  if (ret != 0) {
    fprintf(stderr, "Parse single-part EXR err: %s ret=%d\n", err,ret);
    return ret;
  }
	printf("numChannels = %d\n",output->header.num_channels);
  printf("dataWindow = %d, %d, %d, %d\n",
    output->header.data_window[0],
    output->header.data_window[1],
    output->header.data_window[2],
    output->header.data_window[3]);
  printf("displayWindow = %d, %d, %d, %d\n",
    output->header.display_window[0],
    output->header.display_window[1],
    output->header.display_window[2],
    output->header.display_window[3]);
  printf("screenWindowCenter = %f, %f\n",
    static_cast<double>(output->header.screen_window_center[0]),
    static_cast<double>(output->header.screen_window_center[1]));
  printf("screenWindowWidth = %f\n",
    static_cast<double>(output->header.screen_window_width));
  printf("pixelAspectRatio = %f\n",
    static_cast<double>(output->header.pixel_aspect_ratio));
  printf("lineOrder = %d\n",
    output->header.line_order);

  if (output->header.num_custom_attributes > 0) {
    printf("# of custom attributes = %d\n", output->header.num_custom_attributes);
    for (int i = 0; i < output->header.num_custom_attributes; i++) {
      printf("  [%d] name = %s, type = %s, size = %d\n", i,
        output->header.custom_attributes[i].name,
        output->header.custom_attributes[i].type,
        output->header.custom_attributes[i].size);
      //if (strcmp(exr_header.custom_attributes[i].type, "float") == 0) {
      //  printf("    value = %f\n", *reinterpret_cast<float *>(exr_header.custom_attributes[i].value));
      //}
    }
  }

  // Read HALF channel as FLOAT.
  for (int i = 0; i < output->header.num_channels; i++) {
    if (output->header.pixel_types[i] == TINYEXR_PIXELTYPE_HALF) {
      output->header.requested_pixel_types[i] = TINYEXR_PIXELTYPE_FLOAT;
    }
  }
	//load image
	InitEXRImage(&output->image);

  ret = LoadEXRImageFromFile(&output->image, &output->header, filename.c_str(), &err);
  if (ret != 0) {
    fprintf(stderr, "Load EXR err: %s\n", err);
    return ret;
  }

  printf("EXR size: %d x %d\n", output->image.width, output->image.height);

  for (int i = 0; i < output->header.num_channels; i++) {
    if(strcmp("Z",output->header.channels[i].name)==0)
        output->zIndex=i;
    printf("pixelType[%d]: %s\n", i, GetPixelType(output->header.pixel_types[i]));
    printf("chan[%d] = %s\n", i, output->header.channels[i].name);
    printf("requestedPixelType[%d]: %s\n", i, GetPixelType(output->header.requested_pixel_types[i]));
  }
  if(output->zIndex == -1)
  {
      printf("No Z channel found!");
      return false;
  }
	return true;
}

static void CalibrateExrValues(const CalibValues& calibValues,float u,float v,float z, float& outX, float& outY)
{
	outX = z * (-u - calibValues.principalPointX)/calibValues.focalLength;
	outY =-1* z * (v - calibValues.principalPointY)/calibValues.focalLength;
}

bool CreatePointCloud(const ExrFile& exr,const CalibValues& calibValues,pcl::PointCloud<pcl::PointXYZ>& outCloud)
{
	for(int i = 0; i < (exr.image.width*exr.image.height) ;i++)
	{
		//TODO: CHECK THIS ON PEN + PAPER
		int  v = (i!=0?(i/exr.image.width):0)+1;
		int u = (i - (exr.image.width*v))+1;
		pcl::PointXYZ point;
        point.z = ((float*)exr.image.images[exr.zIndex])[i]; //TODO: This might not be working correctly.
        //if(std::abs(point.z) >= 1000.0f)
        //    point.z = 30.0f;
		if(std::abs(point.z) < 1000.0f)
		{

			CalibrateExrValues(calibValues, u, v, point.z, point.x, point.y);

			outCloud.push_back(point);
		}
	}
	printf("Cloud contains %d points\n",(int)outCloud.size());
	return true;
}

void ComputeOcclusionGrid(const std::vector<TempCube>& sensorGrid,const DownSampleSettings& sensorGridSettings,const CalibValues& calibValues,std::vector<TempCube>& occlusionGridOut)
{
    for(int i=0; i < sensorGrid.size();i++)
    {
        float currZ = sensorGrid[i].minZ+sensorGridSettings.zDiv;
        while(currZ<(sensorGridSettings.minZ+(sensorGridSettings.numIntZ*sensorGridSettings.zDiv)))
        {
            float u = -((calibValues.focalLength*sensorGrid[i].minX+calibValues.principalPointX*currZ)/currZ);
            float v = calibValues.principalPointY-((calibValues.focalLength*sensorGrid[i].minY)/currZ);
            TempCube tc;
            tc.minZ=currZ;
            CalibrateExrValues(calibValues,u,v,currZ,tc.minX,tc.minY);
            tc.gridX = (int)round((tc.minX-sensorGridSettings.minX)/sensorGridSettings.xDiv)+1;
            tc.gridY = (int)round((tc.minY-sensorGridSettings.minY)/sensorGridSettings.yDiv)+1;
            tc.gridZ = (int)round((tc.minZ-sensorGridSettings.minZ)/sensorGridSettings.zDiv)+1;
            occlusionGridOut.push_back(tc);
            currZ = currZ + sensorGridSettings.zDiv;
        }
    }
}

void ComputeOcclusionCloud(const ExrFile& exr, pcl::PointCloud<pcl::PointXYZ>& outCloud,const CalibValues& calibValues)
{
    pcl::PointCloud<pcl::PointXYZ> tempCloud;
    for(int i = 0; i < (exr.image.width*exr.image.height) ;i++)
    {
        //TODO: CHECK THIS ON PEN + PAPER
        int  v = (i!=0?(i/exr.image.width):0)+1;
        int u = (i - (exr.image.width*v))+1;
        pcl::PointXYZ point;
        point.z = ((float*)exr.image.images[exr.zIndex])[i]; //TODO: This might not be working correctly.
        //if(std::abs(point.z) >= 1000.0f)
        //    point.z = 30.0f;
        if(std::abs(point.z) < 20.0f)
        {
            //printf("Z:%f\n",point.z);
            float currZ = point.z;
            while(currZ<20.0f){
                point.z = currZ;
                //printf("Z:%f\n",point.z);
                CalibrateExrValues(calibValues, u, v, currZ, point.x, point.y);

                tempCloud.push_back(point);
                currZ = currZ+0.1f;
            }
            //printf("Occ Cloud contains %d points\n",(int)outCloud.size());
        }
    }

    pcl::VoxelGrid<pcl::PointXYZ> sor;
    sor.setInputCloud(tempCloud.makeShared());
    sor.setLeafSize (0.1f,0.1f,0.1f);
    sor.setSaveLeafLayout(true);
    sor.filter(outCloud);

    printf("Occ Cloud contains %d points\n",(int)outCloud.size());
}

void readGridFile(const char* fileName,std::vector<TempCube>& cloudVoxels,std::vector<TempCube>& occlVoxels
        ,DownSampleSettings& settings)
{
    FILE *fp;
    fp = fopen(fileName, "r");
    //Down sample settings
    fscanf(fp, "%f %f %f %d %d %d %f %f %f\n", &settings.minX,&settings.minY,&settings.minZ
            ,&settings.numIntX,&settings.numIntY,&settings.numIntZ,
            &settings.xDiv,&settings.yDiv,&settings.zDiv);

            printf( "%f %f %f %d %d %d %f %f %f\n",settings.minX,settings.minY,settings.minZ
            ,settings.numIntX,settings.numIntY,settings.numIntZ,
            settings.xDiv,settings.yDiv,settings.zDiv);

    //number of cloudVoxels
    int numCloudVoxel=0;
    fscanf(fp,"%lu\n",&numCloudVoxel);
    //cloud voxels
    for(int i=0;i < numCloudVoxel;i++)
    {
        TempCube tc;
        cloudVoxels.push_back(tc);
        fscanf(fp,"%d %d %d\n",&cloudVoxels[i].gridX,&cloudVoxels[i].gridY,&cloudVoxels[i].gridZ);
        cloudVoxels[i].minX = settings.minX+((cloudVoxels[i].gridX)*settings.xDiv);
        cloudVoxels[i].minY = settings.minY+((cloudVoxels[i].gridY)*settings.yDiv);
        cloudVoxels[i].minZ = settings.minZ+((cloudVoxels[i].gridZ)*settings.zDiv);
    }

    //number of occlVoxel
    int numOcclVoxels=0;
    fscanf(fp,"%lu\n",&numOcclVoxels);
    //occlVoxel
    for(int i=0;i < numOcclVoxels;i++)
    {
        TempCube tc;
        occlVoxels.push_back(tc);
        fscanf(fp,"%d %d %d\n",&occlVoxels[i].gridX,&occlVoxels[i].gridY,&occlVoxels[i].gridZ);
        occlVoxels[i].minX = settings.minX+((occlVoxels[i].gridX-1)*settings.xDiv);
        occlVoxels[i].minY = settings.minY+((occlVoxels[i].gridY-1)*settings.yDiv);
        occlVoxels[i].minZ = settings.minZ+((occlVoxels[i].gridZ-1)*settings.zDiv);
    }

    fclose(fp);
}

void outputGridFile(const char* fileName,const std::vector<TempCube>& cloudVoxels,const std::vector<TempCube>& occlVoxels
        ,const DownSampleSettings& settings)
{
    FILE *fp;

    fp = fopen(fileName, "w");
    //the first line is the DownSample Setings
    fprintf(fp, "%f %f %f %d %d %d %f %f %f\n",settings.minX,settings.minY,settings.minZ
            ,settings.numIntX,settings.numIntY,settings.numIntZ,
            settings.xDiv,settings.yDiv,settings.zDiv);

    //number of cloudVoxels
    fprintf(fp,"%lu\n",cloudVoxels.size());
    //cloudVoxels
    for(int i=0;i < cloudVoxels.size();i++)
    {
        fprintf(fp,"%d %d %d\n",cloudVoxels[i].gridX,cloudVoxels[i].gridY,cloudVoxels[i].gridZ);
    }

    //number of occlVoxel
    fprintf(fp,"%lu\n",occlVoxels.size());
    //occlVoxel
    for(int i=0;i < occlVoxels.size();i++)
    {
        fprintf(fp,"%d %d %d\n",occlVoxels[i].gridX,occlVoxels[i].gridY,occlVoxels[i].gridZ);
    }

    fclose(fp);
}