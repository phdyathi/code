#convert exr to numpy
python ConvertEXR.py &&
#rm renders_exr/* && 
#run python to create npy grids
python CreateOccGrid.py -debug  &&
rm txt/* &&
#convert npy grid to torch
th convertNpyToTorch.lua &&
rm grids/*
#run torch to view grids
#th -i render3DVol.lua




