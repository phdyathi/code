import OpenEXR, Imath, numpy, argparse, os, scipy.misc

parser = argparse.ArgumentParser(description='Convert EXR into txt files')
parser.add_argument('-s', default='./renders_exr/',help='The source directory')
parser.add_argument('-d', default='./txt/',help='The destination directory')

args = parser.parse_args()

sourceDir = args.s
destDir = args.d

if not os.path.exists(destDir):
    os.makedirs(destDir)

exrFiles = []
for file in os.listdir(sourceDir):
    if file.endswith(".exr"):
        exrFiles.append(file)

print "Found",len(exrFiles),"files"

pt = Imath.PixelType(Imath.PixelType.FLOAT)
for exr in exrFiles:
    exrImg = OpenEXR.InputFile(sourceDir+exr)
    dw = exrImg.header()['dataWindow']
    size = (dw.max.x - dw.min.x + 1, dw.max.y - dw.min.y + 1)
    red = numpy.fromstring(exrImg.channel('R', pt), dtype = numpy.float32)
    red.shape = (size[1], size[0]) # Numpy arrays are (row, col)
    green = numpy.fromstring(exrImg.channel('G', pt), dtype = numpy.float32)
    green.shape = (size[1], size[0]) # Numpy arrays are (row, col)
    blue = numpy.fromstring(exrImg.channel('B', pt), dtype = numpy.float32)
    blue.shape = (size[1], size[0]) # Numpy arrays are (row, col)
    depth = numpy.fromstring(exrImg.channel('Z', pt), dtype = numpy.float32)
    depth.shape = (size[1], size[0]) # Numpy arrays are (row, col)
    exrImg.close()

    #red = scipy.misc.imresize(red,(31,31))
    #green = scipy.misc.imresize(green,(31,31))
    #blue = scipy.misc.imresize(blue,(31,31))
    #depth = scipy.misc.imresize(depth,(31,31))

    outImg = numpy.zeros((4,size[1],size[0]))
    outImg[0] = depth
    outImg[1] = red
    outImg[2] = green
    outImg[3] = blue

    numpy.save(destDir+exr,outImg)
    print "Saved",exr

print "Done!"
