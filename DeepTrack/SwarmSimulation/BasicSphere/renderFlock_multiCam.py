#renders the scene from all cameras
#this code is based on renderFlock.py & render_all_cameras.py
import bpy
import os
import sys
import shutil

def renderParticles(count,sample,renderFly):
    scene = bpy.context.scene
    fpRender = os.getcwd() +'/renders'#scene.render.filepath # get existing output path
    fpRenderExr = os.getcwd() +'/renders_exr/'
    cameraNames = 'MultiCam'
    multiCamType = 'BMP'
    singleCamType= 'OPEN_EXR'
    particleEmitterName = 'Sphere'

    parts = bpy.data.objects[particleEmitterName].particle_systems[0]
    parts.settings.count = count
    parts.seed = int.from_bytes(os.urandom(1),'little')
    #print(parts)
    #print("seeds", parts.seed)
    #exit()

    #objName = "CrazyFlie2-Assembly"
    #obj = bpy.data.objects[objName]

    for frame_nr in range(scene.frame_start, scene.frame_end+1):
        # set current frame to frame 5
        scene.frame_set(frame_nr)
        #loop through the objects, select cameras and render from them
        c=0
        for obj in bpy.data.objects:
            # Find cameras that match cameraNames
            if ( obj.type =='CAMERA') and ( cameraNames == '' or obj.name.find(cameraNames) != -1) :
                if(renderFly):
                    print("Rendering scene with Camera["+obj.name+"]")

                    currCamDir = fpRender+'/'+obj.name+'/'
                    if not os.path.exists(currCamDir):
                      os.makedirs(currCamDir)
                    # Set Scenes camera and output filename
                    scene.camera = obj
                    scene.render.resolution_x = 1024
                    scene.render.resolution_y = 1024
                    scene.render.resolution_percentage = 100
                    scene.render.use_border = False
                    scene.render.image_settings.file_format = multiCamType #'OPEN_EXR' # set output format to .png

                    #bpy.data.scenes[sceneKey].render.file_format = 'JPEG'

                    #bpy.ops.render.render(write_still=True) # render still
                    bpy.ops.render.render()

                    # get viewer pixels
                    #pixels = bpy.data.images['Viewer Node'].pixels
                    #print(bpy.data.images['Viewer Node'])
                    scene.render.filepath = currCamDir +str(count)+"_"+str(sample)+"_"+ str(frame_nr)
                    print(scene.render.filepath)
                    bpy.ops.render.render(write_still=True) # render still

                    c = c + 1
        #render exr

        currCamDir = fpRenderExr
        if not os.path.exists(currCamDir):
            os.makedirs(currCamDir)
        # Set Scenes camera and output filename
        scene.camera = bpy.data.objects["Camera"]
        scene.render.resolution_x = 1024
        scene.render.resolution_y = 1024
        scene.render.resolution_percentage = 100
        scene.render.use_border = False
        scene.render.image_settings.file_format = singleCamType #'OPEN_EXR' # set output format to .png

        bpy.ops.render.render()
        scene.render.filepath = currCamDir +str(count)+"_"+str(sample)+"_"+ str(frame_nr)
        print(scene.render.filepath)
        bpy.ops.render.render(write_still=True) # render still

        print(' Frame '+str(frame_nr)+'/'+str(scene.frame_end)+' Done!')



def copytree(src, dst, symlinks=False, ignore=None):
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            shutil.copytree(s, d, symlinks, ignore)
        else:
            shutil.copy2(s, d)


def runGridToolChain():
    os.system('sh runGridToolChain.sh')
    copytree('torch','flockData/trainingData')
    os.system('rm ./torch/*')

#Too get the required 1000 000 training frames need these values
minNum = 500
maxNum = 1000
increment = 50
randomSets = 2
for p in range(minNum,maxNum+increment,increment):
    for s in range(1,randomSets+1,1):
        renderParticles(p,s,True)
    if(os.system('python RunCalibratedGridTool.py') != 0):
        print('Error running RunCalibratedGridTool.py!\n')
        sys.exit(-1)
    if(os.system('th ConvertGridTxtToGrid.lua') != 0):
        print('Error running ConvertGridTxtToGrid.lua!\n')
        sys.exit(-1)
    if(os.system('sh copyToData.sh '+str(p)) != 0 ):
        print('Error running sh copyToData.sh!\n')
        sys.exit(-1)

#runGridToolChain()
