import os
import sys
import shutil

def copytree(src, dst, symlinks=False, ignore=None):
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            shutil.copytree(s, d, symlinks, ignore)
        else:
            shutil.copy2(s, d)


def runGridToolChain():
    os.system('sh runGridToolChain.sh')
    copytree('torch','flockData/trainingData')
    os.system('rm ./torch/*')


dataFolder = "/mnt/c/PhD/Deeptrack_eval/1_3/"
outFolder = "/mnt/c/PhD/Deeptrack_eval/1_3/"
defaultFolderAppend = "/rnn/renders_exr/"
gridSize = 31

if not os.path.exists(outFolder):
    os.makedirs(outFolder)

#get folders
#foreach folder copy exr to renders_exr and run tool chain
for file in os.listdir(dataFolder):
    exrFolder = dataFolder+file+defaultFolderAppend
    #if(os.system('cp -r '+exrFolder+'* ./renders_exr/') != 0):
    #    print('Error copying from '+exrFolder+' !\n')
    #    sys.exit(-1)
    #os.system('rm -rf ./renders_exr/temp')
    if(os.system('python RunCalibratedGridTool.py '+exrFolder+" "+"./grids/ "+str(gridSize)) != 0):
        print('Error running RunCalibratedGridTool.py!\n')
        sys.exit(-1)
    if(os.system('th ConvertGridTxtToGrid.lua') != 0):
        print('Error running ConvertGridTxtToGrid.lua!\n')
        sys.exit(-1)
    if not os.path.exists(outFolder+file+"/rnn/"):
        os.makedirs(outFolder+file+"/rnn/")
    if(os.system("cp -r ./torch "+outFolder+file+"/rnn/") != 0 ):
        print('Error running sh copyToData.sh!\n')
        sys.exit(-1)
    if(os.system("rm -fr ./grids/* ./renders_exr/* ./torch/*") != 0 ):
        print('Error deleting temp files!\n')
        sys.exit(-1)
print('Script Done!')
exit()
#runGridToolChain()
