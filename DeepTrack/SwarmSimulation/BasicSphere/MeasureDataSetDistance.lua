require('torch')
cmd = torch.CmdLine()
require('lfs')
require('io')
require('os')

function _calcMSE(outputs,groundTruths)
  return torch.sum(torch.pow((outputs-groundTruths),2))/outputs:nElement()
end

function alphanumsort(o)
  local function padnum(d) local dec, n = string.match(d, "(%.?)0*(.+)")
    return #dec > 0 and ("%.12f"):format(d) or ("%s%03d%s"):format(dec, #n, n) end
  table.sort(o, function(a,b)
    return tostring(a):gsub("%.?%d+",padnum)..("%3d"):format(#b)
         < tostring(b):gsub("%.?%d+",padnum)..("%3d"):format(#a) end)
  return o
end

function getFilesInDir(dir,ext)
  files = {}

  -- Go over all files in directory. We use an iterator, paths.files().
  for file in paths.files(dir) do
     -- We only load files that match the extension
     if file:find(ext .. '$') then
        -- and insert the ones we care about in our table
        table.insert(files, paths.concat(dir,file))
     end
  end

  -- Check files
  if #files == 0 then
     error('given directory doesnt contain any files of type: ' .. ext)
  end

  -- 3. Sort file names

  -- We sort files alphabetically, it's quite simple with table.sort()

  --table.sort(files, function (a,b) return a < b end)

  files = alphanumsort(files)

  print('Found files:')
  print(#files)

  return files
end

-- Params
cmd:option('-data', './data/', 'Where the data directories are')
cmd:option('-outputFile', './dataDistance.txt', 'Output file')

params = cmd:parse(arg)
print(params)

data = getFilesInDir(params.data,'grid')
diff = {}
totalDiff = 0
baseData  = torch.load(data[1])

print('Distance to self '.._calcMSE(baseData,baseData))

f = io.open(params.outputFile,'a')
for i = 2, #data do
  local other = torch.load(data[i])
  diff[i] = _calcMSE(baseData,other)*1000
  f:write(diff[i]..'\n')
  totalDiff = totalDiff + diff[i]
end

for i = 2, #diff do
  print(data[i].." : "..diff[i])
end

print("totalDiff: "..totalDiff)
avgDiff = totalDiff/(#data-1)
print("avgDiff: "..avgDiff)

f:close()


exBasicPattern = {}
exBasicPattern[1] = torch.Tensor({0,0,0,1,1})
exBasicPattern[2] = torch.Tensor({0,0,0,1,0})
exBasicPattern[3] = torch.Tensor({0,0,0,0,1})
exBasicPattern[4] = torch.Tensor({0,1,1,1,1})

res = {}
for i = 1,#exBasicPattern do
  res[i] = _calcMSE(exBasicPattern[1],exBasicPattern[i])
end

print(res)