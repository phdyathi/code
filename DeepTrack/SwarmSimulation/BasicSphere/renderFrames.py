import bpy

scene = bpy.context.scene
fp = scene.render.filepath # get existing output path
scene.render.image_settings.file_format = 'PNG' # set output format to .png

#objName = "CrazyFlie2-Assembly"
#obj = bpy.data.objects[objName]

for frame_nr in range(scene.frame_start, scene.frame_end):

    # set current frame to frame 5
    scene.frame_set(frame_nr)

    # set output path so render won't get overwritten
    scene.render.filepath = fp + str(frame_nr)
    #bpy.ops.render.render(write_still=True) # render still
    bpy.ops.render.render()

    # get viewer pixels
    #pixels = bpy.data.images['Viewer Node'].pixels
    #print(bpy.data.images['Viewer Node'])
    print(bpy.data.images.keys())
    pixels = bpy.data.images['Render Result'].pixels
    print(bpy.data.images['Render Result'].pixels)
    print(len(pixels)) # size is always width * height * 4 (rgba)
    
    zFile = open('./renders/{}.txt'.format(frame_nr),'w')
    #zFile.write('{} {} {}\n'.format(obj.location[0],obj.location[1],obj.location[2]))
 #   locFile.write('{} {} {}\n'.format(obj.location[0],obj.location[1],obj.location[2]))

# restore the filepath
scene.render.filepath = fp
