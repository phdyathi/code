#!/bin/bash

echo 'Moving data to ./data/'$1'/'
echo 'Creating subfolders...'
mkdir ./data/$1/ &&
mkdir ./data/$1/flyTracker/ &&
mkdir ./data/$1/rnn/ &&

echo 'Compressing bitmap renders...' &&
tar -zcvf renders.tar.gz ./renders/ &&
echo 'DOne compressing' &&
cp -rv ./renders.tar.gz ./data/$1/flyTracker/ &&
cp -rv ./renders_exr/ ./data/$1/rnn/ &&
cp -rv ./torch/ ./data/$1/rnn/ &&

rm -rf ./renders/* &&
rm -rf ./renders_exr/* &&
rm ./grids/* &&
rm ./torch/* &&
rm renders.tar.gz
