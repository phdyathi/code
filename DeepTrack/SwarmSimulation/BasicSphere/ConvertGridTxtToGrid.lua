cmd = torch.CmdLine()

function alphanumsort(o)
  local function padnum(d) local dec, n = string.match(d, "(%.?)0*(.+)")
    return #dec > 0 and ("%.12f"):format(d) or ("%s%03d%s"):format(dec, #n, n) end
  table.sort(o, function(a,b)
    return tostring(a):gsub("%.?%d+",padnum)..("%3d"):format(#b)
         < tostring(b):gsub("%.?%d+",padnum)..("%3d"):format(#a) end)
  return o
end

function getFilesInDir(dir,ext)
  files = {}

  -- Go over all files in directory. We use an iterator, paths.files().
  for file in paths.files(dir) do
     -- We only load files that match the extension
     if file:find(ext .. '$') then
        -- and insert the ones we care about in our table
        table.insert(files, paths.concat(dir,file))
     end
  end

  -- Check files
  if #files == 0 then
     error('given directory doesnt contain any files of type: ' .. ext)
  end

  -- 3. Sort file names

  -- We sort files alphabetically, it's quite simple with table.sort()

  --table.sort(files, function (a,b) return a < b end)

  files = alphanumsort(files)

  print('Found files:')
  print(#files)

  return files
end



function readGridTxt(fileName)
	print("Processing "..fileName)
	file = torch.DiskFile(fileName,"r")

	--Read DownSample Settings:
	--[minX:float] [minY:float] [minZ:float] [numberIntervalsX:int] [numberIntervalsY:int] [numberIntervalsZ:int] [xDivisionSize:float] [yDivisionSize:float] [zDivisionSize:float]
	minX = file:readFloat()
	minY = file:readFloat()
	minZ = file:readFloat()
	numIntX = file:readInt()
	numIntY = file:readInt()
	numIntZ = file:readInt()
	xDiv = file:readFloat()
	yDiv = file:readFloat()
	zDiv = file:readFloat()

	--define the final return grid
	finalGrid = torch.Tensor(2,numIntX,numIntY,numIntZ):zero()
	print('Using tensor size: ',#finalGrid)

	--Read number of cloudVoxel to read
	--[numCloudVoxel:int]
	numCloudVoxels = file:readInt()

	--Read cloud voxels grid coordinates (zero indexed)
	--[x:int] [y:int] [z:int]
	for cV=1,numCloudVoxels do
		x = file:readInt()
		y = file:readInt()
		z = file:readInt()
		print('Accesing: ',x,y,z)
		finalGrid[{1,x,y,z}] = 1
	end

	--Read number of occlusion voxels
	--[numOccluVoxels:int]
	numOccluVoxels = file:readInt()

	--Read occlusion voxels grid coordinates (zero indexed)
	--[x:int] [y:int] [z:int]
	for cV=1,numOccluVoxels do
		x = file:readInt()
		y = file:readInt()
		z = file:readInt()
		print('Accesing: ',x,y,z)
		finalGrid[{2,x,y,z}] = 1
	end

	return finalGrid;
end

-- Params
cmd:option('-grids', './grids/', 'Where the .gridtxt are')
cmd:option('-out', './torch/', 'Where to output')
params = cmd:parse(arg)
print(params)

files = getFilesInDir(params.grids,'.txtgrid')
outputDir = params.out

for i=1,#files do
  t = readGridTxt(files[i])
  --flip the occlution
  t:select(1,2)[torch.eq(t:select(1,2),1)]=2
  t:select(1,2)[torch.eq(t:select(1,2),0)]=1
  t:select(1,2)[torch.eq(t:select(1,2),2)]=0
  t = t:transpose(2,4)
  torch.save(files[i]..'.grid',t)
  print('Saving '..files[i])
end

os.execute('cp '..params.grids..'*.grid '..params.out)
os.execute('rm '..params.grids..'*.grid')
