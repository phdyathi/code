require 'gnuplot'

function renderTensor(tensor)
  nonZ = tensor:nonzero()  
  gnuplot.scatter3(nonZ:select(2,3),nonZ:select(2,1),nonZ:select(2,2))

end

function renderOccGrid(filename)

  file = torch.load(filename)
  renderTensor(file)
end