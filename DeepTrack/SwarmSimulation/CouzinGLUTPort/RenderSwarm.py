import os
import sys
import subprocess
import time
import ast

SIM_PATH = "./CouzinSimGLUT/Release/CouzinSimGLUT.exe"
OUT_PATH = "./renders_exr/"

MAX_TIMESTEPS = 5

SAMPLES = 5

NUM_AGENTS_MIN = 100
NUM_AGENTS_MAX = 100
NUM_AGENTS_INCR = 100

P_OUT_DIR = "-o"
P_NUM_AGENTS = "-n"
P_START_RADIUS = "-r"
P_TIMESTEP_INCRMNT = "-t" #seconds
P_SPEED = "-s"
P_MAX_TURN_RATE = "-z"
P_DEFLECTION_RADIUS = "-x"
P_ORIENT_RADIUS = "-c"
P_ATTRACT_RADIUS = "-v"
P_ANGLE_PERCEPTION = "-b"
P_INDIVID_ERROR = "-e"
P_TIMESTEP_LIMIT = "-l"
P_SAMPLE_NUMBER = "-j"

def readConfigFile(filename):

    configList = []
    with open(filename) as f:
        for line in f:
            config = {
                'numAgents': 0,
                'zoneOfRepulsion':1, #never varied, constant
                'zoneOfOrientation':0,
                'zoneOfAttraction':0,
                'fieldOfPerceeption':0,
                'turningRate':0,
                'speed':0
            }
            #print(line+"\n")

            values = line.split(',')

            config['numAgents'] = ast.literal_eval(values[0])
            config['zoneOfOrientation'] = ast.literal_eval(values[1])
            config['zoneOfAttraction'] = ast.literal_eval(values[2])
            config['fieldOfPerceeption'] = ast.literal_eval(values[3])
            config['turningRate'] = ast.literal_eval(values[4])
            config['speed'] = ast.literal_eval(values[5])
            #print(config)
            configList.append(config)
    f.closed

    return configList

def renderWithParams(config,sampleNumber,outDir):
    print('Rendering with config: '+str(config))
    proc = subprocess.Popen([SIM_PATH,
	P_OUT_DIR,outDir,
	P_TIMESTEP_LIMIT,str(MAX_TIMESTEPS),
	P_NUM_AGENTS,str(config['numAgents']),
    P_DEFLECTION_RADIUS,str(config['zoneOfRepulsion']),
    P_ORIENT_RADIUS,str(config['zoneOfOrientation']),
    P_ATTRACT_RADIUS,str(config['zoneOfAttraction']),
    P_ANGLE_PERCEPTION,str(config['fieldOfPerceeption']),
    P_MAX_TURN_RATE,str(config['turningRate']),
    P_SPEED,str(config['speed']),
    P_SAMPLE_NUMBER,str(sampleNumber)])
    block = True
    while block:
        if(proc.poll() is not None):
            if(proc.poll() != 0):
                sys.exit(proc.poll())
            block = False
        else:
            time.sleep(1)


configFileName = sys.argv[1]
OUT_PATH = sys.argv[2]

if not os.path.exists(OUT_PATH):
    os.makedirs(OUT_PATH)


print("Reading configFile: "+configFileName)
configs = readConfigFile(configFileName)
print("Read in "+str(len(configs))+" test configs")



testCount = 1
for c in configs:
    testExrOutputFolder = OUT_PATH + 'test'+str(testCount)+'/'
    if not os.path.exists(testExrOutputFolder):
        os.makedirs(testExrOutputFolder)
    for s in range(1,SAMPLES+1):
        startTime = time.time()
        renderWithParams(c,s,testExrOutputFolder)
        endTime = time.time()
        print(endTime - startTime)
    testCount = testCount + 1
