// Vec3D.h: interface for the CVec3D class.
//
//////////////////////////////////////////////////////////////////////
#pragma once
#include <math.h>

class CVec3D  
{
public:	// declared inline where possible to maximise speed
	CVec3D() { x=0.0; y=0.0; z=0.0; }
	CVec3D(double n_x, double n_y, double n_z) { x=n_x; y=n_y; z=n_z; }
	CVec3D(CVec3D& n_v) { x=n_v.x; y=n_v.y; z=n_v.z; }

	virtual ~CVec3D();

	double x, y, z;

	CVec3D operator+(const CVec3D&);
	CVec3D operator+=(const CVec3D&);
	CVec3D operator-(const CVec3D&);
	CVec3D operator-=(const CVec3D&);
	CVec3D operator-();
	CVec3D operator=(const CVec3D&);
	CVec3D operator*(const CVec3D&);
	CVec3D operator*(double);
	CVec3D operator*=(const CVec3D&);
	CVec3D operator*=(double);
	CVec3D operator/(const CVec3D&);
	CVec3D operator/(double);
	CVec3D operator/=(double);
	double operator%(const CVec3D&);	// dot product
	CVec3D operator^(const CVec3D&);	// cross / vector product
	CVec3D operator~();	// normalise

	CVec3D Minimum(const CVec3D&);
	CVec3D Maximum(const CVec3D&);

	CVec3D Translate(const CVec3D&);
	CVec3D Translate(double t_x, double t_y, double t_z) { x+=t_x; y+=t_y; z+=t_z; }

	double AcuteAngle(CVec3D);	// calculates the acute angle between this and another vector (both unit from origin)
	double Length();	// length of vector
	double DistanceTo(const CVec3D&);

	// these functions rotate a point (x,y,z) around the appropriate axis
	void RotateX(double degrees);
	void RotateY(double degrees);
	void RotateZ(double degrees);
	void RotateXr(double radians);
	void RotateYr(double radians);
	void RotateZr(double radians);

	void RotateTowards(CVec3D to, double by_degrees);
	void RadiansToZ(double& z_angle, double& y_angle);

	void Clear() { x = 0.0; y=0.0; z=0.0; }
};
