// Numerical.h: interface for the Numerical class.
//
//////////////////////////////////////////////////////////////////////

#pragma once
#include <math.h>

#define TRUE 1
#define FALSE 0

typedef unsigned int        UINT;
typedef int					BOOL;

const double PI = 3.1415926535;
const double PI_OVER_180 = 1.74532925199433E-002;
const double PI_UNDER_180 = 5.72957795130823E+001;

#define EPSILON 1.0e-8

double Angle(double x, double y);

int Round(double val);
double Radians(double degrees);
double Degrees(double radians);
