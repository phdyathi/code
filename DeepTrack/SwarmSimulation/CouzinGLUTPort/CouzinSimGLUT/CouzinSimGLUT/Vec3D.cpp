// Vec3D.cpp: implementation of the CVec3D class.
//
//////////////////////////////////////////////////////////////////////

#include "Group3D.h"
#include "Vec3D.h"
#include <limits>
#include "Numerical.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CVec3D::~CVec3D()
{

}

CVec3D CVec3D::operator+(const CVec3D& vec)
{
	CVec3D result;
	result.x = x + vec.x;
	result.y = y + vec.y;
	result.z = z + vec.z;
	return result;
}

CVec3D CVec3D::operator+=(const CVec3D& vec)
{
	x += vec.x;
	y += vec.y;
	z += vec.z;
	return *this;
}

CVec3D CVec3D::operator-(const CVec3D& vec)
{
	CVec3D result;
	result.x = x - vec.x;
	result.y = y - vec.y;
	result.z = z - vec.z;
	return result;
}

CVec3D CVec3D::operator-=(const CVec3D& vec)
{
	x -= vec.x;
	y -= vec.y;
	z -= vec.z;
	return *this;
}

CVec3D CVec3D::operator-()
{
	CVec3D result;
	result.x = -x;
	result.y = -y;
	result.z = -z;
	return result;
}

CVec3D CVec3D::operator=(const CVec3D& vec)
{
	x = vec.x;
	y = vec.y;
	z = vec.z;
	return *this;
}

CVec3D CVec3D::operator*(const CVec3D& vec)
{
	CVec3D result;
	result.x = x * vec.x;
	result.y = y * vec.y;
	result.z = z * vec.z;
	return result;
}

CVec3D CVec3D::operator*(double val)
{
	CVec3D result;
	result.x = x * val;
	result.y = y * val;
	result.z = z * val;
	return result;
}

CVec3D CVec3D::operator*=(const CVec3D& vec)
{
	x *= vec.x;
	y *= vec.y;
	z *= vec.z;
	return *this;
}

CVec3D CVec3D::operator*=(double val)
{
	x *= val;
	y *= val;
	z *= val;
	return *this;
}

CVec3D CVec3D::operator/(const CVec3D& vec)
{
	CVec3D result;
	result.x = x / vec.x;
	result.y = y / vec.y;
	result.z = z / vec.z;
	return result;
}

CVec3D CVec3D::operator/(double val)
{
	CVec3D result;
	result.x = x / val;
	result.y = y / val;
	result.z = z / val;
	return result;
}

CVec3D CVec3D::operator/=(double val)
{
	x /= val;
	y /= val;
	z /= val;
	return *this;
}

// Dot product
double CVec3D::operator%(const CVec3D& vec)
{
	double result;
	result = x*vec.x + y*vec.y + z*vec.z;
	return result;
}

// Cross product
CVec3D CVec3D::operator^(const CVec3D& vec)
{
	CVec3D c_product;
	c_product.x = y * vec.z - z * vec.y;
	c_product.y = z * vec.x - x * vec.z;
	c_product.z = x * vec.y - y * vec.x;
	c_product=~c_product;
	return c_product;
}

// Normalise
CVec3D CVec3D::operator~()
{
	CVec3D result;
	double dist = this->Length();

	if(dist>EPSILON)
	{
		result.x = x / dist;
		result.y = y / dist;
		result.z = z / dist;
		return result;
	}
	else
	{
		result = *this;
		return result;
	}
}

CVec3D CVec3D::Maximum(const CVec3D& vec)
{
	CVec3D result;
	result.x = std::fmax(x, vec.x);
	result.y = std::fmax(y, vec.y);
	result.z = std::fmax(z, vec.z);
	return result;
}

CVec3D CVec3D::Minimum(const CVec3D& vec)
{
	CVec3D result;
	result.x = std::fmin(x, vec.x);
	result.y = std::fmin(y, vec.y);
	result.z = std::fmin(z, vec.z);
	return result;
}

double CVec3D::AcuteAngle(CVec3D vec2)
{
	// this function calculates the acute angle between two unit vectors that begin at the origin
	CVec3D vec = *this;
	vec = ~vec;	
	vec2 = ~vec2;
	double d_product = vec%vec2;
	double val = (acos(d_product));
	return (val/PI_OVER_180);
}

double CVec3D::Length()
{
	return sqrt((x*x) + (y*y) + (z*z));
}

double CVec3D::DistanceTo(const CVec3D& to_point)
{
	double dist_x = to_point.x - x;
	double dist_y = to_point.y - y;
	double dist_z = to_point.z - z;
	return (sqrt((dist_x * dist_x) + (dist_y * dist_y) + (dist_z * dist_z)));
}

void CVec3D::RotateX(double degrees)
{
	double theta = degrees*PI_OVER_180;
	CVec3D new_vec;
	new_vec.y = (float) (cos(theta)*y + sin(theta)*z);
	new_vec.z = (float) (cos(theta)*z-sin(theta)*y);
	y = new_vec.y;  z = new_vec.z;
}

void CVec3D::RotateY(double degrees)
{
	float theta = degrees * PI_OVER_180;
	CVec3D new_pos;
	new_pos.x = (float) (cos(theta)*x - sin(theta)*z);
	new_pos.z = (float) (sin(theta)*x + cos(theta)*z);
	x = new_pos.x; z = new_pos.z;
}

void CVec3D::RotateZ(double degrees)
{
	float theta = degrees * PI_OVER_180;
	CVec3D new_pos;
	new_pos.x = (float) (cos(theta)*x + sin(theta)*y);
	new_pos.y = (float) (-sin(theta)*x + cos(theta)*y);
	x = new_pos.x;  y = new_pos.y;
}

void CVec3D::RotateXr(double radians)
{
	CVec3D new_pos;
	new_pos.y = (float) (cos(radians)*y + sin(radians)*z);
	new_pos.z = (float) (cos(radians)*z-sin(radians)*y);
	y = new_pos.y;  z = new_pos.z;
}

void CVec3D::RotateYr(double radians)
{
	CVec3D new_pos;
	new_pos.x = (float) (cos(radians)*x - sin(radians)*z);
	new_pos.z = (float) (sin(radians)*x + cos(radians)*z);
	x = new_pos.x; z = new_pos.z;
}

void CVec3D::RotateZr(double radians)
{
	CVec3D new_pos;
	new_pos.x = (float) (cos(radians)*x + sin(radians)*y);
	new_pos.y = (float) (-sin(radians)*x + cos(radians)*y);
	x = new_pos.x;  y = new_pos.y;
}

void CVec3D::RotateTowards(CVec3D to, double by_degrees)
{
	CVec3D c_product;
	c_product = (*this)^to;

	double theta = Angle(c_product.x, c_product.y);
	c_product.RotateZr(theta);	// puts vector on the x-z plane

	double val = sqrt(c_product.x * c_product.x + c_product.y * c_product.y);
	double beta = Angle(c_product.z, val);
//	c_product.RotateYr(beta);	// vector now lying along positive z-axis
	// now I have the two rotations needed to put the vectors on the x-y plane // rotate both vectors
	double check_angle = this->AcuteAngle(to);

	if(check_angle<=by_degrees)	{ (*this)=to;	}
	else
	{	// could convert this to matrixes so there is only one rotation call to make it quicker
		this->RotateZr(theta);
		this->RotateYr(beta);
		this->RotateZ(-by_degrees);
		this->RotateYr(-beta);
		this->RotateZr(-theta);
	}	
}

void CVec3D::RadiansToZ(double& z_angle, double& y_angle)
{
	z_angle = Angle(this->x, this->y);
	this->RotateZr(z_angle);	// puts vector on the x-z plane

	double val = sqrt(this->x * this->x + this->y * this->y);
	y_angle = Angle(this->z, val);
}


