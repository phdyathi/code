// Agent3D.h: interface for the CAgent3D class.
//
//////////////////////////////////////////////////////////////////////
#pragma once

#include "Vec3D.h"
#include "RandDev.h"
#include "Predator3D.h"

class CAgent3D  
{
	// I know a lot of this stuff invalidates the ideals of data protection, but it is so much faster to access public members
public:
	CAgent3D();
	virtual ~CAgent3D();

// Attributes
	CVec3D r_centre;	// rotational centre of agent
	CVec3D direction;
	CVec3D desired_direction;

	// these are just for drawing it to screen
	CVec3D head;
	CVec3D tail;
	double body_length;
	UINT type;	// if you want to specify (and display) different classes of agents - e.g. species

	double zone_of_deflection;
	double zone_of_orientation;
	double zone_of_attraction;
	double zop_angle;

	bool down;

	int equivalence_class;

// Operations
	void Setup(const CVec3D set_r_centre, const CVec3D set_direction, const double set_max_turning_rate, const double set_speed, const double set_zone_of_deflection, const double set_zone_of_orientation,
		const double set_zone_of_perception, const double set_zop_angle, const double set_body_length, const UINT set_type, const double set_angular_error_sd, const double set_speed_error_sd, 
		const CVec3D set_global_vector, const double set_global_vector_error_sd, const double set_global_vector_weight);

	void Setup(const CVec3D set_r_centre, const CVec3D set_direction, const double set_max_turning_rate, const double set_speed, const double set_zone_of_deflection, const double set_zone_of_orientation,
		const double set_zone_of_perception, const double set_zop_angle, const double set_body_length, const UINT set_type, const double set_angular_error_sd, const double set_speed_error_sd);

	void Setup(const double set_max_turning_rate, const double set_speed, const double set_zone_of_deflection, const double set_zone_of_orientation, const double set_zone_of_perception, 
		const double set_zop_angle, const double set_body_length, const UINT set_type, const double set_angular_error_sd, const double set_speed_error_sd,
		const CVec3D set_global_vector, const double set_global_vector_error_sd, const double set_global_vector_weight);

	double AcuteAngleTo(CAgent3D&);
	double AcuteAngleTo(CPredator3D&);

	void Move(double timestep_inc, UINT periodic);
	void Move(double timestep_inc);	

	inline void SetSpeed(double new_speed) { speed = new_speed; }
	
	inline double GetSpeed() {return speed;}
	inline double GetMaxTurningRate() { return max_turning_rate; }
	inline double GetAngularError() { return angular_error_sd; }
	void UpdateMyGeometry();
	bool global_knowledge;

protected:
// Attributes
	double speed;	// in cm per second
	double max_turning_rate;	// in degress per second
	double angular_error_sd;
	double speed_error_sd;	// currently speed is constant, so this is not used	
	
	//	In case there are some global tendencies - e.g. allignment due to gravity / thermal gradient
	CVec3D global_vector;	
	double global_vector_error_sd;
	double global_vector_weight;
	void AddGlobalInfluence();
	void AddGravity();

	RandDev random_dev;	// RandDev object so individual agents can calculate their own errors

// Operations
	void TurnTowardsVector(CVec3D vector, double timestep_inc);
	void AddError();	// adds error to the agents rotation
	void MoveMyself(double timestep_inc);


	void CheckBoundaryConditions();

	double RandGaussAngle(double sigma, double mu);	// returns angle from -180 to 180 with a Gaussian distribution with standard deviation of sigma radians
};

