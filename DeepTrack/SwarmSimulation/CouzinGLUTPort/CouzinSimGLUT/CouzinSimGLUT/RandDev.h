// RandDev.h: interface for the RandDev class.
//
//////////////////////////////////////////////////////////////////////
#pragma once

#include "stddef.h"
#include "time.h"

class RandDev
    {
    protected:
        // used to set default seed argument from system time
        static long TimeSeed()
            {
            return (long)time(NULL);
            }

    public:
        // constructor
        RandDev
            (
            long initSeed = TimeSeed()
            );

        // set seed value
        void SetSeed
            (
            long newSeed = TimeSeed()
            );

        // get a psuedo-random number between 0.0 and 1.0
        float operator () ();

    private:
        long Seed;
    };

inline RandDev::RandDev
    (
    long initSeed
    )
    {
    if (initSeed < 0)
        Seed = initSeed;
    else
        Seed = -initSeed;
    }

inline void RandDev::SetSeed
    (
    long initSeed
    )
    {
    if (initSeed < 0)
        Seed = initSeed;
    else
        Seed = -initSeed;
    }
