#pragma once

#include <string>
#include "gl/gl.h"

#define TINYEXR_IMPLEMENTATION
#include "tinyexr\tinyexr.h"

class ExrRenderer
{
private:
	ExrRenderer() {}

public:
	ExrRenderer(const std::string& outfileName, const int width, const int height,const std::vector<GLfloat>& depthValues)
	{
		EXRHeader header;
		EXRImage image;

		InitEXRHeader(&header);

		InitEXRImage(&image);

		image.num_channels = 1;
		header.num_channels = 1;
		header.screen_window_width = 1.0;
		header.pixel_types = (int *)malloc(sizeof(int) * header.num_channels);
		header.channels = (EXRChannelInfo *)malloc(sizeof(EXRChannelInfo) * header.num_channels);
		header.requested_pixel_types = (int *)malloc(sizeof(int) * header.num_channels);
		for (int i = 0; i < header.num_channels; i++) {
			header.pixel_types[i] = TINYEXR_PIXELTYPE_FLOAT; // pixel type of input image
			header.requested_pixel_types[i] = TINYEXR_PIXELTYPE_FLOAT; // pixel type of output image to be stored in .EXR
		}


		strncpy_s(header.channels[0].name, "Z", 255); header.channels[0].name[strlen("Z")] = '\0';

		float* image_ptr[1];
		image_ptr[0] = &((float)depthValues.at(0));

		image.images = (unsigned char**)image_ptr;
		image.width = width;
		image.height = height;

		const char* err;
		int ret = SaveEXRImageToFile(&image, &header, outfileName.c_str(), &err);
		if (ret != TINYEXR_SUCCESS) {
			char errStr[250];
			sprintf_s(errStr, "Save EXR err: %s\nCan't save: %s\n", err,outfileName.c_str());
			fprintf(stderr, errStr);
			
			throw errStr;
		}
		//printf("Saved exr file. [ %s ] \n", outfileName.c_str());


		free(header.channels);
		free(header.pixel_types);
		free(header.requested_pixel_types);
	}
};