// Numerical.cpp: implementation of the Numerical class.
//
//////////////////////////////////////////////////////////////////////
#include "Group3D.h"
#include "Numerical.h"
#include "Group3D.h"
#include <math.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

double Angle(double x, double y)
{
	if(fabs(x) < EPSILON)
		if(fabs(y) < EPSILON)
			return(0.0);
		else if (y > 0.0)
			return(PI * 0.5);
		else return(PI *1.5);
	else if(x < 0.0)
		return(atan(y/x) + PI);
	else return(atan(y/x));
}

int Round(double val)
{
	return((int)(val+0.5f));
}

double Radians(double degrees)	
{
	return (degrees*PI_OVER_180);
}

double Degrees(double radians)
{
	return (radians*PI_UNDER_180);
}

