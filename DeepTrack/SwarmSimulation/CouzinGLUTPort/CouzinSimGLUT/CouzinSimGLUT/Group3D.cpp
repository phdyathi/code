#include "Group3D.h"


Group3D::Group3D() : timestep_number(0), real_time(0.0), simulation_time(0.0), timestep_time_inc(0.04348), screen_update_rate(1), total_agents(100), total_predators(0),
m_angle_of_perception(270.0), m_angular_error_sd(0.01), m_body_length(1.0), m_max_turning_rate(40.0), m_speed(3.0), m_zod(1.0), m_zoo(2.5), m_zoa(15.0), start_radius(20.0), p_interactions(0.0f),
nn_polarisation(0.0), m_num_informed_individuals(0), m_global_vector_weight(0.0)

{
	agent = new CAgent3D[total_agents];
	//	predator = new CPredator3D[total_predators];
	global_vector.x = 1.0; global_vector.y = 0.0; global_vector.z = 0.0;
}



Group3D::~Group3D()
{
	delete[]agent;
	//	delete[]predator;
}

//****************************************************************
// Public

void Group3D::SetupSimulation()
{
	timestep_number = 0;	// timestep number
	real_time = 0.0;	// absolute time (timestep x time_inc)
	simulation_time = 0.0;

	SetupAgents();
}

void Group3D::MoveAgents()
{
	CalculateSocialForces();

	for (int i = 0; i<(int)total_agents; i++)
	{
		// now each fish has a unit vector which is its desired direction of travel in the next timestep
		agent[i].Move(timestep_time_inc);
	}

	//	for(int j=0; j<total_predators; j++)
	//	{
	//		predator[j].Move(timestep_time_inc);
	//	}
}

//****************************************************************
// Protected

void Group3D::SetupAgents()
{
	delete[]agent;
	agent = new CAgent3D[total_agents];
	//	delete[]predator;
	//	predator = new CPredator3D[total_predators];

	CVec3D set_r_centre;
	CVec3D set_direction(0.0, 0.0, 1.0);	// need to be set to unit vectors

											// automate calculation of start radius
	float zod_volume = 0.0f;
	float max_volume = 0.0f;
	float zod_radius = 0.0f;
	float max_radius = 0.0f;

	for (int i = 0; i<(int)total_agents; i++)
	{
		zod_volume += ((4 / 3)*PI*(m_zod*m_zod*m_zod));

		if (m_zoo>m_zod || m_zoa>m_zod)
		{
			if (m_zoo>m_zoa)
			{
				max_volume += ((4 / 3)*PI*(m_zoo*m_zoo*m_zoo));
			}
			else
			{
				max_volume += ((4 / 3)*PI*(m_zoa*m_zoa*m_zoa));
			}
		}
	}

	zod_radius = zod_volume / ((4 / 3)*PI);
	zod_radius = pow(zod_radius, float(1.0 / 3.0));

	if (max_volume != 0.0)
	{
		max_radius = max_volume / ((4 / 3)*PI);
		max_radius = pow(max_radius, float(1.0 / 3.0));
	}
	else
	{
		max_radius = zod_radius;
	}

	float new_radius = zod_radius;//+(0.05*(max_radius-zod_radius));

	for (int j = 0; j<(int)total_agents; j++)
	{
		set_r_centre = RandBoundedPoint(start_radius);
		//	set_direction =~ RandBoundedPoint(start_radius);
		//	set_r_centre = RandBoundedPoint(new_radius*1.0);
		set_direction = ~RandBoundedPoint(new_radius);
		//	if(set_direction.x<0) {set_direction=-set_direction;}
		//		set_direction.x = 1.0; set_direction.y = 0.0; set_direction.z = 0.0;
		if (fabs(set_direction.x)<EPSILON && fabs(set_direction.y)<EPSILON && fabs(set_direction.z)<EPSILON) { return; }

		//	double speed = RandGauss(0.0125, m_speed);
		//	double speed = m_speed+rand_dev;
		//	double max_turning_rate = RandGauss(8.0, m_max_turning_rate);
		//	double zoo = RandGauss(0.75, m_zoo);
		//	double zoa = RandGauss(0.5, m_zoa);
		//	double angular_error_sd = RandGauss(0.025, m_angular_error_sd);
		//	angular_error_sd = (angular_error_sd<0.0) ? 0.0 : angular_error_sd;
		//	if(zoo<=0.0) { exit(1); }

		agent[j].Setup(set_r_centre, set_direction, m_max_turning_rate, m_speed, m_zod, m_zoo, m_zoa, m_angle_of_perception, m_body_length,
			0, m_angular_error_sd, 0, global_vector, 0.0, m_global_vector_weight);

		if (j<m_num_informed_individuals) { agent[j].global_knowledge = true; agent[j].SetSpeed(1.0*agent[j].GetSpeed()); }
		else { agent[j].global_knowledge = false; }
	}
	/*	for(int k=0; k<(int)total_predators; k++)
	{
	set_r_centre = RandBoundedPoint(5.0);
	CVec3D displacement;
	displacement.z = 55.0f;
	displacement.y = -65.0f;
	predator[k].Setup((set_r_centre+displacement), set_direction, 15.0, (m_speed*2.5f), m_zod, m_zoo, 130.0, 270.0f, (m_body_length*1.0f),
	0, m_angular_error_sd, 0, global_vector, 0.0, m_global_vector_weight);
	}*/
}

void Group3D::UpdateAgents()
{
	for (int i = 0; i<(int)total_agents; i++)
	{
		agent[i].Setup(m_max_turning_rate, m_speed, m_zod, m_zoo, m_zoa, m_angle_of_perception, m_body_length,
			0, m_angular_error_sd, 0, global_vector, 0.0, m_global_vector_weight);

		if (i<m_num_informed_individuals) { agent[i].global_knowledge = true; }
		else { agent[i].global_knowledge = false; }
	}
}

void Group3D::CalculateSocialForces()
{
	CVec3D desired_vector(1.0, 0.0, 0.0);
	double zod_count, zoo_count, zop_count = 0.0;
	double dist = 0.0;
	CVec3D temp_vector;
	CVec3D combined_vector;
	double turning_angle = 0.0;
	CVec3D total_zod, total_zoo, total_zop;
	p_interactions = 0.0f;	// number of individuals in field of perception
	double nn_dist;
	int nn;
	mean_angle = 0.0;
	mean_dist = 0.0;
	nn_polarisation = 0.0;
	CVec3D nn_pol;

	for (int i = 0; i<(int)total_agents; i++)
	{
		zod_count = 0; zoo_count = 0; zop_count = 0;
		desired_vector.Clear();	combined_vector.Clear();	total_zod.Clear();	total_zoo.Clear();	total_zop.Clear();	nn_dist = 1e300;

		for (int j = 0; j<(int)total_agents; j++)
		{
			if (j != i)	// make sure you do not include yourself
			{
				dist = agent[i].r_centre.DistanceTo(agent[j].r_centre);	// fastest way to check distance

				if (dist<nn_dist) { nn_dist = dist; nn = j; }

				if (dist < agent[i].zone_of_deflection)	// this has highest priority
				{
					zod_count++;
					temp_vector = (agent[j].r_centre - agent[i].r_centre);
					temp_vector = ~temp_vector;
					total_zod += temp_vector;
					p_interactions++;
				}
				else if (zod_count == 0 && (dist < agent[i].zone_of_attraction || dist < agent[i].zone_of_orientation))
				{
					double angle = 400.0f;

					if (agent[i].zop_angle != 360.0)	// don't do test for 360 degrees to increase speed
					{
						angle = agent[i].AcuteAngleTo(agent[j]);
					}

					if (agent[i].zop_angle == 360.0 || angle < (agent[i].zop_angle / 2.0))
					{
						if (dist < agent[i].zone_of_orientation)
						{
							zoo_count++;
							total_zoo += agent[j].direction;	// orient to its direction
							p_interactions++;
						}
						// The line below controls how the zones work
						if (dist < agent[i].zone_of_attraction)
							//	if(dist > agent[i].zone_of_orientation && dist < agent[i].zone_of_attraction)
						{
							zop_count++;
							temp_vector = (agent[j].r_centre - agent[i].r_centre);
							temp_vector = ~temp_vector;
							total_zop += temp_vector;
							p_interactions++;
						}
					}
				}
			}
		}

		// Calculate desired movement
		if (zod_count>0)
		{
			total_zod /= zod_count;

			if (fabs(total_zod.x) < EPSILON && fabs(total_zod.y) < EPSILON && fabs(total_zod.z) < EPSILON)	// if it has completely cancelled out
			{
				desired_vector = ~agent[i].direction;	// no stimulus so keep going in same direction - noise gets added later
			}
			else
			{
				total_zod = -total_zod;		// should be at origin now - so this will make the negative vector
				desired_vector = total_zod;
				desired_vector = ~desired_vector;
			}
		}
		else if (zoo_count>0 || zop_count>0)
		{
			if (zoo_count == 0 || (fabs(total_zoo.x) < EPSILON && fabs(total_zoo.y) < EPSILON && fabs(total_zoo.z) < EPSILON))	// if no zoo fish were found or if they cancelled each other out
			{
				total_zoo.Clear();	// zoo will have no influence
			}
			else	// calculate zoo vector
			{
				// add your own influence to the orientation vector
				total_zoo += agent[i].direction;
				zoo_count++;
				total_zoo = ~total_zoo;
			}

			if (zop_count == 0 || (fabs(total_zop.x) < EPSILON && fabs(total_zop.y) < EPSILON && fabs(total_zop.z) < EPSILON))	// if no zop fish were found or if they cancelled each other out
			{
				total_zop.Clear();	// zop will have no influence
			}
			else	// calculate zop_vector
			{
				total_zop = ~total_zop;
			}

			// combine zoo and zop vectors to calculate desired direction
			// add them both - if the combined vector is cancelled out do a correlated random walk - otherwise set it as desitred vector
			combined_vector = (total_zoo + total_zop);	// could put weightings in here for a more complicated model
			if (fabs(combined_vector.x) < EPSILON && fabs(combined_vector.y) < EPSILON && fabs(combined_vector.z) < EPSILON)	// if all vectors cancelled themselves out
			{
				desired_vector = ~agent[i].direction;	// allows a correlated random walk - all influences have cancelled out
			}
			else
			{
				desired_vector = ~combined_vector;
			}
		}
		// This bit below just allows a correlated random walk
		else
		{
			desired_vector = ~agent[i].direction;
		}
		agent[i].desired_direction = desired_vector;

		// calculate angle to nearest neighbour
		if (nn_dist != 1e300)
		{
			mean_angle += (agent[i].AcuteAngleTo(agent[nn]));
			nn_pol = agent[i].direction + agent[nn].direction;
			nn_pol /= 2.0;
			nn_polarisation += (nn_pol.Length());

			mean_dist += (agent[i].r_centre.DistanceTo(agent[nn].r_centre));
		}
	}
	p_interactions /= total_agents;
	mean_angle /= total_agents;
	mean_dist /= total_agents;
	nn_polarisation /= total_agents;
}


CVec3D Group3D::RandBoundedPoint(double radius)
{
	CVec3D vec(1.0, 0.0, 0.0);

	double theta = Angle(vec.x, vec.y);
	vec.RotateZr(theta);
	double val = sqrt(vec.x * vec.x + vec.y + vec.y);
	double beta = Angle(vec.z, val);
	vec.RotateYr(beta);

	double rand = random_dev();
	double degrees = rand*360.0;
	vec.RotateY(degrees);

	rand = random_dev();
	degrees = rand*360.0;

	vec.RotateZ(degrees);

	vec.RotateYr(-beta);
	vec.RotateZr(-theta);

	double dist_from_centre = (random_dev()*radius);

	vec *= dist_from_centre;
	return vec;
}

void Group3D::EquivalenceClasses()
{
	int n = (int)total_agents;
	int *nf = new int[n];
	int j, k;

	for (j = 0; j<n; j++)
	{
		nf[j] = j;

		for (k = 0; k<(j); k++)
		{
			nf[k] = nf[nf[k]];
			if (Equivalent(agent[j], agent[k])) nf[nf[nf[k]]] = j;
		}
	}
	for (j = 0; j<n; j++) nf[j] = nf[nf[j]];

	for (int m = 0; m<n; m++)
	{
		agent[m].equivalence_class = nf[m];
	}
}

BOOL Group3D::Equivalent(CAgent3D& agent1, CAgent3D& agent2)
{
	double dist1, dist2 = 0.0f;

	double largest_zone = (agent1.zone_of_deflection>agent1.zone_of_attraction) ? agent1.zone_of_deflection : agent1.zone_of_attraction;
	largest_zone = (largest_zone>agent1.zone_of_orientation) ? largest_zone : agent1.zone_of_orientation;

	dist1 = agent1.r_centre.DistanceTo(agent2.r_centre);

	double angle = 0.0;

	if (dist1 < largest_zone)
	{
		if (agent1.zop_angle != 360.0)	// don't do test for 360 degrees to increase speed
		{
			angle = agent1.AcuteAngleTo(agent2);
		}
		if (agent1.zop_angle == 360.0 || angle < (agent1.zop_angle / 2.0))
		{
			return TRUE;
		}
	}
	return FALSE;
}

void Group3D::RotateToZ()
{
	CVec3D centroid;
	CVec3D direction;

	for (int i = 0; i<(int)total_agents; i++)
	{
		centroid += agent[i].r_centre;
		direction += agent[i].direction;
	}
	centroid /= total_agents;
	direction /= total_agents;

	double z_angle, y_angle = 0.0;

	direction.RadiansToZ(z_angle, y_angle);

	for (int j = 0; j<(int)total_agents; j++)
	{
		agent[j].r_centre - centroid;

		agent[j].direction.RotateZr(z_angle);
		agent[j].direction.RotateYr(y_angle);
	}
}

double Group3D::RandGauss(double sigma, double mu)
{
	static int iset = 0;
	static double gset;
	double fac, rsq, v1, v2;

	if (iset == 0)
	{
		do
		{
			v1 = 2.0 * random_dev() - 1.0;
			v2 = 2.0 * random_dev() - 1.0;
			rsq = v1*v1 + v2*v2;
		} while (rsq >= 1.0 || rsq == 0.0);

		fac = sqrt(-2.0*log(rsq) / rsq);

		gset = v1*fac;
		iset = 1;
		return (((v2*fac)*sigma) + mu);
	}
	else
	{
		iset = 0;
		return ((gset*sigma) + mu);
	}
}
