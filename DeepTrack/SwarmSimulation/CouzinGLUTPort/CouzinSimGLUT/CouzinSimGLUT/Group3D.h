#pragma once
#include "Numerical.h"
#include "Vec3D.h"
#include "RandDev.h"
#include "Agent3D.h"

class Group3D
{
public:
	Group3D();
	~Group3D();


	// Attributes
public:
	unsigned long timestep_number;	// timestep number
	double real_time;	// absolute time (timestep x time_inc)
	double simulation_time;
	double timestep_time_inc;	// time increment (between timesteps)
	UINT screen_update_rate;

	CAgent3D* agent;
	CPredator3D* predator;

	UINT total_agents;
	UINT total_predators;
	double start_radius;

	double	m_angle_of_perception;
	double	m_angular_error_sd;
	double	m_body_length;
	double	m_max_turning_rate;
	double	m_speed;
	double	m_zod;
	double  m_zoo;
	double	m_zoa;
	int m_num_informed_individuals;
	double m_global_vector_weight;

	float p_interactions;
	double mean_angle;
	double mean_dist;
	double nn_polarisation;

	CVec3D global_vector;

	// Operations
public:
	void SetupSimulation();
	void MoveAgents();

	void UpdateAgents();	// allows agent properties to be changed without restatring the simulation

	BOOL Equivalent(CAgent3D&, CAgent3D&);	// testing for equivalence classes (school membership)
	void EquivalenceClasses();
	double RandGauss(double sigma, double mu);

	void RotateToZ();

protected:
	void SetupAgents();
	void CalculateSocialForces();
	CVec3D RandBoundedPoint(double radius);

	RandDev random_dev;
};

