// Agent3D.cpp: implementation of the CAgent3D class.
//
//////////////////////////////////////////////////////////////////////
#include "Group3D.h"
#include "Agent3D.h"
#include "Numerical.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAgent3D::CAgent3D() : max_turning_rate(0.0), speed(0.0), zone_of_deflection(0.0), zone_of_orientation(0.0), zone_of_attraction(0.0), zop_angle(0.0), body_length(0.0), angular_error_sd(0.0), speed_error_sd(0.0), 
						type(0), global_vector_error_sd(0.0), global_vector_weight(0.0), equivalence_class(0), global_knowledge(false), down(false)
{
	global_vector.x = 1.0; global_vector.y = 0.0; global_vector.z = 0.0;
}

CAgent3D::~CAgent3D()
{

}

//************************************************************************
// Public
void CAgent3D::Setup(const CVec3D set_r_centre, const CVec3D set_direction, const double set_max_turning_rate, const double set_speed, const double set_zone_of_deflection, const double set_zone_of_orientation,
				const double set_zone_of_attraction, const double set_zop_angle, const double set_body_length, const UINT set_type, const double set_angular_error_sd, const double set_speed_error_sd, 
				const CVec3D set_global_vector, const double set_global_vector_error_sd, const double set_global_vector_weight)
{

	r_centre = set_r_centre;	direction = set_direction;	max_turning_rate = set_max_turning_rate;	speed = set_speed;	zone_of_deflection = set_zone_of_deflection;	zone_of_orientation = set_zone_of_orientation;
	zone_of_attraction = set_zone_of_attraction;	zop_angle = set_zop_angle;	body_length = set_body_length;	type = set_type;	angular_error_sd = set_angular_error_sd;	speed_error_sd = set_speed_error_sd;
	global_vector_error_sd = set_global_vector_error_sd;	global_vector_weight = set_global_vector_weight;

	UpdateMyGeometry();
}

void CAgent3D::Setup(const CVec3D set_r_centre, const CVec3D set_direction, const double set_max_turning_rate, const double set_speed, const double set_zone_of_deflection, const double set_zone_of_orientation,
				const double set_zone_of_attraction, const double set_zop_angle, const double set_body_length, const UINT set_type, const double set_angular_error_sd, const double set_speed_error_sd)
{
	r_centre = set_r_centre;	direction = set_direction;	max_turning_rate = set_max_turning_rate;	speed = set_speed;	zone_of_deflection = set_zone_of_deflection;	zone_of_orientation = set_zone_of_orientation;
	zone_of_attraction = set_zone_of_attraction;	zop_angle = set_zop_angle;	body_length = set_body_length;	type = set_type;	angular_error_sd = set_angular_error_sd;	speed_error_sd = set_speed_error_sd;

	UpdateMyGeometry();
}

void CAgent3D::Setup(const double set_max_turning_rate, const double set_speed, const double set_zone_of_deflection, const double set_zone_of_orientation, const double set_zone_of_attraction, 
					 const double set_zop_angle, const double set_body_length, const UINT set_type, const double set_angular_error_sd, const double set_speed_error_sd,
					 const CVec3D set_global_vector, const double set_global_vector_error_sd, const double set_global_vector_weight)
{
	max_turning_rate = set_max_turning_rate;	speed = set_speed;	zone_of_deflection = set_zone_of_deflection;	zone_of_orientation = set_zone_of_orientation; zone_of_attraction = set_zone_of_attraction;
	zop_angle = set_zop_angle;	body_length = set_body_length;	type = set_type;	angular_error_sd = set_angular_error_sd;	speed_error_sd = set_speed_error_sd;

	UpdateMyGeometry();

	global_vector_error_sd = set_global_vector_error_sd;	global_vector_weight = set_global_vector_weight;
}

void CAgent3D::Move(double timestep_inc, UINT periodic)	// so we have direction (current direction) and a desired direction
{	
	AddGravity();
	AddError();
	TurnTowardsVector(desired_direction, timestep_inc);
	MoveMyself(timestep_inc);	
	UpdateMyGeometry();
}

void CAgent3D::Move(double timestep_inc)
{
	if(global_knowledge==true) { AddGlobalInfluence(); }
//	else { AddGravity(); }

//	AddGravity();
//	CheckBoundaryConditions();
	AddError();
	TurnTowardsVector(desired_direction, timestep_inc);
	MoveMyself(timestep_inc);	
	UpdateMyGeometry();
}

//************************************************************************
// Protected
double CAgent3D::AcuteAngleTo(CAgent3D& other_agent)	// this function calculates the acute angle between two unit vectors that begin at the origin
{
	CVec3D vec;
	vec = (other_agent.r_centre - r_centre);
	return direction.AcuteAngle(vec);
}

double CAgent3D::AcuteAngleTo(CPredator3D& other_agent)	// this function calculates the acute angle between two unit vectors that begin at the origin
{
	CVec3D vec;
	vec = (other_agent.r_centre - r_centre);
	return direction.AcuteAngle(vec);
}

void CAgent3D::MoveMyself(double timestep_inc)
{
	CVec3D velocity = direction;
	velocity*=(speed*timestep_inc);
	r_centre+=velocity;
}

void CAgent3D::TurnTowardsVector(CVec3D vector, double timestep_inc)	// for speed it is up to the caller to make sure the vector passed is normalised 
{
	double max_degrees = max_turning_rate*timestep_inc;
	direction =~ direction;
	direction.RotateTowards(vector, max_degrees);
}

void CAgent3D::CheckBoundaryConditions()
{
	CVec3D new_vec = desired_direction;

/*	if((r_centre.y + 3.0) >= 5.0)
	{
		global_vector.x = desired_direction.x;	global_vector.y = -1.0; global_vector.z = desired_direction.z;
		global_vector =~ global_vector;

		new_vec += (global_vector*0.5);
	//	desired_direction =~ new_vec;
	}*/
	if((r_centre.y + 1.0) >= 5.0)
	{
	//	global_vector.x = desired_direction.x;	global_vector.y = -1.0; global_vector.z = desired_direction.z;
	//	global_vector =~ global_vector;

	//	new_vec += (global_vector*1.0);
	//	desired_direction =~ new_vec;
		down = true;
	}

//	desired_direction =~ new_vec;

//	if(r_centre.y >= 10.0)
//	{
//		r_centre.y = 10.0;
//	}

	if((r_centre.y - 1.0) <= -5.0)
	{
	//	global_vector.x = desired_direction.x;	global_vector.y = 1.0; global_vector.z = desired_direction.z;
	//	global_vector =~ global_vector;

	//	new_vec += (global_vector*1.0);
	//	desired_direction =~ new_vec;
		down = false;
	}

	if(down == true)
	{
		global_vector.x = desired_direction.x;	global_vector.y = -1.0; global_vector.z = desired_direction.z;
		global_vector =~ global_vector;
		new_vec += (global_vector*1.0);
		desired_direction =~ new_vec;
	}
	else
	{
		global_vector.x = desired_direction.x;	global_vector.y = 1.0; global_vector.z = desired_direction.z;
		global_vector =~ global_vector;
		new_vec += (global_vector*1.0);
		desired_direction =~ new_vec;

	}

//	if(r_centre.y <= -6.0)
//	{
//		r_centre.y = -6.0;
//	}
}

void CAgent3D::UpdateMyGeometry()
{
	direction =~ direction;	// this is the second normalisation of direction and as such isn't strictly necessary - however it is essential to keep it unit for calculations outwith this class
	head = direction;	
	head *= (body_length/2);
	tail =- head;
	head+= r_centre;
	tail+=r_centre;
}

void CAgent3D::AddError()
{
	double theta = Angle(direction.x, direction.y);
	direction.RotateZr(theta);	// this puts the vector on the x-z plane
	double val = sqrt(direction.x * direction.x + direction.y * direction.y);
	double beta = Angle(direction.z, val);
	direction.RotateYr(beta);	// vector now lying along positive z axis

	double rand_gauss_angle = RandGaussAngle(angular_error_sd, 0.0);	// angular_error_sd is the error s.d in radians
	direction.RotateY(rand_gauss_angle);

	double rand_uniform = random_dev();
	double degrees = rand_uniform*360.0;

	direction.RotateZ(degrees);
	direction.RotateYr(-beta);
	direction.RotateZr(-theta);
}

// NOTE: this function is designed to work with angles so sigma is the standard deviation in radians - it is not a standard Gaussian distributed random number generator.
// returns angle in degrees with s.d. in radians (just to be nice and confusing)
double CAgent3D::RandGaussAngle(double sigma, double mu)
{
	// sigma is the s.d. and mu is the mean
	double sample = 10.0;
	double w1, w2;
	while(sample < -0.50 || sample > 0.50)
	{
		w1 = random_dev();
		w2 = random_dev();
		w1 = sqrt(-2.0 * log(w1));
		w2 = 2 * PI * w2;
		sample = w1 * sin(w2);
		sample*=sigma;
		sample/=(2*PI);
		sample += mu;
	}
	return (sample*360.0);
}

void CAgent3D::AddGlobalInfluence()
{
	CVec3D new_vec = desired_direction+(global_vector*global_vector_weight);
	desired_direction =~ new_vec;
}

void CAgent3D::AddGravity()
{
	global_vector.x = desired_direction.x;	global_vector.y = 0.0; global_vector.z = desired_direction.z;
	global_vector =~ global_vector;

	CVec3D new_vec = desired_direction+(global_vector*0.5);
	desired_direction =~ new_vec;
}