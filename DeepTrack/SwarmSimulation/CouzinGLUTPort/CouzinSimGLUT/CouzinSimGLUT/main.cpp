#include <GL/glut.h>
#include <vector>
#include <iostream>
#include <sstream>

#include "ExrRenderer.h"
#include "Camera.h"
#include "Numerical.h"
#include "Group3D.h"

#include "FrustumG.h"

#include "cxxopts.h"

std::string output_dir = "./renders_exr/";
int time_step_limit = 1000;
bool renderExr = true;

/* Global variables */
char title[] = "3D Shapes";

int WINDOW_WIDTH = 1024;//800;// 1024;
int WINDOW_HEIGHT = 1024;//600;// 1024;
const GLdouble zNear = 10;
const GLdouble zFar = 1000;

int refreshMills = 15;        // refresh interval in milliseconds [NEW]

Camera* camera;

Group3D group3D;

CVec3D group_centroid;	// for positioning of camera

bool moveCamera = false;

std::string sampleNumber = "1";

//parameters
#define P_OUT_DIR "o"
#define P_NUM_AGENTS "n"
#define P_START_RADIUS "r"
#define P_TIMESTEP_INCRMNT "t" //SECONDS
#define P_SPEED "s"
#define P_MAX_TURN_RATE "z"
#define P_DEFLECTION_RADIUS "x"
#define P_ORIENT_RADIUS "c"
#define P_ATTRACT_RADIUS "v"
#define P_ANGLE_PERCEPTION "b"
#define P_INDIVID_ERROR "e"
#define P_TIMESTEP_LIMIT "l"
#define P_RENDER_EXR "p"
#define P_SAMPLE_NUMBER "j"
//#define P_RENDER_SIZE "k" //TODO: not quite working

// Set lighting intensity and color
GLfloat qaAmbientLight[] = { 0.2, 0.2, 0.2, 1.0 };
GLfloat qaDiffuseLight[] = { 0.8, 0.8, 0.8, 1.0 };
GLfloat qaSpecularLight[] = { 1.0, 1.0, 1.0, 1.0 };
GLfloat emitLight[] = { 0.9, 0.9, 0.9, 0.01 };
GLfloat Noemit[] = { 0.0, 0.0, 0.0, 1.0 };
// Light source position
GLfloat qaLightPosition[] = { 0.5, 0, -3.5, -2.5 };

GLfloat qaGreen[] = { 0.0, 1.0, 0.0, 1.0 }; //Green Color
GLfloat qaWhite[] = { 1.0, 1.0, 1.0, 1.0 }; //White Color

FrustumG frustrum;

/* Initialize OpenGL Graphics */
void initGL() {
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // Set background color to black and opaque
	glClearDepth(1.0f);                   // Set background depth to farthest
	glEnable(GL_DEPTH_TEST);   // Enable depth testing for z-culling
	glDepthFunc(GL_LEQUAL);    // Set the type of depth-test
	glShadeModel(GL_SMOOTH);   // Enable smooth shading
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);  // Nice perspective corrections
	group3D.SetupSimulation();
	Camera::KEYBOARD mode = Camera::QWERTY;
	camera = new Camera(Camera::QWERTY, 0, 10, -30, 1.88,-0.02, 0.01, 0.1f, WINDOW_WIDTH, WINDOW_HEIGHT);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(49.134, float(WINDOW_WIDTH) / float(WINDOW_HEIGHT), zNear, zFar);
	frustrum.setCamInternals(49.134, float(WINDOW_WIDTH) / float(WINDOW_HEIGHT), zNear, zFar);

	// Enable lighting
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	// Set lighting intensity and color
	glLightfv(GL_LIGHT0, GL_AMBIENT, qaAmbientLight);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, qaDiffuseLight);
	glLightfv(GL_LIGHT0, GL_SPECULAR, qaSpecularLight);

	// Set the light position
	glLightfv(GL_LIGHT0, GL_POSITION, qaLightPosition);
}



void outputDepthEXR(){
	std::vector<GLfloat> depthValues(WINDOW_WIDTH * WINDOW_HEIGHT, 0);
	glReadPixels(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, GL_DEPTH_COMPONENT, GL_FLOAT, &depthValues[0]);
	
	std::vector<GLdouble> nonzeros;
	int x = 0;
	int y = 0;
	for (int i = 0; i < depthValues.size(); i++)
	{

		GLdouble modelview[16];
		GLdouble projection[16];
		GLint viewport[4];

		GLdouble unprojX;
		GLdouble unprojY;
		GLdouble unprojZ;
		glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
		glGetDoublev(GL_PROJECTION_MATRIX, projection);
		glGetIntegerv(GL_VIEWPORT, viewport);
		GLfloat winZ;
		//glReadPixels(x, y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ);
		if (depthValues[i] != 1.0f)
		{
			gluUnProject(x, y, depthValues[i], modelview, projection, viewport, &unprojX, &unprojY, &unprojZ);
			//std::cout << "The depth is " << unprojZ - camera->getZ() << std::endl;
			depthValues[i] = unprojZ - camera->getZ();
		}
		else
		{
			depthValues[i] = 9999.0f;
		}
		
		x++;
		if (x >= viewport[2])
		{
			x = 0;
			y++;
		}

	}
	std::stringstream ss;
	ss << output_dir <<sampleNumber<< "_"<< group3D.timestep_number << ".exr";
	try {
		ExrRenderer exrRender(ss.str(), WINDOW_WIDTH, WINDOW_HEIGHT, depthValues);
	}
	catch (const char* msg)
	{
		exit(-1);
	}
}

void renderCentroid()
{
	glLineWidth(2);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);	glVertex3f(-15, 0, 0);	glColor3f(1.0f, 1.0f, 1.0f);	glVertex3f(15, 0, 0);
	glEnd();
	glLineWidth(2);
	glBegin(GL_LINES);
	glColor3f(0.0f, 1.0f, 0.0f);	glVertex3f(0, -15, 0);	glColor3f(1.0f, 1.0f, 1.0f);	glVertex3f(0, 15, 0);
	glEnd();
	glLineWidth(2);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);		glVertex3f(0, 0, -15);	glColor3f(1.0f, 1.0f, 1.0f);	glVertex3f(0, 0, 15);
	glEnd();
}

//based from this article: http://www.lighthouse3d.com/tutorials/view-frustum-culling/clip-space-approach-extracting-the-planes/
//return true if any part of the swarm is in the frustrum
bool isSwarmInFrustrum()
{
	for (int i = 0; i < group3D.total_agents; i++)
	{
		CVec3D center = group3D.agent[i].r_centre;// - group_centroid;

		//A point is in the frustrum if:
		// -1 < x < 1
		// -1 < y < 1
		// -1 < z < 1
		if(frustrum.sphereInFrustum(Vec3(center.x, center.y, center.z),group3D.m_body_length/2) != FrustumG::OUTSIDE)
		{
			return true;
		}

	}
	return false;
}

void renderSwarm() {
	CVec3D tail_pos;
	CVec3D head_pos;
	CVec3D center_pos;

	CVec3D polarisation;

	UINT j = 0;

	for (j = 0; j < group3D.total_agents; j++)
	{


		tail_pos = (group3D.agent[j].tail);// -group_centroid);
		head_pos = (group3D.agent[j].head);// -group_centroid);
		center_pos = (group3D.agent[j].r_centre);// -group_centroid);
		//tail_pos = group3D.agent[j].tail;
		//head_pos = group3D.agent[j].head;


		//old		fout_data << pDoc->timestep_number << "\t" << (j+1) << "\t" << head_pos.x << "\t" << head_pos.y << "\t" << head_pos.z << "\t" << tail_pos.x << "\t" << tail_pos.y << "\t" << tail_pos.z << "\n";

		/*	new*///		fout_data << (pDoc->timestep_number-1) << "\t" << (j+1) << "\t" << pDoc->agent[j].r_centre.x << "\t" << pDoc->agent[j].r_centre.y << "\t" << pDoc->agent[j].r_centre.z << "\t" 
				 //		<< pDoc->agent[j].direction.x << "\t" << pDoc->agent[j].direction.y << "\t" << pDoc->agent[j].direction.z << "\n";

		polarisation += group3D.agent[j].direction;
		
		glLineWidth((float)5);
		glBegin(GL_LINES);
		if (j < group3D.m_num_informed_individuals)
		{

			glColor3f(0.0f, 0.0f, 0.2f);
			glVertex3f((GLfloat)tail_pos.x, (GLfloat)tail_pos.y, (GLfloat)tail_pos.z);
			glColor3f(1.0f, 0.0f, 0.0f);
			glVertex3f((GLfloat)head_pos.x, (GLfloat)head_pos.y, (GLfloat)head_pos.z);
		}
		else
		{
			glColor3f(1.0f, 0.0f, 0.2f);
			glVertex3f((GLfloat)tail_pos.x, (GLfloat)tail_pos.y, (GLfloat)tail_pos.z);
			glColor3f(0.0f, 1.0f, 1.0f);
			glVertex3f((GLfloat)head_pos.x, (GLfloat)head_pos.y, (GLfloat)head_pos.z);
		}

		glEnd();
		
		glPushMatrix();
		glTranslatef(center_pos.x, center_pos.y, center_pos.z);
		
		CVec3D a = CVec3D(0, 0, 1) ^ group3D.agent[j].direction;
		float omega = acos(CVec3D(0,0,1)% group3D.agent[j].direction);
		glRotatef(Degrees(omega), a.x, a.y, a.z);

		glScalef(0.3, 0.3, 1);

		// Set material properties
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, qaGreen);

		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, qaGreen);

		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, qaWhite);

		glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 500);
		
		glutSolidSphere(group3D.m_body_length, 25, 25);
		glPopMatrix();
		
	}
	
	if (isSwarmInFrustrum())
		std::cout << "Swarm is in frustrum" << std::endl;
	else
	{
		std::cout << "Swarm is not in view" << std::endl;
		group3D.SetupSimulation();
	}
}

/* Handler for window-repaint event. Called back when the window first appears and
whenever the window needs to be re-painted. */
void display() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear color and depth buffers

	glMatrixMode(GL_MODELVIEW);     // To operate on model-view matrix
	
	glLoadIdentity();                 // Reset the model-view matrix
	camera->translation();
	gluLookAt(camera->getX(), camera->getY(), camera->getZ(), camera->getSightX(), camera->getSightY(), camera->getSightZ(), 0, 1, 0);
	frustrum.setCamDef(Vec3(camera->getX(), camera->getY(), camera->getZ()), Vec3(camera->getSightX(), camera->getSightY(), camera->getSightZ()), Vec3(0, 1, 0));


									// Render a color-cube consisting of 6 quads with different colors
	glTranslatef(0.0f, 0.0f, 0.0f);  // Move right and into the screen
	//renderCentroid();

	renderSwarm();

	glutSwapBuffers();  // Swap the front and back frame buffers (double buffering)
	if(renderExr)
		outputDepthEXR();
}

/* Handler for window re-size event. Called back when the window first appears and
whenever the window is re-sized with its new width and height */
void reshape(GLsizei width, GLsizei height) {  // GLsizei for non-negative integer
											   // Compute aspect ratio of the new window
	if (height == 0) height = 1;                // To prevent divide by 0
	GLfloat aspect = (GLfloat)width / (GLfloat)height;

	// Set the viewport to cover the new window
	glViewport(0, 0, width, height);

	// Set the aspect ratio of the clipping volume to match the viewport
	glMatrixMode(GL_PROJECTION);  // To operate on the Projection matrix
	glLoadIdentity();             // Reset
								  // Enable perspective projection with fovy, aspect, zNear and zFar
	gluPerspective(45.0f, aspect, 0.1f, 100.0f);
}

/* Called back when timer expired [NEW] */
void timer(int value) {
	group3D.MoveAgents();

	group_centroid.Clear();
	for (int i = 0; i<(int)group3D.total_agents; i++)
	{
		group_centroid += group3D.agent[i].r_centre;
	}
	group_centroid /= group3D.total_agents;

	if ((group3D.timestep_number % group3D.screen_update_rate) == 0) {
		glutPostRedisplay(); }
	group3D.timestep_number++;
	std::cout << "Timestep = " << group3D.timestep_number << std::endl;
;      // Post re-paint request to activate display()
	glutTimerFunc(refreshMills, timer, 0); // next timer call milliseconds later

	//exit after limit
	if (group3D.timestep_number > time_step_limit)
	{
		std::cout << "Timestep (" << group3D.timestep_number << ") exceeded limit ("<< time_step_limit << ")\nExiting..." << std::endl;
		exit(0);
	}
}


void keyboard(unsigned char key, int x, int y) {
	camera->setKeyboard(key, true);
}

void keyboardUp(unsigned char key, int x, int y) {
	camera->setKeyboard(key, false);

	if (key == 'k')
		outputDepthEXR();
}

void mouseMove(int x, int y) {
	if (moveCamera)
	{
		camera->rotation(x, y);
		if (x >= WINDOW_WIDTH || x <= 0 || y >= WINDOW_HEIGHT || y <= 0) {
			glutWarpPointer(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2);
			camera->setMouse(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2);
		}
	}
}

void mouseButton(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
		moveCamera = !moveCamera;

	if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
	{
		GLdouble modelview[16];
		GLdouble projection[16];
		GLint viewport[4];

		GLdouble unprojX;
		GLdouble unprojY;
		GLdouble unprojZ;
		glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
		glGetDoublev(GL_PROJECTION_MATRIX, projection);
		glGetIntegerv(GL_VIEWPORT, viewport);
		GLfloat winZ;
		glReadPixels(x, y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ);
		gluUnProject(x, y, winZ, modelview, projection, viewport, &unprojX, &unprojY, &unprojZ);
		std::cout << "The depth is " << unprojZ - camera->getZ() << std::endl;
	}

}

/* Main function: GLUT runs as a console application starting at main() */
int main(int argc, char** argv) {

	cxxopts::Options cmd_opts("CouzinGLUTPort","A glut renderer for Couzin's swarm simulation.");
	cmd_opts.add_options()
		(P_OUT_DIR, "Output directory", cxxopts::value<std::string>())
		(P_NUM_AGENTS, "Number of agents", cxxopts::value<int>())
		(P_START_RADIUS,"Starting radius",cxxopts::value<double>())
		(P_TIMESTEP_INCRMNT, "Timestep increment (s)", cxxopts::value<double>())
		(P_SPEED, "Speed", cxxopts::value<double>())
		(P_MAX_TURN_RATE, "Max turning rate (degrees/s)", cxxopts::value<double>())
		(P_DEFLECTION_RADIUS, "Zone of deflection radius", cxxopts::value<double>())
		(P_ATTRACT_RADIUS, "Zone of attraction radius", cxxopts::value<double>())
		(P_ORIENT_RADIUS, "Zone of orientation radius", cxxopts::value<double>())
		(P_ANGLE_PERCEPTION, "Angle of perception (degrees)", cxxopts::value<double>())
		(P_INDIVID_ERROR, "Individual error (s. d. )", cxxopts::value<double>())
		(P_TIMESTEP_LIMIT, "Number of timestep to run", cxxopts::value<int>())
		(P_RENDER_EXR, "Whether to render exrs", cxxopts::value<bool>())
		(P_SAMPLE_NUMBER, "Sample number", cxxopts::value<std::string>())
		;

	auto parsed = cmd_opts.parse(argc, argv);

	if (parsed.count(P_OUT_DIR))
	{
		output_dir = parsed[P_OUT_DIR].as<std::string>();
		std::cout << "output_dir = " << output_dir << std::endl;
	}

	if (parsed.count(P_NUM_AGENTS))
	{
		group3D.total_agents = parsed[P_NUM_AGENTS].as<int>();
		std::cout << "total_agents = " << group3D.total_agents << std::endl;
	}

	if (parsed.count(P_START_RADIUS))
	{
		group3D.start_radius = parsed[P_START_RADIUS].as<double>();
		std::cout << "start_radius = " << group3D.start_radius << std::endl;
	}

	if (parsed.count(P_TIMESTEP_INCRMNT))
	{
		group3D.timestep_time_inc = parsed[P_TIMESTEP_INCRMNT].as<double>();
		std::cout << "timestep_time_inc = " << group3D.timestep_time_inc << std::endl;
	}

	if (parsed.count(P_SPEED))
	{
		group3D.m_speed = parsed[P_SPEED].as<double>();
		std::cout << "m_speed = " << group3D.m_speed << std::endl;
	}

	if (parsed.count(P_MAX_TURN_RATE))
	{
		group3D.m_max_turning_rate = parsed[P_MAX_TURN_RATE].as<double>();
		std::cout << "m_max_turning_rate = " << group3D.m_max_turning_rate << std::endl;
	}
	
	if (parsed.count(P_DEFLECTION_RADIUS))
	{
		group3D.m_zod = parsed[P_DEFLECTION_RADIUS].as<double>();
		std::cout << "m_zod = " << group3D.m_zod << std::endl;
	}

	if (parsed.count(P_ATTRACT_RADIUS))
	{
		group3D.m_zoa = parsed[P_ATTRACT_RADIUS].as<double>();
		std::cout << "m_zoa = " << group3D.m_zoa << std::endl;
	}

	if (parsed.count(P_ORIENT_RADIUS))
	{
		group3D.m_zoo = parsed[P_ORIENT_RADIUS].as<double>();
		std::cout << "m_zoo = " << group3D.m_zoo << std::endl;
	}

	if (parsed.count(P_ANGLE_PERCEPTION))
	{
		group3D.m_angle_of_perception = parsed[P_ANGLE_PERCEPTION].as<double>();
		std::cout << "m_angle_of_perception = " << group3D.m_angle_of_perception << std::endl;
	}

	if (parsed.count(P_INDIVID_ERROR))
	{
		group3D.m_angular_error_sd = parsed[P_INDIVID_ERROR].as<double>();
		std::cout << "m_angular_error_sd = " << group3D.m_angular_error_sd << std::endl;
	}

	if (parsed.count(P_TIMESTEP_LIMIT))
	{
		time_step_limit = parsed[P_TIMESTEP_LIMIT].as<int>();
		std::cout << "time_step_limit = " << time_step_limit << std::endl;
	}

	if (parsed.count(P_RENDER_EXR))
	{
		renderExr = parsed[P_RENDER_EXR].as<bool>();
		std::cout << "renderExr = " << renderExr << std::endl;
	}

	if (parsed.count(P_SAMPLE_NUMBER))
	{
		sampleNumber = parsed[P_SAMPLE_NUMBER].as<std::string>();
		std::cout << "sampleNumber = " << sampleNumber << std::endl;
	}
	/*
	if (parsed.count(P_RENDER_SIZE))
	{
		WINDOW_WIDTH	= parsed[P_RENDER_SIZE].as<int>();
		WINDOW_HEIGHT	= parsed[P_RENDER_SIZE].as<int>();
		std::cout << "renderSize = " << WINDOW_WIDTH << " by " << WINDOW_HEIGHT << std::endl;
	}
	*/

	glutInit(&argc, argv);            // Initialize GLUT
	glutInitDisplayMode(GLUT_DOUBLE); // Enable double buffered mode
	glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);   // Set the window's initial width & height
	glutInitWindowPosition(50, 50); // Position the window's initial top-left corner
	glutCreateWindow(title);          // Create window with the given title
	glutDisplayFunc(display);       // Register callback handler for window re-paint event
	glutReshapeFunc(reshape);       // Register callback handler for window re-size event
	initGL();                       // Our own OpenGL initialization
	glutTimerFunc(0, timer, 0);     // First timer call immediately [NEW]
	glutPassiveMotionFunc(mouseMove);
	glutKeyboardFunc(keyboard);
	glutKeyboardUpFunc(keyboardUp);
	glutMouseFunc(mouseButton);
	glutMainLoop();                 // Enter the infinite event-processing loop
	return 0;
}