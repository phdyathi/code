--[[
Run this to compute precision, recall and F1 scores
You need to mask out the occluded predictions so that only voxels available to the sensor
are used
--]]

require 'torchx'

function _calcMSE(outputs,groundTruths)
  return torch.sum(torch.pow((outputs-groundTruths),2))/outputs:nElement()
end

function _maskTruePositives(outputs,groundTruths)
  return torch.cmul(groundTruths,outputs)
end

function _maskFalsePositives(outputs,groundTruths)
  local falses = torch.zeros(#groundTruths)
  local zs = torch.lt(groundTruths,0.5)
  falses[zs] = 1
  return torch.cmul(falses,outputs)
end

function _maskFalseNegatives(outputs,groundTruths)
  local falseOutputs = torch.zeros(#outputs)
  local zs = torch.lt(outputs,0.5)
  falseOutputs[zs] = 1
  return torch.cmul(groundTruths,falseOutputs)
end

function falsePositives(outputs,groundTruths)
  local ret =  (_maskFalsePositives(outputs,groundTruths)):nonzero()
  if(ret:dim() ~= 0) then
    return (#ret)[1]
  else
    return 0
  end
end

function truePositives(outputs,groundTruths)
  local ret = (_maskTruePositives(outputs,groundTruths)):nonzero()
  if(ret:dim() ~= 0) then
    return (#ret)[1]
  else
    return 0
  end
end

function falseNegatives(outputs,groundTruths)
  local ret = (_maskFalseNegatives(outputs,groundTruths)):nonzero()
  if(ret:dim() ~= 0) then
    return (#ret)[1]
  else
    return 0
  end
end

function trueNegatives(outputs,groundTruths)
end

function precision(tp,fp)
  if tp == 0 and fp == 0 then
    return 0
  end
  return tp/(tp+fp)
end

function recall(tp,fn)
  if tp == 0 and fn == 0 then
    return 0
  end
  return tp/(tp+fn)
end

function _f1Score(p,r)
  if p == 0 and  r == 0 then
    return 0
  end
  return 2*((p*r)/(p+r))
end

function mseScore(predictions,truths)
  if #predictions ~= #truths then
    print("Prediction length ("..(#predictions)..") does not match truths length ("..(#truths)..")")
    return
  end

  local size = #predictions


  local cummMSE= 0
  for i = 1,size do
    cummMSE = cummMSE + _calcMSE(predictions[i],truths[i])
  end

  return (cummMSE/size)

end

function f1Score(predictions,truths)
  if #predictions ~= #truths then
    print("Prediction length ("..(#predictions)..") does not match truths length ("..(#truths)..")")
    return
  end

  local size  = #predictions
  local cummP = 0
  local cummR = 0

  local cummTP = 0
  local cummFP = 0
  local cummFN = 0
  for i = 1,size do
    cummTP = cummTP + truePositives(predictions[i],truths[i])
    cummFP = cummFP + falsePositives(predictions[i],truths[i])
    cummFN = cummFN + falseNegatives(predictions[i],truths[i])

  end
  print(cummTP,cummFP,cummFN)
  cummP = precision(cummTP,cummFP)
  cummR = recall(cummTP,cummFN)
  print(cummP,cummR)
  return _f1Score(cummP,cummR)
end

function truePositivePrecentage(predictions,truths)
  if #predictions ~= #truths then
    print("Prediction length ("..(#predictions)..") does not match truths length ("..(#truths)..")")
    return
  end

  local size  = #predictions

  local cummTP = 0
  for i = 1,size do
    cummTP = cummTP + (truePositives(predictions[i],truths[i])/(truths[i]:nonzero():size(1)))
  end

  return cummTP/size
end

-- This outputTensor needs to have 2 channels (sensor hits,visibility)
function extractVisiblePredictions(prediction,visibility)
  local masked = torch.cmul(visibility,prediction)
  masked[torch.gt(masked,0.5)] = 1
  masked[torch.lt(masked,0.5)] = 0
  return masked
end
